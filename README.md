Helena is a High Level Nets Analyzer under the GPL license (see file
LICENSE).  It can be used to model check high level Petri nets.

Download the latest version of the tool at
http://www.lipn.univ-paris13.fr/~evangelista/helena/

# Prerequisites

To compile Helena you will need:

* GCC
* [http://libre.adacore.com] The GNAT ada compiler.
* [http://mlton.org] The mlton compiler.  It is required if you want
  to analyse DVE models (option `--with-dve` of the compilation script).

# Compilation

To compile helena, go to directory src and execute script compile-helena.
The script can be passed with the following options:

* option `--with-ltl` => This is required if you want to analyse LTL
  properties.
* option `--with-dve` => This is required if you want to analyse DVE
  models (beta version).

Once the compilation is finished make sure that you PATH variable
points to the bin directory.  You may change the value of some
variables in script bin/helena:

* `gcc` - path of the gcc compiler used by helena to translate a model
  in C code
* `helenaDir` - path of the directory in which helena will store
  specific files for a model
  
# Acknowledgements

LTL properties are translated to Buchi automata using the LTL2BA tool by Oddoux
and Gastin.  See http://www.lsv.ens-cachan.fr/~gastin/ltl2ba for more
information on this tool.

# Usage

```
usage: helena [-h] [-V] [-v] [-N ACTION] [-p PROPERTY] [-pf FILE]
              [-md DIRECTORY] [-wp {0,1}] [-b {0,1}] [-g LEVEL]
              [-cp PARAM=VALUE] [-A ALGO] [-t HASH_SIZE] [-W N] [-R {0,1}]
              [-H {0,1}] [-P {0,1}] [-PD {0,1}] [-PA POR-ALGO]
              [-sc SCAPEGOAT-CHOICE] [-dt TRANS-CHOICE] [-i PROVISO]
              [-C {0,1}] [-cb N] [--DC {0,1}] [-mf FILE] [-ds STRATEGY]
              [-mr ARG] [-sl N] [-tl N] [-o FILE] [-hf FILE] [-tr TRACE-TYPE]
              [-d SYMBOL] [-a N] [-r {0,1}] [-L FILE] [-m PARAM=VAL]
              [-sh {0,1}] [-SM FILE] [-LM FILE]
              input

positional arguments:
  input

optional arguments:
  -h, --help            show this help message and exit
  -V, --version         Prints the version number and exit.
  -v, --verbose         Be verbose.
  -N ACTION, --action ACTION
                        Indicate the action performed on the model. Available
                        actions are: *EXPLORE* (Explore the state space of the
                        model and then prints some statistics), *SIMULATE*
                        (Start interactive simulation mode. You can then
                        navigate through the reachability graph of the model.
                        A simple command language is provided. Once the
                        simulation is started, type `help` to see the list of
                        commands.), *BUILD-GRAPH* (Build the reachability
                        graph of the model using algorithm DELTA-DDD (see
                        option `--algo`) and store it on disk. This graph can
                        then be analyzed using the helena-graph tool) and
                        *CHECK* (Check property specified by option
                        `--property`.).
  -p PROPERTY, --property PROPERTY
                        (For helena models only) Set the property to check if
                        option `--action=CHECK` has been set.
  -pf FILE, --property-file FILE
                        (For helena models only) File FILE contains the
                        definition of the property to check (specified with
                        option `--action=CHECK` and `--property=PROP`). By
                        default, if the input file of the model is model.lna,
                        Helena will look into model.prop.lna for the property
                        definition.
  -md DIRECTORY, --model-directory DIRECTORY
                        All generated files such as source files are put in
                        DIRECTORY (instead of ~/.helena/models/lna/my-net).
  -wp {0,1}, --with-papi {0,1}
                        Activate/deactivate the use of the PAPI (Performance
                        Application Programming Interface, see
                        http://icl.utk.edu/papi/) to print additional
                        statistics at the end of the search.
  -b {0,1}, --observer {0,1}
                        Activate/deactivate the observer thread that prints
                        some progression informations during the search.
  -g LEVEL, --progress LEVEL
                        LEVEL can take one of these three values: *no-compile*
                        (Helena stops after the generation of source files.),
                        *no-check* (Helena launches compilation but does not
                        launches the search.), *no-report* (Helena launches
                        the search but does not print any report.), and *all*
                        (everything is done normally).
  -cp PARAM=VALUE, --config-parameter PARAM=VALUE
                        TODO

search:
  -A ALGO, --algo ALGO  Sets search algorithm used to explore the state space.
                        Available algorithms are: *DFS* (depth-first search),
                        *BFS* (breadth-first search), *DBFS* (distributed
                        breadth-first search), *DELTA-DDD* (multi-threaded
                        breadth-first search based on state compression),
                        *DDELTA-DDD* (same but in a distributed way) and
                        *RWALK* (random walk. If no limit is specified the
                        search will last forever.)
  -t HASH_SIZE, --hash-size HASH_SIZE
                        Set to 2^N the size of the hash table which stores the
                        set of reachable states.
  -W N, --workers N     Set to N the number of working threads that will
                        perform the search.
  -R {0,1}, --random-succs {0,1}
                        Activate/deactivate randomised successor selection.
                        This is only valid if algorithm DFS is used. This
                        option is useful if the counter-example produced is
                        too long. Using randomisation can often produce a
                        smaller counter-example.

reduction techniques:
  -H {0,1}, --hash-compaction {0,1}
                        Activate/deactivate hash compaction. Its principle is
                        to only store a hash signature of each visited state.
                        In case of hash conflict, Helena will not necessarily
                        explore the whole state space and may report that no
                        error has been found whereas one could exist.
  -P {0,1}, --partial-order {0,1}
                        Activate/deactivate partial-order reduction. This
                        reduction limits the exploration of multiple paths
                        that are redundant with respect to the desired
                        property. This causes some states to be never explored
                        during the search. The reductions done depend on the
                        property verified. If there is no property checked,
                        the reduction done only preserves the existence of
                        deadlock states.
  -PD {0,1}, --partial-order-dynamic {0,1}
                        (For pnml and DVE models only) Activate/deactivate
                        dynamic partial-order reduction.
  -PA POR-ALGO, --partial-order-algo POR-ALGO
                        (For pnml models only) Select the partial order
                        reduction algorithm to be used: *CLOSURE*, or
                        *DELETION*.
  -sc SCAPEGOAT-CHOICE, --scapegoat-choice SCAPEGOAT-CHOICE
                        (For pnml models only) With the CLOSURE algorithm, set
                        the strategy to choose the scapegoat place: *FST*,
                        *MAX*, *MAXEN*, *MIN*, *MINEN*, or *RND*.
  -dt TRANS-CHOICE, --trans-choice TRANS-CHOICE
                        (For pnml models only) With the DELETION algorithm,
                        set the strategy to choose the transition to be
                        deleted at each iteration: *FST*, *MAX*, *MAXFWD*,
                        *MIN*, *MINFWD*, or *RND*. With the CLOSURE algorithm,
                        set the strategy to choose the starting transition:
                        *FST*, *MIN*, *MINFWD*, or *RND*.
  -i PROVISO, --proviso PROVISO
                        Activate/deactivate the proviso of partial-order
                        reduction. Available provisos are: *NONE* (This is
                        default if no property is checked or if the property
                        is deadlock absence), *SRC* (Source state expansion),
                        *DEST* (Destination state expansion. This is the
                        default if a property (different from deadlock
                        absence) is checked).
  -C {0,1}, --state-compression {0,1}
                        (For DVE models only) Activate/deactivate collapse
                        state compression
  -cb N, --compression-bits N
                        Set the base number of bits used by state compression
                        (option `-C`) to N.

distributed mode options:
  --DC {0,1}, --distributed-state-compression {0,1}
                        (For DVE models only.) Activate/deactivate state
                        compression based on distributed hash tables.
  -mf FILE, --machine-file FILE
                        Provides the machine-file to be passed to MPI for
                        distributed algorithms.
  -ds STRATEGY, --ddelta-ddd-strategy STRATEGY
                        Specify the strategy to use for algorithm DDELTA-DDD.
  -mr ARG, --to-mpirun ARG
                        Pass argument ARG to mpirun.

search limits:
  -sl N, --state-limit N
                        As soon as N states have been processed the search is
                        stopped as soon as possible. 0 = no limit
  -tl N, --time-limit N
                        The search time is limited to N seconds. When this
                        limit is reached the search stops as soon as possible.
                        0 = no limit

output:
  -o FILE, --report-file FILE
                        An XML report file is created by Helena once the
                        search terminated. It contains some informations such
                        as the result of the search, or some statistics.
  -hf FILE, --history-file FILE
                        Produces an history file containing data on states
                        stored, processed, ..., during the execution. Output
                        is in CSV format.
  -tr TRACE-TYPE, --trace-type TRACE-TYPE
                        Specify the type of trace (i.e., counter-example)
                        displayed. TRACE-TYPE must take one of these three
                        values: *FULL* (The full trace is displayed), *EVENTS*
                        (Only the sequence of events, the initial and the
                        final faulty states are displayed. Intermediary states
                        are not displayed) and *STATE* (Only the faulty state
                        reached is displayed. No information on how this state
                        can be reached is therefore available.).

model:
  -d SYMBOL, --define SYMBOL
                        (For helena models only.) Define preprocessor symbol
                        SYMBOL in the net.
  -a N, --capacity N    (For helena models only.) The default capacity of
                        places is set to N.
  -r {0,1}, --run-time-checks {0,1}
                        (For helena models only.) Activate/deactivate run time
                        checks such as: division by 0, expressions out of
                        range, capacity of places exceeded, ... If this option
                        is not activated, and such an error occurs during the
                        analysis, Helena may either crash, either produce
                        wrong results.
  -L FILE, --link FILE  (For helena models only.) Add FILE to the files linked
                        by Helena when compiling the net. Please consult user
                        guide for further help on this option.
  -m PARAM=VAL, --parameter PARAM=VAL
                        (For helena models only.) This gives value VAL (an
                        integer) to net parameter PARAM.
  -sh {0,1}, --shuffle {0,1}
                        (For pnml models only.) Shuffle model description
                        randomly (e.g., place list is reordered randomly).
  -SM FILE, --save-model-library FILE
                        Generate the model source files and compile them into
                        shared library FILE. Helena stops right after.
  -LM FILE, --load-model-library FILE
                        Do not generate nor compile model source files but
                        give instead the model library (generated before with
                        option `--save-model-library`.

```