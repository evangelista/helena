#!/usr/bin/env python3

import os
import matplotlib as mp
import matplotlib.pyplot as plt
import csv
import numpy as np
import itertools
import random
import pathlib

from util import fields, csv_file, figures_dir

def png_file(conf):
    return os.path.join(
        figures_dir,
        str(conf[0]),  # model
        str(conf[1]),  # nodes
        '_'.join(str(p) for p in conf[2:]) + '.png'
    )
    

with open(csv_file) as fd:
    reader = csv.DictReader(fd, delimiter=';')
    data = [{k: fields[k](v) for k, v in row.items()} for row in reader]

plt.rc('font', size=18)

try:
    os.mkdir(figures_dir)
except:
    pass

P = sorted(list({row['procs_per_node'] for row in data}))
D = 20

for conf in itertools.product(
        {row['model'] for row in data},
        {row['nodes'] for row in data},
        {row['steal_strategy'] for row in data},
        {row['cand_set_size'] for row in data},
        {row['ro_set_size'] for row in data},
        {row['bs'] for row in data},
        {row['dd_strategy'] for row in data},
        {row['dd_strategy_time'] for row in data}
):
    conf_rows = [
        row for row in data
        if conf == (
                row['model'],
                row['nodes'],
                row['steal_strategy'],
                row['cand_set_size'],
                row['ro_set_size'],
                row['bs'],
                row['dd_strategy'],
                row['dd_strategy_time']
        )
    ]
    if conf_rows == list():
        continue
    times = list()
    procs = list()
    for p in P:
        p_rows = [r for r in conf_rows if r['procs_per_node'] == p]
        if p_rows == list():
            continue
        l = list(r['time'] for r in p_rows)
        avg_time = np.average(l)
        procs.append(p * p_rows[0]['nodes'])
        times.append(avg_time)
    if len(times) == len(P):
        png = png_file(conf)
        #if os.path.exists(png):
        #    continue
        D = D - 1
        dirpath = os.path.dirname(png)
        pathlib.Path(dirpath).mkdir(parents=True, exist_ok=True)
        fig, axis = plt.subplots(figsize=(14, 8))
        axis.plot(procs, times, color='blue')
        axis.set(xlabel='processes', xticks=procs)
        axis.set_ylim(ymin=0)
        axis.margins(x=0, y=0)
        for x, y in zip(procs, times):
            axis.text(
                x, y, y,
                horizontalalignment='center',
                verticalalignment='bottom',
                fontsize=12,
                color='blue'
            )
        print(png)
        fig.savefig(png, bbox_inches='tight')
        #if D == 0:
        #    break
print(D)
exit(0)
