#!/usr/bin/env python3

import os
import sys
from sbench import Benchmark
import xml.etree.ElementTree as ET
import numpy as np
import csv

from util import fields_mc, csv_file_mc


bench_dir = os.path.join(
    os.environ['HOME'], '.local', 'share', 'sbench', 'd5-mc'
)

def extract_run(run):
    try:
        xml_file = os.path.join(run.get_data_dir(), 'report.xml')
        report = ET.parse(xml_file)
    except:
        print('could not parse %s' % xml_file, file=sys.stderr)
    else:
        def get_stat(typ, stat, op):
            values = [
                typ(el.text)
                for el in report.findall(f'.//{stat}')
            ]
            return op(values)
        stime = float(report.findall(f'.//searchTime')[0].text)
        event_exec = int(report.findall(f'.//eventsExecuted')[0].text)
        stored = int(report.findall(f'.//statesStored')[0].text)
        processed = int(report.findall(f'.//statesProcessed')[0].text)
        yield f'{stime};{event_exec};{stored};{processed}'


def extract_all():
    yield ';'.join(fields_mc.keys())
    bench = Benchmark.load_from_dir(bench_dir)
    for row in bench.extract(handler_fun=extract_run):
        yield(row)


reader = csv.DictReader(extract_all(), delimiter=';')
rows = [
    [
        fields_mc[f](v)
        if fields_mc[f] != float else ('%.2f' % float(v))
        for f, v in row.items()
    ]
    for row in reader
]
rows.sort()

with open(csv_file_mc, 'w') as fd:
    fd.write(';'.join(fields_mc.keys()) + '\n')
    fd.write('\n'.join(';'.join(str(data) for data in row) for row in rows))
    fd.write('\n')
exit(0)
