#!/usr/bin/env python3

import os
import matplotlib as mp
import matplotlib.pyplot as plt
import csv
import numpy as np
import itertools
import random
import pathlib

from util import fields, csv_file, figures_dir

def png_file(conf, prefix):
    return os.path.join(
        figures_dir,
        str(conf[0]),  # model
        str(conf[1]),  # nodes
        prefix + '_' + '_'.join(str(p) for p in conf[2:]) + '.png'
    )


def conf_strat_name(conf_strat):
    steal_strat, dd_strat, dd_strat_time = conf_strat
    if steal_strat == 'steal':
        return f'{steal_strat}, {dd_strat}'
    return f'{steal_strat}, {dd_strat}({dd_strat_time})'


def conf_strat_style(conf_strat):
    return {
        ('info', 'steal', 0): ('red', None),
        ('info', 'time', 10): ('red', 'dashed'),
        ('info', 'time', 100): ('red', 'dotted'),
        ('mem', 'steal', 0): ('green', None),
        ('mem', 'time', 10): ('green', 'dashed'),
        ('mem', 'time', 100): ('green', 'dotted'),
        ('rnd', 'steal', 0): ('blue', None),
        ('rnd', 'time', 10): ('blue', 'dashed'),
        ('rnd', 'time', 100): ('blue', 'dotted'),
        ('frnd', 'steal', 0): ('yellow', None),
        ('frnd', 'time', 10): ('yellow', 'dashed'),
        ('frnd', 'time', 100): ('yellow', 'dotted')
    }[conf_strat]


with open(csv_file) as fd:
    reader = csv.DictReader(fd, delimiter=';')
    data = [{k: fields[k](v) for k, v in row.items()} for row in reader]

plt.rc('font', size=18)

try:
    os.mkdir(figures_dir)
except:
    pass

P = sorted(list({row['procs_per_node'] for row in data}))
D = 20

for conf in itertools.product(
        {row['model'] for row in data},
        {row['nodes'] for row in data},
        {row['cand_set_size'] for row in data},
        {row['ro_set_size'] for row in data},
        {row['bs'] for row in data}
):
    conf_rows = [
        row for row in data
        if conf == (
                row['model'],
                row['nodes'],
                row['cand_set_size'],
                row['ro_set_size'],
                row['bs']
        )
    ]
    fig_time, axis_time = plt.subplots(figsize=(14, 8))
    fig_steal, axis_steal = plt.subplots(figsize=(14, 8))

    for conf_strat in itertools.product(
            {row['steal_strategy'] for row in data},
            {row['dd_strategy'] for row in data},
            {row['dd_strategy_time'] for row in data}
    ):
        rows = [
            row for row in conf_rows
            if conf_strat == (
                    row['steal_strategy'],
                    row['dd_strategy'],
                    row['dd_strategy_time']
            )
        ]
        times = list()
        procs = list()
        steal = list()
        for p in P:
            p_rows = [r for r in rows if r['procs_per_node'] == p]
            if p_rows == list():
                continue
            procs.append(p * p_rows[0]['nodes'])
            #  time
            avg_time = np.average([r['time'] for r in p_rows])
            times.append(avg_time)
            #  steal
            avg_steal = np.average(
                [100 * r['steal_successes'] / r['steal_attempts']
                 for r in p_rows]
            )
            steal.append(avg_steal)
        if len(times) > 1:
            axis_time.set(xlabel='processes', xticks=procs)
            axis_steal.set(xlabel='processes', xticks=procs)
            color, linestyle = conf_strat_style(conf_strat)
            axis_time.plot(
                procs, times, label=conf_strat_name(conf_strat),
                color=color, linestyle=linestyle
            )
            axis_steal.plot(
                procs, steal, label=conf_strat_name(conf_strat),
                color=color, linestyle=linestyle
            )

    for png, fig, axis, unit in [
            (png_file(conf, 'time'), fig_time, axis_time, ' s.'),
            (png_file(conf, 'steal'), fig_steal, axis_steal, ' %')
    ]:
        if os.path.exists(png):
            continue
        dirpath = os.path.dirname(png)
        pathlib.Path(dirpath).mkdir(parents=True, exist_ok=True)
        axis_steal.get_yaxis().set_major_formatter(
             mp.ticker.FuncFormatter(lambda x, p: format(float(x), ',') + unit)
        )
        axis.set_ylim(ymin=0)
        axis.legend(loc='lower left', fontsize=8)
        fig.savefig(png, bbox_inches='tight')
        print(png)
        D = D - 1
        if D <= 0:
            print(0)
            exit(0)
print(D)
exit(0)
