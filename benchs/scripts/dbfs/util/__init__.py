#!/usr/bin/env python3

import os
import pathlib

out_d5 = os.path.join('out', 'd5')

#  mkdir -p out/d5
pathlib.Path(out_d5).mkdir(parents=True, exist_ok=True)

csv_file = os.path.join(out_d5, 'data.csv')
csv_file_mc = os.path.join(out_d5, 'data-mc.csv')
figures_dir = os.path.join(out_d5, 'figures')

fields_mc = {
    'model': str,
    'workers': int,
    'set_size': int,
    'run': int,
    'exit_code': int,
    'timeout': int,
    'sbench_time': float,
    'time': float,
    'event_exec': int,
    'stored': int,
    'processed': int
}

#  the fields must be 
fields = {
    'model': str,
    'bs': int,
    'nodes': int,
    'procs_per_node': int,
    'steal_strategy': str,
    'dd_strategy': str,
    'dd_strategy_time': int,
    'cand_set_size': int,
    'ro_set_size': int,
    'run': int,
    'exit_code': int,
    'timeout': int,
    'sbench_time': float,
    'time': float,
    'dd_time': float,
    'event_exec': int,
    'processed': int,
    'processed_min': int,
    'processed_max': int,
    'stored_full': int,
    'steal_attempts': int,
    'steal_successes': int,
    'dds': int,
    'barrier_time': float
}


proc_per_node = [16, 14, 12, 10, 8]  #  grimoire
ro_block_size = [100, 1_000]
steal_strategy = ['mem', 'rnd', 'info']
dd_strategy = ['steal', 'time']
dd_strategy_times = [0, 10, 100]
cand_set_size = [10_000, 100_000]
ro_set_size = [10_000, 100_000]
