This directory contains scripts to perform experiments on stubborn
sets algorithms implemented in helena.  All commands must be executed
from the helena/benchs directory.


# Requirements

download and install the python3 sbench module:

```
git clone git@depot.lipn.univ-paris13.fr:evangelista/sbench.git
cd sbench
python3 setup.py install --user
```


# Run the experiments

from the helena/benchs directory, generate the benchmark description file

```
mkdir -p out/stub
scripts/stub/gen-sbench
```

launch the benchmarks

```
sbench -v -r out/stub/sbench.json
sbench -v -r out/stub/sbench-prov.json
sbench -v -r out/stub/sbench-prod.json
```

# Extract results

extract data and check that everything went fine

```
mkdir -p out/stub/html
scripts/stub/extract > out/stub/html/data.csv
scripts/stub/merge out/stub/html/data.csv
scripts/stub/check out/stub/html/data.csv
```

generate figures (this may take a while) and html pages

```
scripts/stub/gen-figures out/stub/html/data.csv out/stub/html/figures
scripts/stub/gen-models out/stub/html/data.csv > out/stub/html/models.html
cp scripts/stub/html/*html scripts/stub/html/*css out/stub/html/
```

view results

```
firefox out/stub/html/index.html
```
