#!/usr/bin/env python3

import os
import sys
from sbench import Benchmark

if len(sys.argv) != 3:
    print("usage: $1 == machine-file, $2 == output json file")
    sys.exit(1)
elif not os.path.isfile(sys.argv[1]):
    print(f"cannot read file {sys.argv[1]}")
    sys.exit(1)

machine_file = sys.argv[1]
json_file = sys.argv[2]
with open(machine_file, "r") as fd:
    nodes = {node for node in fd.readlines()}

script_dir = os.path.join("scripts", "d5-comp")
nb_nodes = len(nodes)
name = f"d5-comp-xp-{nb_nodes}"
timeout = 3600
repeat = 5
sma_symmetric_size = 2000000000
observer = 0
cmd = f"""\
helena\
 --verbose\
 --progress NO-REPORT\
 --report-file $SBENCH_DATA_DIR/report.xml\
 --observer {observer}\
 -mr \"-x SMA_SYMMETRIC_SIZE={sma_symmetric_size}"\
"""


def get_table_size(proc):
    return {
        1: 29,
        2: 28,
        4: 27,
        8: 26,
        12: 25
    }[proc]


#  parameters
proc_per_node = [12, 8, 4, 2, 1]  # uvb
buffer_size = 2000000000
args_d5 = f"""\
--algo D5 \
--config-parameter D5_BUFFER_SIZE={buffer_size} \
--config-parameter D5_RO_BLOCK_SIZE=1000 \
--config-parameter D5_STEAL_STRATEGY=frnd \
--config-parameter D5_DD_STRATEGY=steal \
--config-parameter D5_CAND_SET_SIZE=100000 \
--config-parameter D5_RO_SET_SIZE=10000"""
args_dbfs = """\
--algo DBFS \
--dbfs-strategy poll \
--config-parameter DBFS_BUFFER_SIZE=64000"""
args_dbfs_collapse = """\
--algo DBFS \
--dbfs-strategy poll \
--config-parameter DBFS_BUFFER_SIZE=64000 \
--distributed-state-compression 1 \
--compression-bits 20"""

#  get all models
models = dict()
models_lang = dict()
with open(f"{script_dir}/models", "r") as fd:
    for line in fd.readlines():
        lang, model, instance, check = line.strip().split(";")
        models[instance] = f"in/{lang}/{model}/{instance}.{lang}"
        models_lang.setdefault(lang, list()).append(instance)

filters = [
    f"algo != 'dbfs-collapse' or model != '{instance}'"
    for instance in (["firewire_tree.7", "pgm_protocol.11"] + models_lang.get("pnml", list()))
] + [
    f"algo != 'dbfs' or model != '{instance}'"
    for instance in ["firewire_tree.7", "leader_election.7", "needham.7" ]
] + [
    "algo != 'd5' or model != 'needham.7' or proc_per_node != '1'",
    "algo != 'd5' or model != 'synapse.9' or proc_per_node != '1'",
]

params = {
    "algo": {
        "d5": args_d5,
        "dbfs": args_dbfs,
        "dbfs-collapse": args_dbfs_collapse
    },
    "proc_per_node": {
        str(p): f"-mr \"-npernode {p}\"  --hash-size {get_table_size(p)}"
        for p in proc_per_node
    },
    "model": models,
    "num": {
        str(num): "" for num in range(repeat)
    }
}
Benchmark(
    name=name,
    cmd=cmd,
    timeout=timeout,
    params=params,
    filters=filters
).save(
    json_file
)
