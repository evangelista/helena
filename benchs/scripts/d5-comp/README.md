Experiments to compare D5 algorithms to other algorithms:
- D4
- DBFS
- DBFS with collapse compression

Experiments to be used in the STTT paper.
