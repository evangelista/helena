(*
 *  File:
 *     dve-por-compiler.sml
 *)


structure
DvePorCompiler:
sig

    val gen:
        System.system * TextIO.outstream * TextIO.outstream
	-> unit

end = struct

open DveCompilerUtils

fun eventsHaveDifferentSources e e' = let
    val ts = getTrans e
    val ts' = getTrans e'
in
    List.exists
        (fn (p, t) =>
            List.exists
                (fn (p', t') => p = p'
                                andalso
                                Trans.getSrc t <> Trans.getSrc t') ts') ts
end

val mergeEventList = ListMergeSort.uniqueSort
                         (fn (e, e') => String.compare (getEventName e,
                                                        getEventName e'))

fun buildAndOrGraph (s as { procs, ... }: System.system) = let
    exception EventNotFound
    exception GraphTooLarge
    val maxDepListSize = 16
    val E = buildEvents s
    val G = AndOrGraph.new_graph ()
    val H = HashTable.mkTable ((HashString.hashString o getEventName),
                               fn (e, f) => getEventName e = getEventName f)
                              (1000, EventNotFound)
    fun eventsWithSameSource (p, t) =
      List.filter (fn e => NONE
                           <> (List.find (fn (p', t') =>
                                             p = p'
                                             andalso
                                             Trans.getSrc t = Trans.getSrc t')
                                         (getTrans e)))
                  E
    fun getEventChannel (SYNC(_, _, _, _, _, t)) =
      SOME (Sync.getChan (valOf (Trans.getSync t)))
      | getEventChannel _ = NONE
    fun eventsWithSameChannel NONE = []
      | eventsWithSameChannel (SOME chan) =
        List.filter (fn e => getEventChannel e = SOME chan) E
    fun addDependentEvents e = let
        fun splitDependentSyncEvents evts = let
            val todo = ref [ evts ]
            val res = ref []
            val no = ref 0
            fun loop () = let
                fun splitList evts = let
                    val trans = List.concat (List.map getTrans evts)
                    val trans = List.map (fn (p, t) => (p, Trans.getSrc t))
                                         trans
                    val trans = ListMergeSort.sort
                                    (fn ((p, _), (p', _)) => p > p') trans
                    fun merge NONE [] = []
                      | merge (SOME (p, s)) [] =
                        [ (p, Utils.mergeStringList s) ]
                      | merge NONE ((p, s) :: tl) = merge (SOME (p, [ s ])) tl
                      | merge (SOME (p, l)) ((p', s') :: tl) =
                        if p <> p'
                        then (p, Utils.mergeStringList l)
                             :: (merge (SOME (p', [ s' ])) tl)
                        else merge (SOME (p', s' :: l)) tl
                    val trans = merge NONE trans
                    fun isRightLocation (p, s) e =
                      List.all (fn (p', t') =>
                                   p <> p'
                                   orelse
                                   s = Trans.getSrc t') (getTrans e)
                    val proc = List.find (fn (_, l) => List.length l > 1) trans
                in
                    case proc
                     of NONE => (no := !no + 1
                                ; if !no > maxDepListSize
                                  then raise GraphTooLarge
                                  else ()
                                ; res := evts :: !res)
                     |  SOME (p, l) => let
                         val evtsl = List.map (fn s =>
                                                  List.filter
                                                      (isRightLocation (p, s))
                                                      evts) l
                     in
                         List.app (fn l => todo := l :: (!todo)) evtsl
                     end
                end
            in
                case !todo
                 of [] => ()
                  | evts :: tl => (todo := tl; splitList evts; loop ())
            end
        in
            loop ()
          ; let val evts = ListMergeSort.uniqueSort
                               (List.collate (fn (e, e') =>
                                                 String.compare
                                                     (getEventName e,
                                                      getEventName e')))
                               (List.map (ListMergeSort.sort
                                              (fn (e, e') =>
                                                  getEventName e >
                                                  getEventName e'))
                                         (!res))
            in
                evts
            end
        end
        fun guardsInConflict e' = let
            val ts' = getTrans e'
            fun conf (NONE, _) = false
              | conf (_, NONE) = false
              | conf (SOME g, SOME g') =
                Expr.isContradiction (Expr.BIN_OP (0, g, Expr.AND, g'))
        in
            List.exists
                (fn (p, t) =>
                    List.exists
                        (fn (p', t') =>
                            p = p' andalso
                            conf (Trans.getGuard t,
                                  Trans.getGuard t')) ts')
                (getTrans e)
        end
        val ts = getTrans e
        val eName = getEventName e
        val eRef = HashTable.lookup H e
        val chan = getEventChannel e
        val evts = List.concat (List.map eventsWithSameSource ts)
        val evts = evts @ (eventsWithSameChannel chan)
        val evts = List.filter (not o guardsInConflict) evts
        val evts = List.filter (fn e => getEventName e <> eName) evts
        val evts = List.filter (not o (eventsHaveDifferentSources e)) evts
    in
        if List.length evts = 0
        then ()
        else let
            val (locEvts, syncEvts) =
                List.partition (fn LOCAL _ => true | _ => false) evts
            val syncEvts = splitDependentSyncEvents syncEvts
            val syncEvts = List.map mergeEventList syncEvts
            val andRef = AndOrGraph.add_node G AndOrGraph.AND
            val andSyncRef =
                List.mapPartial
                    (fn [] => NONE
                    | [ evt ] => SOME (HashTable.lookup H evt)
                    | evts => let
                        val evtsRefs = List.map (HashTable.lookup H) evts
                        val andRef = AndOrGraph.add_node G AndOrGraph.AND
                    in
                        List.app (fn n => AndOrGraph.add_edge G (andRef, n))
                                 evtsRefs
                      ; SOME andRef
                    end) syncEvts
        in
            AndOrGraph.add_edge G (eRef, andRef)
          ; List.app (fn l => let val lRef = HashTable.lookup H l
                              in AndOrGraph.add_edge G (andRef, lRef) end)
                     locEvts
          ; if List.length andSyncRef = 0
            then ()
            else let val orRef = AndOrGraph.add_node G AndOrGraph.OR
                 in
                     List.app (fn n => AndOrGraph.add_edge
                                           G (orRef, n)) andSyncRef
                   ; AndOrGraph.add_edge G (andRef, orRef)
                 end
        end handle GraphTooLarge => let
                 val falseRef = AndOrGraph.add_node G AndOrGraph.FALSE
             in
                 AndOrGraph.add_edge G (eRef, falseRef)                 
             end
    end
in
    List.app (fn e => let val n = AndOrGraph.add_node G (AndOrGraph.DATA e)
                      in HashTable.insert H (e, n) end)
             E
  ; List.app addDependentEvents E
  (*; List.app ((AndOrGraph.print G getEventName) o (HashTable.lookup H)) E*)
  ; (G, H)
end

fun analyseGraph (s as { procs, ... }: System.system) G H = let
    fun loop [] res = res
      | loop (todo :: tl) res = (
          case List.find (not o ! o #2) todo
           of NONE => loop tl (todo :: res)
            | SOME (e, done) => let
                val _ = done := true
                val dep = AndOrGraph.traverse G (HashTable.lookup H e)
                fun addDep l = let
                    val notIn = List.filter
                                    (fn e =>
                                        not (List.exists (fn (e', _) =>
                                                             e = e') todo)) l
                    val notIn = List.map (fn e => (e, ref false)) notIn
                in
                    ListMergeSort.sort
                        (fn ((e, _), (e', _)) =>
                            getEventName e > getEventName e')
                        (todo @ notIn)
                end
                val todo = List.map addDep dep
                val todo =
                    List.filter
                        (fn l =>
                            not (List.exists
                                     (fn (e, _) =>
                                         List.exists
                                             (eventsHaveDifferentSources e
                                              o #1)
                                             l)
                                     l)
                        ) todo
            in
                loop (tl @ todo) res
            end)
    fun eventOnlyAccessLocalVarsOrGlobalConstants e =
      List.all
          (fn (p, t) => System.onlyAccessLocalVarsOrGlobalConstants
                            s (System.getProc (s, p)) t)
          (getTrans e)
    val E = buildEvents s
    val D = loop (List.map (fn e => [ (e, ref false) ]) E) []
    val D = List.map (List.map #1) D
    val D = List.filter (List.all eventOnlyAccessLocalVarsOrGlobalConstants) D
in
    D
end

fun compileReduced (s: System.system, hFile, cFile) D = let
    val no = ref 0
    fun oneCase l = let
        val locs = List.map ((List.map (fn (p, t) =>
                                           (p, Trans.getSrc t))) o getTrans) l
        val locs = ListMergeSort.uniqueSort
                       (Utils.cmpCouple String.compare)
                       (List.concat locs)
	val conds = List.map (fn (p, s) => 
				 "s->"
				 ^ (getCompName (PROCESS_STATE p))
				 ^ " == "
				 ^ (getLocalStateName (p, s))) locs
	val conds = String.concatWith " && " conds
	val condsEvt = String.concatWith
			   " || "
			   (List.map
                                (fn e => "(" ^ (getEventName e) ^ " == e)") l)
	val checkFunc = "por_check_" ^ (Int.toString (!no))
	val countFunc = "por_count_" ^ (Int.toString (!no))
    in
	if condsEvt = ""
	then ""
	else (TextIO.output
                  (cFile,
		   "char " ^ checkFunc
		   ^ "(void * item, void * data) { "
		   ^ "mevent_t e = * ((mevent_t *) item); "
		   ^ "return " ^ condsEvt ^ "; }\n")
	     ; TextIO.output
                   (cFile,
		    "void " ^ countFunc
		    ^ "(void * item, void * n) { "
		    ^ "mevent_t e = * ((mevent_t *) item); "
		    ^ "if(" ^ condsEvt ^ ") { (* (int *) n) ++; } "
                    ^ "}\n")
	     ; no := !no + 1
	     ; concatLines [
		   "   if(" ^ conds ^ ") {",
		   "      if(choose) {",
                   "         n = 0;",
		   "         list_app(evts, &" ^ countFunc ^ ", &n);",
		   "         if(n > 0 && n < min) {",
		   "            min = n;",
		   "            found = " ^ (Int.toString (!no - 1)) ^ ";",
		   "         }",
		   "      } else if(list_find(evts, &"
		   ^ checkFunc ^ ", NULL)) {",
		   "         list_filter(evts, " ^ checkFunc ^ ", NULL);",
		   "         return;",
                   "      }",
		   "   }"
	     ])
    end
    val no = ref 0
    fun filter l = (
        no := !no + 1
      ; "   case "
        ^ (Int.toString (!no - 1))
        ^ ": list_filter(evts, por_check_" ^ (Int.toString (!no - 1))
        ^ ", NULL); return;"
    )
    val D = List.filter (fn [ l ] => false | _ => true) D
    val prot = concatLines [
            "void dynamic_por",
            "(mstate_t s,",
	    " list_t evts,",
	    " bool_t choose,",
	    " bool_t rnd)"
	]
    val body = concatLines [
            " {",
            "   int n, found = -1, min = 65536;",
	    String.concatWith "\n" (List.map oneCase D),
            "   switch(found) {",
	    String.concatWith "\n" (List.map filter D),
            "   }",
            "}"
	]
in
    TextIO.output (hFile, "#define MODEL_HAS_DYNAMIC_POR\n")
  ; TextIO.output (hFile, prot ^ ";\n")
  ; TextIO.output (cFile, prot ^ body ^ "\n")
end

fun compileIsSafe (s: System.system, hFile, cFile) D = let
    val E = List.filter (fn [ l ] => true | _ => false) D
    val E = List.map List.hd E
    val prot = concatLines [
            "bool_t mevent_is_safe",
            "(mevent_t e)"
        ]
    val body = [
        " {",
        "  switch(e) {",
        concatLines (List.map (fn evt => "  case " ^ (getEventName evt) ^
                                         ": return TRUE;") E),
        "  default : return FALSE;",
        "  }",
        "}"
    ]
    val body = prot ^ (concatLines body)
in
    TextIO.output (hFile, prot ^ ";\n")
  ; TextIO.output (cFile, body ^ "\n")
end

fun gen (s, hFile, cFile) = let
    val (G, H) = buildAndOrGraph s
    val D = analyseGraph s G H
in
    compileIsSafe (s, hFile, cFile) D
  ; compileReduced (s, hFile, cFile) D
end

end
