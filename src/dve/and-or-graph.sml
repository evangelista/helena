structure
AndOrGraph:
sig

    type 'a graph

    type node_ref

    datatype 'a node = DATA of 'a
                     | AND
                     | OR
                     | FALSE

    val new_graph:
        unit
        -> 'a graph

    val add_node:
        'a graph
        -> 'a node
        -> node_ref

    val add_edge:
        'a graph
        -> (node_ref * node_ref)
        -> unit
        
    val get_node:
        'a graph
        -> node_ref
        -> 'a node
        
    val get_succs:
        'a graph
        -> node_ref
        -> node_ref list

    val print:
        'a graph
        -> ('a -> string)
        -> node_ref
        -> unit

    val traverse:
        'a graph
        -> node_ref
        -> 'a list list

    exception NodeNotFoundException
        
end = struct

type node_ref = int

datatype 'a node = DATA of 'a
                 | AND
                 | OR
                 | FALSE

type 'a graph = {
    nodes: ('a node * node_ref list) option Array.array ref,
    no_nodes: int ref
}

exception NodeNotFoundException

fun new_graph () = {
    nodes = ref (Array.array(1, NONE)),
    no_nodes = ref 0
}

fun add_node { no_nodes, nodes } n = (
    if !no_nodes < Array.length (!nodes)
    then ()
    else nodes := Array.tabulate (if !no_nodes = 0 then 1 else !no_nodes * 2,
                                  (fn i => if i < !no_nodes
                                           then Array.sub (!nodes, i)
                                           else NONE))
  ; Array.update (!nodes, !no_nodes, SOME (n, []))
  ; !no_nodes
    before no_nodes := !no_nodes + 1
)
        
fun get_node_internal ({ no_nodes, nodes }: 'a graph) nref =
  if nref >= (!no_nodes) orelse nref < 0
  then raise NodeNotFoundException
  else case Array.sub (!nodes, nref)
        of NONE => raise NodeNotFoundException
        |  SOME n => n

fun get_node g nref =
  #1 (get_node_internal g nref)

fun get_succs g nref =
  #2 (get_node_internal g nref)
                                         
fun add_edge (g as { no_nodes, nodes }) (ref_from, ref_to) = let
    val (node_from, succs_from) = get_node_internal g ref_from
in
    Array.update (!nodes, ref_from, SOME (node_from, ref_to :: succs_from))
end

fun print (g as { no_nodes, nodes }) node_to_string nref = let
    val baseIndent = "  "
    fun loop indent continue nref = let
        val (n, succs) = get_node_internal g nref
        val (str, continue) =
            case n
             of DATA d => (node_to_string d, continue)
              | AND => ("AND", true)
              | OR => ("OR", true)
              | FALSE => ("FALSE", false)
    in
        TextIO.print (indent ^ str ^ "\n")
      ; if not continue
        then ()
        else List.app (loop (indent ^ baseIndent) false) succs
    end
in
    loop "" true nref
end

fun traverse (g as { no_nodes, nodes }) nref = let
    fun loop nref =
      case get_node_internal g nref
       of (DATA value, _) => [ [ value ] ]
        | (FALSE, _) => [ ]
        | (AND, succs) => let
            val prod = List.foldl
                           (fn (nref, NONE) => SOME (loop nref)
                           |   (nref, SOME []) => SOME []
                           |   (nref, SOME prod) =>
                               case loop nref
                                of [] => SOME []
                                 | values => let
                                     val prod = Utils.listProd (values, prod)
                                 in
                                     SOME (List.map List.@ prod)
                                 end
                           ) NONE succs
        in
            case prod
             of NONE => []
             |  SOME prod => prod
        end
        | (OR, succs) =>
          List.foldl (fn (nref, union) => (loop nref) @ union) [] succs
    val (_, succs) = get_node_internal g nref
in
    case succs
     of [] => [ [] ]
      | succ :: _ => loop succ
end

end
