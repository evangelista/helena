#include "heap.h"
#include "model.h"
#include "reduction.h"
#include "debug.h"
#include "model_por_closure.h"


typedef struct {
  token_t * m;
  bool_t opt;
  bool_t * is_stub;
  bool_t * is_en;
  bool_t * is_fwd;
  uint32_t * incr_p;
  list_t en;
  list_t todo;
  por_scapegoat_choice_t sg_choice;
  por_trans_choice_t tr_choice;
} por_closure_data_t;


void
check_place
(plid_t p,
 por_closure_data_t * data);


/**
 *  transition t becomes stubborn.  array stub is updated and t is put
 *  in list todo if put_in_todo != 0
 */
void
dynamic_por_add_stub
(trid_t t,
 bool_t put_in_todo,
 por_closure_data_t * data) {
  if(!data->is_stub[t]) {
    data->is_stub[t] = TRUE;
    if(put_in_todo) {
      list_append(data->todo, &t);
    }
    if(data->opt) {

      /* decrease incr_p of each output place */
      for(int i = 0; i < NET.updated_t_nb[t]; i ++) {
        const arc_t a = NET.updated_t[t][i];
        if(a.tok > 0) {
          LNA_ASSERT(data->incr_p[a.id] > 0, "incr_p[%d] == 0", a.id);
          data->incr_p[a.id] --;
          check_place(a.id, data);
        }
      }

      /* use net units */
      if(data->is_en[t]) {
        /*  for each input place of t  */ 
        for(int i = 0; i < NET.pre_t_nb[t]; i ++) {
          const plid_t p = NET.pre_t[t][i].id;
          /*  if p is part of a unit un */ 
          if(NET.p_unit[p] < NET.un_nb) {
            const unid_t un = NET.p_unit[p];
            /*  for each place q != p of un  */ 
            for(int j = 0; j < NET.unit_p_nb[un]; j ++) {
              const plid_t q = NET.unit_p[un][j];
              if(p != q) {
                /*  for each output transition v of q => v is stubborn  */ 
                for(int k = 0; k < NET.post_p_nb[q]; k ++) {
                  const unid_t v = NET.post_p[q][k].id;
                  dynamic_por_add_stub(v, FALSE, data);
                }
              }
            }
          }
        }
      }
    }
  }
}


/**
 *  (CLOSURE-OPT only) if the incr counter of place p has reached 0 =>
 *  mark all transitions that are disabled by p as stubborn
 */
void
check_place
(plid_t p,
 por_closure_data_t * data) {
  if(data->incr_p[p] == 0) {
    for(int i = 0; i < NET.post_p_nb[p]; i ++) {
      const arc_t a = NET.post_p[p][i];
      if(a.tok > data->m[p]) {
        dynamic_por_add_stub(a.id, FALSE, data);
      }
    }
  }
}


/**
 *  count the number of input transitions of place pid that are not
 *  already stubborn
 */
uint32_t
dynamic_por_count_scapegoat_inputs
(trid_t t,
 plid_t p,
 por_closure_data_t * data) {
  uint32_t result = 0;
  const bool_t fwd_check =
    data->sg_choice == POR_SCAPEGOAT_CHOICE_MINFWD ||
    data->sg_choice == POR_SCAPEGOAT_CHOICE_MAXFWD;
  const bool_t en_check =
    data->sg_choice == POR_SCAPEGOAT_CHOICE_MINEN ||
    data->sg_choice == POR_SCAPEGOAT_CHOICE_MAXEN;

  for(int i = 0; i < NET.act_p_nb[p]; i ++) {
    const trid_t u = NET.act_p[p][i];
    if(!data->is_stub[u]) {
      if(fwd_check) {
        if(data->is_fwd[u]) {
          result += 1000000;
        } else if(data->is_en[u]) {
          result += 1000;
        } else {
          result ++;
        }
      } else if(en_check) {
        if(data->is_en[u]) {
          result += 1000;
        } else {
          result ++;
        }
      } else {
        result ++;
      }
    }
  }
  return result;
}


/**
 *  put in stub all transitions of which the firing increase the
 *  number of tokens in p
 */
void
dynamic_por_handle_scapegoat
(trid_t t,
 plid_t p,
 por_closure_data_t * data) {
  for(int i = 0; i < NET.act_p_nb[p]; i ++) {
    const trid_t u = NET.act_p[p][i];
    if(!NET.R || NET.R[u][t]) {
      dynamic_por_add_stub(u, TRUE, data);
    }
  }
}


/**
 *  POR closure algorithm main function
 */
void
dynamic_por_closure
(list_t en,
 void * data,
 reduction_algo_t * algo,
 token_t * m,
 bool_t * is_en,
 bool_t * is_fwd,
 bool_t * is_stub) {
  plid_t p;
  uint32_t sg_ctr, ctr, no_sgs, sgs[NET.pl_nb];
  heap_t h;
  rseed_t seed;
  list_iter_t it;
  bool_t best_stub[NET.tr_nb];
  int start, i, sg, min_cost, cost = 0;
  trid_t t, u = 0;
  bool_t first = TRUE;
  por_data_t * por_data = (por_data_t *) data;
  list_t included = por_data ? por_data->included : NULL;
  uint32_t incr_p[NET.pl_nb];
  por_closure_data_t cdata = {
    .opt = algo->por_algo_opt,
    .m = m,
    .incr_p = incr_p,
    .en = en,
    .is_en = is_en,
    .is_fwd = is_fwd,
    .is_stub = is_stub,
    .sg_choice = algo->scapegoat_choice,
    .tr_choice = algo->trans_choice
  };

  seed = random_seed(0);
  h = local_heap_new();
  if(included) {
    start = 0;
    cdata.tr_choice = POR_TRANS_CHOICE_FST;
  } else {
    if(POR_TRANS_CHOICE_RND == cdata.tr_choice) {
      seed = random_seed(0);
      start = random_int(&seed) % list_size(en);
    } else {
      start = 0;
    }
  }
 start_stubborn_computation:
  cdata.todo = list_new(h, sizeof(trid_t), NULL);
  memset(cdata.is_stub, 0, sizeof(bool_t) * NET.tr_nb);
  memcpy(incr_p, NET.act_p_nb, sizeof(uint32_t) * NET.pl_nb);
  for(i = 0; i < NET.pl_nb; i ++) {
    check_place(i, &cdata);
  }
  if(included) {
    list_for_each(included, it) {
      u = * ((trid_t *) list_iter_item(it));
      if(cdata.is_en[u]) {
        dynamic_por_add_stub(u, TRUE, &cdata);
      }
    }
  } else {
    u = * ((trid_t *) list_nth(en, start));
    dynamic_por_add_stub(u, TRUE, &cdata);
  }
  while(!list_is_empty(cdata.todo)) {
    list_pick_first(cdata.todo, &t);
    if(cdata.is_en[t]) {

      /*  enabled transition handling  */
      for(i = 0; i < NET.conf_t_nb[t]; i ++) {
        if(!NET.D || NET.D[t][NET.conf_t[t][i]]) {
          dynamic_por_add_stub(NET.conf_t[t][i], TRUE, &cdata);
        }
      }

    } else {        

      /*  disabled transition handling  */
      for(no_sgs = 0, sg_ctr = 0, sg = - 1, i = 0;
	  i < NET.pre_t_nb[t];
	  i ++) {
	p = NET.pre_t[t][i].id;

	/*  a scapegoat has been found  */
	if(NET.pre_t[t][i].tok > m[p]) {
          
	  if(cdata.sg_choice == POR_SCAPEGOAT_CHOICE_RND) {
	    sgs[no_sgs] = p;
	    no_sgs ++;
	  } else if(-1 == sg) {
            sg = p;
	    if(cdata.sg_choice == POR_SCAPEGOAT_CHOICE_FST) {
	      break;
	    }
	    sg_ctr = dynamic_por_count_scapegoat_inputs(t, p, &cdata);
          } else {
	    ctr = dynamic_por_count_scapegoat_inputs(t, p, &cdata);
	    switch(cdata.sg_choice) {
	    case POR_SCAPEGOAT_CHOICE_MIN:
	    case POR_SCAPEGOAT_CHOICE_MINEN:
	    case POR_SCAPEGOAT_CHOICE_MINFWD:
	      if(ctr < sg_ctr) {
		sg = p;
		sg_ctr = ctr;
	      }
	      break;
	    case POR_SCAPEGOAT_CHOICE_MAX:
	    case POR_SCAPEGOAT_CHOICE_MAXEN:
	    case POR_SCAPEGOAT_CHOICE_MAXFWD:
	      if(ctr > sg_ctr) {
		sg = p;
		sg_ctr = ctr;
	      }
	      break;
	    default:
	      LNA_UNREACHABLE_CODE();
	    }
          }
        }
      }
      if(cdata.sg_choice == POR_SCAPEGOAT_CHOICE_RND) {
	sg = sgs[random_int(&seed) % no_sgs];
      }
      LNA_ASSERT(sg >= 0, "no scapegoat found");
      dynamic_por_handle_scapegoat(t, sg, &cdata);
    }
  }

  /**
   *  strategy MIN and MINFWD: count the number of enabled/forward
   *  transitions
   */
  if(POR_TRANS_CHOICE_MINFWD == cdata.tr_choice ||
     POR_TRANS_CHOICE_MIN == cdata.tr_choice) {
    cost = 0;
    for(t = 0; t < NET.tr_nb; t ++) {
      if(cdata.is_stub[t] && cdata.is_en[t]) {
        if(POR_TRANS_CHOICE_MINFWD == cdata.tr_choice) {
          cost += is_fwd[t];
        } else {
          cost ++;
        }
      }
    }
  }

  /**
   *  check what to do with the scapegoat computed
   */
  if(first) {
    first = FALSE;
    memcpy(best_stub, cdata.is_stub, sizeof(bool_t) * NET.tr_nb);
    min_cost = cost;
  } else {
    if(cdata.tr_choice == POR_TRANS_CHOICE_MINFWD ||
       cdata.tr_choice == POR_TRANS_CHOICE_MIN) {
      if(cost < min_cost) {
        memcpy(best_stub, cdata.is_stub, sizeof(bool_t) * NET.tr_nb);
	min_cost = cost;
      }
    } else {
      LNA_UNREACHABLE_CODE();
    }
  }

  /**
   *  under some conditions we have to compute another stubborn set
   *  with a different starting transition
   */
  if(!included && start < list_size(en) - 1 &&
     ((POR_TRANS_CHOICE_MIN == cdata.tr_choice && 1 != min_cost)
      ||
      (POR_TRANS_CHOICE_MINFWD == cdata.tr_choice && 0 != min_cost))) {
    start ++;
    goto start_stubborn_computation;
  }

  memcpy(is_stub, best_stub, sizeof(bool_t) * NET.tr_nb);
  heap_free(h);
}
