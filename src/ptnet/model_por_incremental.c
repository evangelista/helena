#include "model.h"
#include "reduction.h"
#include "darray.h"
#include "debug.h"
#include "model_por_incremental.h"
#include "heap.h"
#include "model_por_scapegoat.h"


typedef struct {
  token_t * m;
  list_t en;
  bool_t * is_en;
  bool_t * is_fwd;
} por_incremental_data_t;


void
inc_graph_add_succ
(por_incremental_data_t * data,
 trid_t src,
 trid_t dst);


void
inc_graph_handle_disabled
(por_incremental_data_t * data,
 trid_t t) {
}


void
inc_graph_handle_enabled
(por_incremental_data_t * data,
 trid_t t) {
  for(int i = 0; i < NET.conf_t_nb[t]; i ++) {
    if(!NET.D || NET.D[t][NET.conf_t[t][i]]) {
      inc_graph_add_succ(data, t, NET.conf_t[t][i]);
    }
  }
}


void
inc_graph_handle_trans
(por_incremental_data_t * data,
 trid_t t) {
  if(!NET.inc_graph.nodes[t].computed) {
    NET.inc_graph.nodes[t].computed = TRUE;
    darray_reset(NET.inc_graph.nodes[t].succ);
    if(data->is_en[t]) {
      inc_graph_handle_enabled(data, t);
    } else {
      inc_graph_handle_disabled(data, t);
    }
  }
}


void
inc_graph_add_succ
(por_incremental_data_t * data,
 trid_t src,
 trid_t dst) {
  darray_push(NET.inc_graph.nodes[src].succ, &dst);
  inc_graph_handle_trans(data, dst);
}


void
dynamic_por_incremental
(list_t en,
 void * data,
 reduction_algo_t * algo,
 token_t * m,
 bool_t * is_en,
 bool_t * is_fwd,
 bool_t * is_stub) {
  por_incremental_data_t incremental_data = {
    .m = m,
    .en = en,
    .is_en = is_en,
    .is_fwd = is_fwd
  };
  list_iter_t it;

  for(int t = 0; t < NET.tr_nb; t ++) {
    NET.inc_graph.nodes[t].computed = FALSE;
  }
  list_for_each(en, it) {
    trid_t t = * ((trid_t *) list_iter_item(it));
    inc_graph_handle_trans(&incremental_data, t);
  }
}
