#include "model_por_scapegoat.h"
#include "debug.h"


uint32_t
dynamic_por_count_scapegoat_inputs
(trid_t t,
 plid_t p,
 bool_t * is_en,
 bool_t * is_fwd,
 bool_t * is_stub,
 por_scapegoat_choice_t sg_choice) {
  uint32_t result = 0;
  const bool_t fwd_check =
    sg_choice == POR_SCAPEGOAT_CHOICE_MINFWD ||
    sg_choice == POR_SCAPEGOAT_CHOICE_MAXFWD;
  const bool_t en_check =
    sg_choice == POR_SCAPEGOAT_CHOICE_MINEN ||
    sg_choice == POR_SCAPEGOAT_CHOICE_MAXEN;

  for(int i = 0; i < NET.act_p_nb[p]; i ++) {
    const trid_t u = NET.act_p[p][i];
    if(!is_stub[u]) {
      if(fwd_check) {
        if(is_fwd[u]) {
          result += 1000000;
        } else if(is_en[u]) {
          result += 1000;
        } else {
          result ++;
        }
      } else if(en_check) {
        if(is_en[u]) {
          result += 1000;
        } else {
          result ++;
        }
      } else {
        result ++;
      }
    }
  }
  return result;
}


plid_t
dynamic_por_choose_scapegoat
(trid_t t,
 token_t * m,
 bool_t * is_en,
 bool_t * is_fwd,
 bool_t * is_stub,
 por_scapegoat_choice_t sg_choice) {
  bool_t found = FALSE;
  plid_t result, p, sgs[NET.pl_nb];
  uint32_t best = 0, ctr, no_sgs = 0;

  for(int i = 0; i < NET.pre_t_nb[t]; i ++) {
    p = NET.pre_t[t][i].id;
    if(NET.pre_t[t][i].tok > m[p]) {
      if(sg_choice == POR_SCAPEGOAT_CHOICE_RND) {
        sgs[no_sgs] = p;
        no_sgs ++;
      } else if(!found) {
        result = p;
        if(sg_choice == POR_SCAPEGOAT_CHOICE_FST) {
          break;
        }
        best = dynamic_por_count_scapegoat_inputs
          (t, p, is_en, is_fwd, is_stub, sg_choice);
      } else {
        ctr = dynamic_por_count_scapegoat_inputs
          (t, p, is_en, is_fwd, is_stub, sg_choice);
        if((sg_choice == POR_SCAPEGOAT_CHOICE_MIN ||
            sg_choice == POR_SCAPEGOAT_CHOICE_MINEN ||
            sg_choice == POR_SCAPEGOAT_CHOICE_MINFWD)
           && ctr < best) {
          result = p;
          best = ctr;
        } else if((sg_choice == POR_SCAPEGOAT_CHOICE_MAX ||
                   sg_choice == POR_SCAPEGOAT_CHOICE_MAXEN ||
                   sg_choice == POR_SCAPEGOAT_CHOICE_MAXFWD)
                  && ctr > best) {
          result = p;
          best = ctr;
        }
      }
      found = TRUE;
    }
  }
  if(sg_choice == POR_SCAPEGOAT_CHOICE_RND) {
    result = sgs[random_int(&seed) % no_sgs];
  }
  LNA_ASSERT(found, "no scapegoat found");
  return result;
}
