#include "model.h"
#include "model_loader.h"

#define MODEL_TOKENS_WIDTH_WIDTH 5

ptnet_t NET;

char
mstate_tokens_width
(token_t tok) {
  char result = 0;
  while(tok) {
    tok = tok >> 1;
    result ++;
  }
  assert(result <= 32);
  return result;
}

unsigned int
mstate_char_size
(mstate_t s) {
  list_iter_t it;
  marked_place_t m;
  unsigned int result = NET.pl_nb_width;

  list_for_each(s->marked, it) {
    m = * ((marked_place_t *) list_iter_item(it));
    result += NET.pl_id_width + MODEL_TOKENS_WIDTH_WIDTH +
      mstate_tokens_width(m.tok);
  }
  return (result / CHAR_BIT) + ((result % CHAR_BIT) ? 1 : 0);
}

void
mstate_serialise
(mstate_t s,
 char * v,
 uint16_t * size) {
  const int len = list_size(s->marked);
  list_iter_t it;
  marked_place_t m;
  bit_stream_t stream;
  char w;

  *size = mstate_char_size(s);
  v[*size - 1] = 0; 
  bit_stream_init(stream, v);
  bit_stream_set(stream, len, NET.pl_nb_width);
  list_for_each(s->marked, it) {
    m = * ((marked_place_t *) list_iter_item(it));
    bit_stream_set(stream, m.pid, NET.pl_id_width);
    w = mstate_tokens_width(m.tok);
    bit_stream_set(stream, w, MODEL_TOKENS_WIDTH_WIDTH);
    bit_stream_set(stream, m.tok, (int) w);
  }
}

mstate_t
mstate_unserialise(char * v, heap_t heap) {
  int len;
  marked_place_t m;
  bit_stream_t stream;
  mstate_t result = mem_alloc(heap, sizeof(struct_mstate_t));
  char w;

  result->heap = heap;
  result->marked = list_new(heap, sizeof(marked_place_t), NULL);
  bit_stream_init(stream, v);
  bit_stream_get(stream, len, NET.pl_nb_width);
  while(len --) {
    bit_stream_get(stream, m.pid, NET.pl_id_width);
    bit_stream_get(stream, w, MODEL_TOKENS_WIDTH_WIDTH);
    bit_stream_get(stream, m.tok, (int) w);
    list_append(result->marked, &m);
  }
  return result;
}

unsigned int
mevent_char_size
(mevent_t e) {
  return sizeof(mevent_t);
}

bool_t
mstate_equal
(mstate_t s1,
 mstate_t s2) {
  list_iter_t it1, it2;
  if(list_size(s1->marked) != list_size(s2->marked)) {
    return FALSE;
  }
  for(it1 = list_get_iter(s1->marked), it2 = list_get_iter(s2->marked);
      !list_iter_at_end(it1);
      it1 = list_iter_next(it1), it2 = list_iter_next(it2)) {
    marked_place_t * m1 = (marked_place_t *) list_iter_item(it1);
    marked_place_t * m2 = (marked_place_t *) list_iter_item(it2);
    if(m1->pid != m2->pid || m1->tok != m2->tok) {
      return FALSE;
    }
  }
  return TRUE;
}

bool_t
mstate_cmp_string
(mstate_t s,
 char * v) {
  uint32_t not_empty = 0, pid, tokens;
  marked_place_t m;
  char w;
  list_iter_t it = list_get_iter(s->marked);
  bit_stream_t stream;

  bit_stream_init(stream, v);
  bit_stream_get(stream, not_empty, NET.pl_nb_width);
  if(not_empty != list_size(s->marked)) {
    return FALSE;
  }
  while(not_empty) {
    m = * ((marked_place_t *) list_iter_item(it));
    bit_stream_get(stream, pid, NET.pl_id_width);
    bit_stream_get(stream, w, MODEL_TOKENS_WIDTH_WIDTH);
    bit_stream_get(stream, tokens, (int) w);
    if(pid != m.pid || tokens != m.tok) {
      return FALSE;
    }
    it = list_iter_next(it);
    not_empty --;
  }   
  return TRUE;
}

void
mevent_serialise
(mevent_t e,
 char * v) {
  memcpy(v, &e, sizeof(mevent_t));
}

mevent_t
mevent_unserialise
(char * v,
 heap_t heap) {
  mevent_t result;

  memcpy(&result, v, sizeof(mevent_t));
  return result;
}

mevent_id_t
mevent_id
(mevent_t e) {
  return e;
}

void
mstate_free
(mstate_t s) {
  list_free(s->marked);
  mem_free(s->heap, s);
}

void
mevent_free
(mevent_t e) {
}

order_t
mevent_cmp
(mevent_t e,
 mevent_t f) {
  if(e < f) {
    return LESS;
  } else if(e > f) {
    return GREATER; 
  } else {
    return EQUAL;
  }
}

mstate_t
mstate_copy
(mstate_t s,
 heap_t heap) {
  mstate_t result = mem_alloc(heap, sizeof(struct_mstate_t));

  result->marked = list_copy(s->marked, heap, NULL);
  result->heap = heap;
  return result;
}

mevent_t
mevent_copy
(mevent_t e,
 heap_t heap) {
  return e;
}

hkey_t
mstate_hash
(mstate_t s) {
  uint16_t size = mstate_char_size(s);
  char buffer[size];

  buffer[size - 1] = 0;
  mstate_serialise(s, buffer, &size);
  return string_hash(buffer, size);
}

bool_t
mevent_is_visible
(mevent_t e) {
  return FALSE;
}

char
is_not_empty
(void * item,
 void * data) {
  marked_place_t m = * ((marked_place_t *) item);

  return (0 != m.tok) ? TRUE : FALSE;
}

int
marked_place_cmp
(void * a,
 void * b) {
  marked_place_t ma = * ((marked_place_t *) a);
  marked_place_t mb = * ((marked_place_t *) b);

  if(ma.pid < mb.pid) {
    return -1;
  } else if(ma.pid > mb.pid) {
    return 1;
  } else {
    return 0;
  }
}

void
mevent_exec_or_undo
(mevent_t e,
 mstate_t s,
 char op) {
  marked_place_t * m;
  int i = 0;
  list_iter_t it = list_get_iter(s->marked);
  arc_t arc;
  bool_t done[NET.updated_t_nb[e]];

  memset(done, 0, sizeof(bool_t) * NET.updated_t_nb[e]);
  i = 0;
  while(!list_iter_at_end(it) && i < NET.updated_t_nb[e]) {
    m = (marked_place_t *) list_iter_item(it);
    arc = NET.updated_t[e][i];
    if(m->pid == arc.id) {
      m->tok += op * arc.tok;
      done[i] = TRUE;
      it = list_iter_next(it);
      i ++;
    } else if(m->pid < arc.id) {
      it = list_iter_next(it);
    } else {
      i ++;
    }
  }
  for(i = 0; i < NET.updated_t_nb[e]; i++) {
    arc = NET.updated_t[e][i];
    if(!done[i]) {
      marked_place_t new_m = { .pid = arc.id, .tok = 0 };
      new_m.tok += op * arc.tok;
      list_insert_sorted(s->marked, &new_m, marked_place_cmp);
    }
  }
  list_filter(s->marked, is_not_empty, NULL);
}

void
mevent_exec
(mevent_t e,
 mstate_t s) {
  mevent_exec_or_undo(e, s, 1);
}

void
mevent_undo
(mevent_t e,
 mstate_t s) {
  mevent_exec_or_undo(e, s, -1);
}

mstate_t
mstate_succ
(mstate_t s,
 mevent_t e,
 heap_t heap) {
  mstate_t result = mstate_copy(s, heap);

  mevent_exec(e, result);
  return result;
}

mstate_t
mstate_pred
(mstate_t s,
 mevent_t e,
 heap_t heap) {
  mstate_t result = mstate_copy(s, heap);

  mevent_undo(e, result);
  return result;
}

mevent_t
mstate_event
(mstate_t s,
 mevent_t e,
 heap_t h) {
  return e;
}

bool_t
mevent_is_safe
(mevent_t e) {
  return NET.safe_t[e];
}

void
mstate_to_xml
(mstate_t s,
 FILE * out) {
  list_iter_t it;
  fprintf(out, "<stateDescription>\n{\n");
  list_for_each(s->marked, it) {
    const marked_place_t * m = list_iter_item(it);
    fprintf(out, "  %s = %d\n", NET.pl_names[m->pid], m->tok);
  }
  fprintf(out, "}\n</stateDescription>\n");
}

void
mevent_to_xml
(mevent_t e,
 FILE * out) {
  fprintf(out, "<eventDescription>%s</eventDescription>",
          NET.tr_names[e]);
}

void
model_xml_statistics
(FILE * out) {
  fprintf(out, "<modelStatistics>\n");
  fprintf(out, "<places>%d</places>\n", NET.pl_nb);
  fprintf(out, "<transitions>%d</transitions>\n", NET.tr_nb);
  fprintf(out, "<netArcs>%d</netArcs>\n", NET.arcs_nb);
  fprintf(out, "</modelStatistics>\n");
}

mstate_t
mstate_initial
(heap_t heap) {
  return mstate_copy(NET.m0, heap);
}

void
init_model
() {
  model_load(&NET);
}

void
finalise_model
() {
}

char *
model_name
() {
  return NET.name;
}

bool_t
model_is_state_proposition
(char * prop) {
  return FALSE;
}

bool_t
model_check_state_proposition
(char * prop,
 mstate_t s) {
  return FALSE;
}

list_t
mstate_events
(mstate_t s,
 heap_t heap) {
  list_t result = list_new(heap, sizeof(mevent_t), NULL);
  marked_place_t m;
  list_iter_t it;
  mevent_t t;
  uint32_t nb_pre_t[NET.tr_nb];
  int i;
  arc_t arc;

  //  TODO: special case for transitions that have no input place
  memcpy(nb_pre_t, NET.pre_t_nb, sizeof(uint32_t) * NET.tr_nb);
  list_for_each(s->marked, it) {
    m = * ((marked_place_t *) list_iter_item(it));
    for(i = 0; i < NET.post_p_nb[m.pid]; i ++) {
      arc = NET.post_p[m.pid][i];
      if(m.tok >= arc.tok) {
	nb_pre_t[arc.id] --;
	if(0 == nb_pre_t[arc.id]) {
	  t = arc.id;
	  list_append(result, &t);
	}
      }
    }
  }
  return result;
}

void
mstate_print
(mstate_t s,
 FILE * out) {
  marked_place_t m;
  list_iter_t it;

  list_for_each(s->marked, it) {
    m = * ((marked_place_t *) list_iter_item(it));
    fprintf(out, "%s = %d\n", NET.pl_names[m.pid], m.tok);
  }
}

void
mevent_print
(mevent_t e,
 FILE * out) {
  fprintf(out, "%s\n", NET.tr_names[e]);
}

bool_t
mstate_check_tokens
(mstate_t s,
 plid_t p,
 int val) {
  list_iter_t it;
  list_for_each(s->marked, it) {
    const marked_place_t * m = list_iter_item(it);
    if(m->pid == p) {
      return val == m->tok;
    }
  }
  return val == 0;
}
