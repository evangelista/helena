#include "includes.h"
#include "common.h"
#include "list.h"
#include "model.h"

#ifndef LIB_MODEL_POR_SCAPEGOAT
#define LIB_MODEL_POR_SCAPEGOAT

plid_t
dynamic_por_choose_scapegoat
(trid_t t,
 token_t * m,
 bool_t * is_en,
 bool_t * is_fwd,
 bool_t * is_stub,
 por_scapegoat_choice_t sg_choice);

#endif
