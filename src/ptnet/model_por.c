#include "model.h"
#include "reduction.h"
#include "model_por_closure.h"
#include "model_por_deletion.h"
/*#include "model_por_incremental.h"*/

char
dynamic_por_is_stubborn
(void * t,
 void * stub) {
  return ((char *) stub)[* ((trid_t *) t)];
}


/**
 *  call the appropriate dynamic_por function
 */
void dynamic_por
(mstate_t s,
 list_t en,
 void * data,
 void * algo) {
  list_iter_t it;
  marked_place_t mp;
  reduction_algo_t * red_algo = algo;
  por_data_t * por_data = (por_data_t *) data;
  token_t m[NET.pl_nb];
  bool_t is_en[NET.tr_nb], is_fwd[NET.tr_nb], is_stub[NET.tr_nb];
  bool_t check_fwd;
  const por_trans_choice_t tr_choice = red_algo->trans_choice;
  const por_scapegoat_choice_t sg_choice = red_algo->scapegoat_choice;

  /**
   *  no enabled transition => nothing to do
   */
  if(list_is_empty(en)) {
    return;
  } 

  /**
   *  put the marking in an int vector for easier manipulation
   */
  memset(m, 0, sizeof(token_t) * NET.pl_nb);
  list_for_each(s->marked, it) {
    mp = * ((marked_place_t *) list_iter_item(it));
    m[mp.pid] = mp.tok;
  }

  /**
   *  same for the enabled list we put in a bool vector
   */
  memset(is_en, 0, NET.tr_nb * sizeof(bool_t));
  list_for_each(en, it) {
    trid_t t = * ((trid_t *) list_iter_item(it));
    is_en[t] = TRUE;
  }


  /**
   *  with some strategies we have to check which transitions are
   *  forward (i.e., enabled + leading to new states)
   */
  check_fwd = tr_choice == POR_TRANS_CHOICE_MAXFWD
    || tr_choice == POR_TRANS_CHOICE_MINFWD
    || sg_choice == POR_SCAPEGOAT_CHOICE_MAXFWD
    || sg_choice == POR_SCAPEGOAT_CHOICE_MINFWD;
  if(check_fwd) {
    memset(is_fwd, 0, NET.tr_nb * sizeof(bool_t));
    list_for_each(en, it) {
      trid_t t = * ((trid_t *) list_iter_item(it));
      mevent_exec(t, s);
      is_fwd[t] = por_data->is_new_func(s);
      mevent_undo(t, s);
    }
  }

  switch(red_algo->por_algo) {
  case POR_ALGO_CLOSURE:
    dynamic_por_closure(en, data, red_algo, m, is_en, is_fwd, is_stub);
    break;
  case POR_ALGO_INCREMENTAL:
    /*dynamic_por_incremental(en, data, red_algo, m, is_en, is_fwd, is_stub);*/
    assert(0);
    break;
  case POR_ALGO_CLODEL:
    dynamic_por_clodel(en, data, red_algo, m, is_en, is_fwd, is_stub);
    break;
  case POR_ALGO_DELETION:
    dynamic_por_deletion(en, data, red_algo, m, is_en, is_fwd, is_stub);
    break;
  }
  list_filter(en, dynamic_por_is_stubborn, is_stub);
}
