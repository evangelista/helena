#include "model.h"
#include "reduction.h"
#include "debug.h"
#include "model_por_deletion.h"
#include "model_por_closure.h"
#include "heap.h"

typedef struct {
  token_t * m;
  list_t en;
  uint32_t best_size;
  bool_t * best_deleted;
} por_deletion_data_t;


void
dynamic_por_deletion_delete_node
(uint32_t nid,
 uint32_t * deleted,
 por_deletion_data_t * data) {
  const del_graph_node_t node = NET.del_graph.nodes[nid];
  list_iter_t it;
  uint32_t pred_id;
  plid_t p;
  int i;

  if(NET.del_graph.protected[nid]) {
    (*deleted) = NET.del_graph.nodes_nb;
    assert(TRANS_NODE == node.ctype && AND_NODE == node.ntype);
    return;
  }
  if(NET.del_graph.deleted[nid]) {
    return;
  }

  /**
   *  an AND-node is deleted whereas we only decrease the reference
   *  counter of an OR-node and delete it if the counter reaches 0
   */
  if(AND_NODE == node.ntype) {
    if(TRANS_NODE == node.ctype) {
      (*deleted) ++;
    }
    NET.del_graph.deleted[nid] = TRUE;
  } else {
    NET.del_graph.ref[nid] --;
    if(0 == NET.del_graph.ref[nid]) {
      NET.del_graph.deleted[nid] = TRUE;
    }
  }

  /**
   *  recursive call on the predecessors of the node
   */
  if(NET.del_graph.deleted[nid]) {
    list_for_each(node.pred, it) {
      pred_id = * ((uint32_t *) list_iter_item(it));

      /**
       *  the node is a place => recursively delete the predecessor
       *    transition t only if p is not marked enough for the firing
       *    of t in the current marking
       */
      if(PLACE_NODE == node.ctype) {
        p = nid - NET.tr_nb;
        assert(pred_id < NET.tr_nb && nid >= NET.tr_nb);
        for(i = 0;
            i < NET.post_p_nb[p] && NET.post_p[p][i].id != pred_id;
            i ++);
        assert(i < NET.post_p_nb[p]);
        if(data->m[p] < NET.post_p[p][i].tok) {
          dynamic_por_deletion_delete_node(pred_id, deleted, data);
        }
      } else {
        
        /**
         *  the node is a transition => we ignore its predecessors
         *    that are disabled transitions
         */
        if(!(/*OR_NODE == NET.del_graph.nodes[nid].ntype &&*/
             TRANS_NODE == NET.del_graph.nodes[pred_id].ctype &&
             (OR_NODE == NET.del_graph.nodes[pred_id].ntype
              || (NET.D && !NET.D[pred_id][nid])))) {
          dynamic_por_deletion_delete_node(pred_id, deleted, data);
        }
      }
    }
  }
}


void
dynamic_por_deletion_restore_del_graph
() {
  for(uint32_t t = 0; t < NET.tr_nb; t ++) {
    NET.del_graph.nodes[t].ntype = OR_NODE;
  }
  NET.del_graph_init = FALSE;
}


void
dynamic_por_deletion_init_del_graph
(por_data_t * data,
 por_deletion_data_t * deletion_data) {
  int i;
  list_iter_t it;
  trid_t t;
  plid_t p;
  const uint32_t sizeof_deleted = sizeof(bool_t) * NET.del_graph.nodes_nb;
  const uint32_t sizeof_ref = sizeof(uint16_t) * NET.del_graph.nodes_nb;

  if(NET.del_graph_init) {
    return;
  }
  NET.del_graph_init = TRUE;

  /**
   *  initialise reference counters and flags 
   */
  memset(NET.del_graph.deleted, 0, sizeof_deleted);
  memset(NET.del_graph.protected, 0, sizeof_deleted);
  memset(NET.del_graph.ref, 0, sizeof_ref);
  if(data && data->included) {
    list_for_each(data->included, it) {
      t = * ((trid_t *) list_iter_item(it));
      NET.del_graph.protected[t] = TRUE;
    }
  }
  
  /**
   *  enabled transitions become AND-nodes
   */
  list_for_each(deletion_data->en, it) {
    t = * ((trid_t *) list_iter_item(it));
    NET.del_graph.nodes[t].ntype = AND_NODE;
  }

  /**
   *  initialise the reference counter of OR-nodes (disabled
   *  transitions) to the number of input places of t that are not
   *  marked enough to enable t
   */
  for(p = 0; p < NET.pl_nb; p ++) {
    for(i = 0; i < NET.post_p_nb[p]; i ++) {
      if(deletion_data->m[p] < NET.post_p[p][i].tok) {
	t = NET.post_p[p][i].id;
	NET.del_graph.ref[t] ++;
      }
    }
  }

  /**
   *  check all OR-nodes are disabled transitions with a positive
   *  reference counter, and all AND-NODES have a zero reference
   *  counter
   */
  for(t = 0; t < NET.tr_nb; t ++) {
    assert(NET.del_graph.nodes[t].ctype == TRANS_NODE);
    if(NET.del_graph.nodes[t].ntype == OR_NODE) {
      assert(NET.del_graph.ref[t] > 0);
    } else {
      assert(NET.del_graph.ref[t] == 0);
    }
  }
}


void
dynamic_por_deletion
(list_t en,
 void * data,
 reduction_algo_t * algo,
 token_t * m,
 bool_t * is_en,
 bool_t * is_fwd,
 bool_t * is_stub) {
  list_t l, lcp;
  trid_t t, t_minmax, t_it;
  list_iter_t it;
  bool_t deleted_backup[NET.del_graph.nodes_nb], restore, found, update;
  bool_t deletable[NET.tr_nb];
  uint32_t cost[NET.tr_nb], deleted, deleted_minmax = 0;
  uint16_t ref_backup[NET.del_graph.nodes_nb];
  list_size_t size;
  const uint32_t sizeof_deleted = sizeof(bool_t) * NET.del_graph.nodes_nb;
  const uint32_t sizeof_ref = sizeof(uint16_t) * NET.del_graph.nodes_nb;
  heap_t heap = NULL;
  rseed_t seed;
  por_deletion_data_t deletion_data = {
    .m = m,
    .en = en
  };

  memset(deletable, 1, NET.tr_nb);
  dynamic_por_deletion_init_del_graph((por_data_t *) data, &deletion_data);

  /**
   *  compute in l the traversal order of list en
   */
  if(algo->trans_choice != POR_TRANS_CHOICE_RND) {
    l = en;
  } else {
    heap = local_heap_new();
    lcp = list_copy(en, heap, NULL);
    l = list_new(heap, sizeof(trid_t), NULL);
    seed = random_seed(0);
    while(!list_is_empty(lcp)) {
      list_pick_nth(lcp, random_int(&seed) % list_size(lcp), &t);
      list_append(l, &t);
    }
  }

  /**
   *  strategy min-forward or max-forward => check which transitions
   *     of en are forward transitions
   */
  if(algo->trans_choice == POR_TRANS_CHOICE_MINFWD ||
     algo->trans_choice == POR_TRANS_CHOICE_MAXFWD) {
    memset(cost, 0, NET.tr_nb * sizeof(uint32_t));
    list_for_each(en, it) {
      trid_t t = * ((trid_t *) list_iter_item(it));
      cost[t] = is_fwd[t] ? 1000 : 1;
    }
  }

  /**
   *  compute the number of non deleted transitions
   */
  size = 0;
  list_for_each(en, it) {
    trid_t t = * ((trid_t *) list_iter_item(it));
    if(!NET.del_graph.deleted[t]) {
      size ++;
    }
  }

  /**
   *  try to delete every enabled transition.  if after a deletion
   *  there is no enabled transition remaining, we undo the deletion.
   *  we use backup arrays to undo the deletion
   */
 start_deletion:
  found = FALSE;
  list_for_each(l, it) {
    t = * ((trid_t *) list_iter_item(it));
    if(!deletable[t] || NET.del_graph.deleted[t]) {
      continue;
    }
    memcpy(deleted_backup, NET.del_graph.deleted, sizeof_deleted);
    memcpy(ref_backup, NET.del_graph.ref, sizeof_ref);
    deleted = 0;
    dynamic_por_deletion_delete_node(t, &deleted, &deletion_data);
    assert(deleted > 0);
    if(deleted >= size) {
      restore = TRUE;
      deletable[t] = FALSE;
    } else {
      switch(algo->trans_choice) {	
      case POR_TRANS_CHOICE_MINFWD:
      case POR_TRANS_CHOICE_MAXFWD:
	/**
	 *  minfwd and maxfwd => count the number of forward
	 *  transitions deleted
	 */
	deleted = 0;
	for(t_it = 0; t_it < NET.tr_nb; t_it ++) {
	  if(!deleted_backup[t_it] && NET.del_graph.deleted[t_it]) {
	    deleted += cost[t_it];
	  }
	}
      case POR_TRANS_CHOICE_MIN:
      case POR_TRANS_CHOICE_MAX:
        update = !found
          || ((algo->trans_choice == POR_TRANS_CHOICE_MIN ||
               algo->trans_choice == POR_TRANS_CHOICE_MINFWD)
              && deleted < deleted_minmax)
          || ((algo->trans_choice == POR_TRANS_CHOICE_MAX ||
               algo->trans_choice == POR_TRANS_CHOICE_MAXFWD)
              && deleted > deleted_minmax);
        if(update) {
          t_minmax = t;
          deleted_minmax = deleted;
        }
        found = TRUE;
        restore = TRUE;
	break;
      default:
        restore = FALSE;
        size -= deleted;
      }
    }
    if(restore) {
      memcpy(NET.del_graph.deleted, deleted_backup, sizeof_deleted);
      memcpy(NET.del_graph.ref, ref_backup, sizeof_ref);
    }
  }
  if(found) {
    deleted = 0;
    dynamic_por_deletion_delete_node(t_minmax, &deleted, &deletion_data);
    assert(size > deleted);
    size -= deleted;
    goto start_deletion;
  }

  dynamic_por_deletion_restore_del_graph();
  if(heap) {
    heap_free(heap);
  }

  memset(is_stub, 0, sizeof(bool_t) * NET.tr_nb);
  for(t = 0; t < NET.tr_nb; t ++) {
    is_stub[t] = !NET.del_graph.deleted[t];
  }
}


void dynamic_por_clodel
(list_t en,
 void * data,
 reduction_algo_t * algo,
 token_t * m,
 bool_t * is_en,
 bool_t * is_fwd,
 bool_t * is_stub) {
  bool_t tmp_is_stub[NET.tr_nb];
  list_iter_t it;
  por_deletion_data_t deletion_data = {
    .m = m,
    .en = en
  };
  uint32_t deleted, size = list_size(en);
  const por_trans_choice_t trans_choice = algo->trans_choice;
  trid_t t;
  
  dynamic_por_deletion_init_del_graph((por_data_t *) data, &deletion_data);

  /**
   *  first apply closure algorithm
   */
  dynamic_por_closure(en, data, algo, m, is_en, is_fwd, tmp_is_stub);

  /**
   *  then delete from the graph transitions that are not stubborn
   */
  list_for_each(en, it) {
    t = * ((trid_t *) list_iter_item(it));
    if(!tmp_is_stub[t]) {
      deleted = 0;
      dynamic_por_deletion_delete_node(t, &deleted, &deletion_data);
      if(size <= deleted) {
        memcpy(is_stub, tmp_is_stub, sizeof(bool_t) * NET.tr_nb);
        goto dynamic_por_clodel_end;
      }
      size -= deleted;
    }
  }

  /**
   *  and finally apply deletion algorithm
   */
  algo->trans_choice = POR_TRANS_CHOICE_FST;
  dynamic_por_deletion(en, data, algo, m, is_en, is_fwd, is_stub);
  algo->trans_choice = trans_choice;

 dynamic_por_clodel_end:
  dynamic_por_deletion_restore_del_graph();
}
