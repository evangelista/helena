#include "includes.h"
#include "common.h"
#include "errors.h"
#include "heap.h"
#include "list.h"
#include "bit_stream.h"
#include "darray.h"

#ifndef LIB_MODEL
#define LIB_MODEL

#define MODEL_HAS_EVENT_UNDOABLE
#define MODEL_HAS_DYNAMIC_POR
#define MODEL_HAS_SAFE_EVENTS_XXXXXXXXXX


typedef uint32_t token_t;

typedef uint32_t plid_t;

typedef uint32_t trid_t;

typedef uint32_t unid_t;

typedef struct {
  plid_t pid;
  token_t tok;
} marked_place_t;

typedef struct {
  uint32_t id;
  int32_t tok;
} arc_t;

typedef struct {
  list_t marked;
  heap_t heap;
} struct_mstate_t;

typedef struct_mstate_t * mstate_t;

typedef trid_t mevent_t;

typedef uint32_t mevent_id_t;

typedef enum
  {
   AND_NODE,
   OR_NODE
  } del_graph_node_type_t;

typedef enum
  {
   PLACE_NODE,
   TRANS_NODE
  } del_graph_content_type_t;

typedef struct  {
  uint32_t nid;
  del_graph_node_type_t ntype;
  del_graph_content_type_t ctype;
  list_t pred;
} del_graph_node_t;

typedef struct {
  bool_t * deleted;
  bool_t * protected;
  uint16_t * ref;
  del_graph_node_t * nodes;
  uint32_t nodes_nb;
} del_graph_t;

typedef struct {
  bool_t computed;
  darray_t succ;
} inc_graph_node_t;

typedef struct {
  inc_graph_node_t * nodes;
} inc_graph_t;

typedef struct {
  uint32_t    pl_nb;
  uint32_t    tr_nb;
  uint32_t    un_nb;  
  uint32_t    arcs_nb;

  uint32_t    pl_nb_width;
  uint32_t    pl_id_width;

  char *      name;
  char **     tr_names;
  char **     pl_names;

  mstate_t    m0;
  
  uint32_t *  post_p_nb;
  arc_t **    post_p;

  uint32_t *  pre_t_nb;
  arc_t **    pre_t;
  
  uint32_t *  updated_t_nb;
  arc_t **    updated_t;

  uint32_t *  conf_t_nb;
  trid_t **   conf_t;
  
  uint32_t *  act_p_nb;
  trid_t **   act_p;

  bool_t *    safe_t;
  
  uint32_t *  unit_p_nb;
  plid_t **   unit_p;
  
  unid_t *    p_unit;

  del_graph_t del_graph;
  bool_t      del_graph_init;

  inc_graph_t inc_graph;

  bool_t **   D;
  bool_t **   R;
} ptnet_t;

extern ptnet_t NET;

list_t
mstate_events
(mstate_t s,
 heap_t heap);

mevent_t
mstate_event
(mstate_t s,
 mevent_t e,
 heap_t h);

void
mevent_exec
(mevent_t e,
 mstate_t s);

void
mevent_undo
(mevent_t e,
 mstate_t s);

mstate_t
mstate_succ
(mstate_t s,
 mevent_t e,
 heap_t heap);

mstate_t
mstate_pred
(mstate_t s,
 mevent_t e,
 heap_t heap);

unsigned int
mstate_char_size
(mstate_t s);

bool_t
mstate_equal
(mstate_t s1,
 mstate_t s2);

void
mstate_serialise
(mstate_t s,
 char * v,
 uint16_t * size);

mstate_t
mstate_unserialise
(char * v,
 heap_t heap);

void
mevent_serialise
(mevent_t e,
 char * v);

mevent_t
mevent_unserialise
(char * v,
 heap_t heap);

unsigned int
mevent_char_size
(mevent_t e);

bool_t
mstate_cmp_string
(mstate_t s,
 char * v);

mevent_id_t
mevent_id
(mevent_t e);

void
mstate_free
(mstate_t s);

void
mevent_free
(mevent_t e);

order_t
mevent_cmp
(mevent_t e,
 mevent_t f);

mstate_t
mstate_copy
(mstate_t s,
 heap_t heap);

mevent_t
mevent_copy
(mevent_t e,
 heap_t heap);

hkey_t
mstate_hash
(mstate_t s);

bool_t
mevent_is_visible
(mevent_t e);

void
init_model
();

void
finalise_model
();

char *
model_name
();

bool_t
model_is_state_proposition
(char * prop);

bool_t
model_check_state_proposition
(char * prop,
 mstate_t s);

list_t
mstate_events
(mstate_t s,
 heap_t heap);

void
mstate_print
(mstate_t s,
 FILE * out);

void
mevent_print
(mevent_t e,
 FILE * out);

mstate_t
mstate_initial
(heap_t heap);

void
mstate_to_xml
(mstate_t s,
 FILE * out);

void
mevent_to_xml
(mevent_t e,
 FILE * out);

void
model_xml_statistics
(FILE * out);

bool_t
mevent_is_safe
(mevent_t e);

void
dynamic_por
(mstate_t s,
 list_t en,
 void * data,
 void * algo);

#endif
