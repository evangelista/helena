#include "includes.h"
#include "common.h"
#include "list.h"

#ifndef LIB_MODEL_POR_DELETION
#define LIB_MODEL_POR_DELETION

void
dynamic_por_deletion
(list_t en,
 void * data,
 reduction_algo_t * algo,
 token_t * m,
 bool_t * is_en,
 bool_t * is_fwd,
 bool_t * is_stub);

void
dynamic_por_clodel
(list_t en,
 void * data,
 reduction_algo_t * algo,
 token_t * m,
 bool_t * is_en,
 bool_t * is_fwd,
 bool_t * is_stub);

#endif
