#include "d5.h"
#include "d5_lb.h"
#include "hset.h"
#include "comm.h"
#include "debug.h"
#include "context.h"
#include "darray.h"
#include "bit_stream.h"


#if CFG_ALGO_D5 && CFG_D5_LB

#include "shmem.h"

/**
 *  error messages used in LNA_ASSERTs
 */
#define D5_ERR_BALANCE_UNFINISHED "other PEs are still balancing loads"
#define D5_ERR_BLOCK_DUPLICATE "block already in map"
#define D5_ERR_BLOCK_EMPTY "empty block"
#define D5_ERR_BLOCK_INVALID "invalid block"

#define D5_MAX_BLOCK_NB 10000000

#define d5_bl_ref_equal(_ref1, _ref2)           \
  (((_ref1).pe == (_ref2).pe) &&                \
   ((_ref1).bid == (_ref2).bid))
#define d5_st_ref_equal(_ref1, _ref2)           \
  (((_ref1).idx == (_ref2).idx) &&              \
   d5_bl_ref_equal((_ref1).blk, (_ref2).blk))
#define d5_uncomp_st_ref(_comp_ref, _ref) {             \
    bit_stream_t bs;                                    \
    bit_stream_init(bs, _comp_ref);                     \
    bit_stream_get(bs, _ref.blk.pe, D5.bwidth_pe);      \
    bit_stream_get(bs, _ref.blk.bid, D5.bwidth_bid);    \
    bit_stream_get(bs, _ref.idx, D5.bwidth_idx);        \
  }
#define d5_comp_st_ref(_ref, _comp_ref) {               \
    bit_stream_t bs;                                    \
    bit_stream_init(bs, _comp_ref);                     \
    bit_stream_set(bs, _ref.blk.pe, D5.bwidth_pe);      \
    bit_stream_set(bs, _ref.blk.bid, D5.bwidth_bid);    \
    bit_stream_set(bs, _ref.idx, D5.bwidth_idx);        \
  }
#define d5_get_local_dstate(_id, _idx)          \
  ((d5_st_t *) (D5_SH_BLOCK +                   \
                D5_SH_BLOCK_DATA[_id].pos +     \
                sizeof(d5_st_t) * _idx))


/**
 *  global shared variables (names prefixed by D5_SH_)
 */
static d5_bl_data_t D5_SH_BLOCK_DATA[D5_MAX_BLOCK_NB];
static uint32_t D5_SH_NO_NEW_BL = 0;
static int D5_SH_LOCK = 0;
static d5_new_blocks_data_t D5_SH_NEW_BLOCKS_DATA;
char * D5_SH_BLOCK;


/**
 *  global private variables and constants
 */
const d5_bl_ref_t D5_B0 = { .bid = 0, .pe = 0 };


/**
 * @d5_bl_data_hash
 */
hkey_t
d5_bl_data_hash
(void * ptr) {
  return ((d5_bl_data_t *) ptr)->ref.pe * 10000000 +
    ((d5_bl_data_t *) ptr)->ref.bid;
}


/**
 * @d5_bl_data_equal
 */
char
d5_bl_data_equal
(void * a,
 void * b) {
  return d5_bl_ref_equal(((d5_bl_data_t *) a)->ref, ((d5_bl_data_t *) b)->ref);
}


/**
 *  d5_print_block
 *
 */
void
d5_print_block
(d5_bid_t bid) {
  d5_bl_data_t blk_data = D5_SH_BLOCK_DATA[bid];
  d5_st_t * blk = (d5_st_t *) (D5_SH_BLOCK + blk_data.pos);
  int i;

  printf("[%d]block (%d, %d) of %d states {\n",
         D5.pe_me, blk_data.ref.pe, blk_data.ref.bid, blk_data.no_st);
  for(i = 0; i < blk_data.no_st; i ++) {
    d5_st_ref_t father;
    d5_st_ref_t fst_child;
    
    d5_uncomp_st_ref(blk[i].father, father);
    d5_uncomp_st_ref(blk[i].fst_child, fst_child);
    printf
      ("[%d]  father=(%d,%d,%d), fst_child=(%d,%d,%d), flags=%d, \
children=%d, evt=%d\n",
       D5.pe_me, father.blk.pe, father.blk.bid, father.idx,
       fst_child.blk.pe, fst_child.blk.bid, fst_child.idx,
       blk[i].flags,
       blk[i].no_children,
       blk[i].evt);
  }
  printf("[%d]}\n", D5.pe_me);
}

void
d5_print_blocks
() {
  for(int i = 0; i < D5.next_block_id; i ++) {
    d5_print_block(i);
  }
}


/**
 *  d5_print_block_tree
 *
 */
void
d5_print_block_tree
() {
  printf("=====\n");
  for(int i = 0; i < D5.next_block_id; i ++) {
    printf("[%d] block=%d ", D5.pe_me, i);
    printf("=> no_st=%d\n", D5_SH_BLOCK_DATA[i].no_st);
  }
  printf("=====\n");
  d5_print_blocks();
}


#define d5_flag_for_step(_ref, _proc_flag, _visit_flag) {               \
  d5_bl_data_t bl_data;                                                 \
  d5_st_t * sts;                                                        \
  bool_t loop = TRUE;                                                   \
  uint8_t xx = 3;                                                       \
  assert(sts = d5_get_block(_ref.blk, TRUE, FALSE, &bl_data));          \
  sts[_ref.idx].flags |= _proc_flag;                                    \
  while(loop && !d5_bl_ref_equal(_ref.blk, D5_B0)) {                    \
    d5_st_ref_t fref;                                                   \
    if(xx) xx--;                                                        \
    d5_uncomp_st_ref(sts[_ref.idx].father, fref);                       \
    assert(sts = d5_get_block(fref.blk, TRUE, !xx, &bl_data));          \
    if(loop = !(sts[fref.idx].flags & _visit_flag)) {                   \
      sts[fref.idx].flags |= _visit_flag;                               \
      _ref = fref;                                                      \
    }                                                                   \
  }                                                                     \
  }


/**
 * @d5_pull_buffer
 */
void
d5_pull_buffer
(void * src,
 uint32_t size,
 int pe) {
  LNA_ASSERT(size <= CFG_D5_BUFFER_SIZE, D5_ERR_BUFFER_TOO_SMALL);
  comm_getmem(D5.buf, src, size, pe);
}


/**
 * @d5_pull_block
 */
d5_st_t *
d5_pull_block
(d5_bl_ref_t ref,
 d5_bl_data_t * data,
 d5_st_t * blk,
 heap_t heap) {
  d5_st_t * result;

  comm_getmem(data, &D5_SH_BLOCK_DATA[ref.bid], sizeof(d5_bl_data_t), ref.pe);
  const uint32_t size = sizeof(d5_st_t) * data->no_st;
  if(blk) {
    result = blk;
  } else {
    result = mem_alloc(heap, size);
  }
  comm_getmem(result, D5_SH_BLOCK + data->pos, size, ref.pe);
  if(ref.pe != D5.pe_me) {
    context_incr_stat(STAT_BLOCK_PULLS, 0, 1);
  }
  return result;
}


/**
 * @d5_update_block
 */
void
d5_update_block
(d5_bl_ref_t ref,
 d5_bl_data_t data,
 d5_st_t * blk) {
  comm_putmem
    (D5_SH_BLOCK + data.pos, blk, data.no_st * sizeof(d5_st_t), ref.pe);
}


/**
 * @d5_get_block
 */
d5_st_t *
d5_get_block
(d5_bl_ref_t ref,
 bool_t pull,
 bool_t cache,
 d5_bl_data_t * data) {
  d5_st_t * result = NULL;
  if(D5.pe_me == ref.pe) {
    *data = D5_SH_BLOCK_DATA[ref.bid];
    return (d5_st_t *) (D5_SH_BLOCK + data->pos);
  }
  data->ref = ref;
  if(hmap_lookup(D5.blk_cache, data, &result)) {
    return result;
  }
  if(!hmap_lookup(D5.blk_map, data, &result)) {
    if(pull) {
      result = d5_pull_block(ref, data, NULL, D5.blk_heap);
      for(int i = 0; i < data->no_st; i ++) {
        result[i].flags = 0;
      }
      hmap_insert(D5.blk_map, data, &result, NULL);
      if(cache) {
        hmap_insert(D5.blk_cache, data, &result, NULL);
      }
    }
  }
  return result;
}


/**
 * @d5_traversal
 *
 * traverse the tree for step step (D5_STEP_EXP or D5_STEP_DD).  call
 * hook on all states tagged with the D5_FLAG_PROC flag for this step
 */
void
d5_traversal
(int step,
 d5_st_hook_t hook) {
  typedef struct state_stack_item_t {
    d5_st_t * blk;  /*  block of states  */
    d5_bl_ref_t blk_ref;
    uint32_t idx;  /*  index of the state in this block  */
    uint8_t c;  /*  number of children already processed  */
    state_t s;  /*  the state  */
    list_t en;  /*  its enabled events  */
    bool_t proc; /*  has the node been processed?  */
    mem_size_t pos;
  } state_stack_item_t;
  state_stack_item_t stack[D5.depth + 2];
  int32_t d = 0;
  d5_st_t * blk;
  heap_t heap;
  d5_bl_data_t blk_data;

  memset(stack, 0, (D5.depth + 2) * sizeof(state_stack_item_t));

#define d5_traversal_push_item(_s, _blk, _blk_ref, _idx)    {   \
    stack[d].pos = heap_get_position(heap);                     \
    stack[d].idx = _idx;                                        \
    stack[d].blk = _blk;                                        \
    stack[d].blk_ref = _blk_ref;                                \
    stack[d].s = _s;                                            \
    stack[d].en = state_events(stack[d].s, heap);               \
    stack[d].c = 0;                                             \
    stack[d].proc = FALSE;                                      \
  }

  blk = d5_get_block(D5_B0, FALSE, FALSE, &blk_data);
  if(
    !blk ||
    (!d5_get_flag(blk[0].flags, step, D5_FLAG_PROC) &&
     !d5_get_flag(blk[0].flags, step, D5_FLAG_VISIT))
  ) {
    return;
  }

  heap = local_heap_new();
  d5_traversal_push_item(state_initial(heap), blk, D5_B0, 0);

  while(d >= 0) {
    const state_stack_item_t top = stack[d];
    d5_st_t * st = &top.blk[top.idx];

    /* the top stack state must be processed and has not been */
    uint8_t flags = st->flags;
    if(!top.proc && d5_get_flag(st->flags, step, D5_FLAG_PROC)) {
      const d5_st_ref_t top_ref = {.idx = top.idx, .blk = top.blk_ref};
      hook(top_ref, stack[d].s, stack[d].en);
      stack[d].proc = TRUE;
    }

    if(stack[d].c >= st->no_children) {

      /* all children visited => pop */
      heap_set_position(heap, stack[d].pos);
      d5_unset_flag(st->flags, step, D5_FLAG_PROC);
      d5_unset_flag(st->flags, step, D5_FLAG_VISIT);
      d --;

    } else {

      /* some child state remains to be visited */
      d5_st_ref_t fst_child;
      d5_st_t child;

      stack[d].c ++;

      /* get the child state and push it if it must be visited or
         processed */
      d5_uncomp_st_ref(st->fst_child, fst_child);
      if(d5_bl_ref_equal(fst_child.blk, stack[d + 1].blk_ref)) {
        blk = stack[d + 1].blk;
      } else {
        blk = d5_get_block(fst_child.blk, TRUE, FALSE, &blk_data);
      } 
      LNA_ASSERT(blk, D5_ERR_BLOCK_INVALID);
      child = blk[fst_child.idx + stack[d].c - 1];
      if(
        d5_get_flag(child.flags, step, D5_FLAG_PROC) ||
        d5_get_flag(child.flags, step, D5_FLAG_VISIT)
      ) {
        const event_t e = * (event_t *) list_nth(stack[d].en, child.evt);
        const state_t succ = state_succ(stack[d].s, e, heap);
        const uint32_t idx = fst_child.idx + stack[d].c - 1;
        d ++;
        d5_traversal_push_item(succ, blk, fst_child.blk, idx);
        context_incr_stat(STAT_EVENT_EXEC, 0, 1);
      }
    }
  }
  heap_free(heap);
}


/**
 * @d5_dd_hook
 */
void
d5_dd_hook
(d5_st_ref_t s_ref,
 state_t s,
 list_t en) {
  const uint16_t size = state_char_size(s);
  const mem_size_t heap_pos = heap_get_position(D5.cd_heap);
  hset_mdata_t mdata;
  d5_cd_t cd = {
    .pe = D5.pe_me,
    .size = size,
    .s = mem_alloc(D5.cd_heap, size),
    .hash = state_hash(s)
  };
  state_serialise(s, cd.s, &cd.size);
  if(hset_insert(D5.recons, &cd, &mdata)) {
    darray_push(D5.recons_by_pe[d5_get_dp(cd.hash)], &mdata.ref);
  } else {
    heap_set_position(D5.cd_heap, heap_pos);
  }
}


/**
 * @d5_build_exp_tree
 */
void
d5_build_exp_tree
() {
  list_iter_t it;
  list_for_each(D5.new_bl, it) {
    d5_bl_data_t fdata;
    d5_bl_ref_t blk_ref = * ((d5_bl_ref_t *) list_iter_item(it));
    d5_get_block(blk_ref, TRUE, FALSE, &fdata);
    for(int idx = 0; idx < fdata.no_st; idx ++) {
      d5_st_ref_t ref = {.blk = blk_ref, .idx = idx};
      d5_flag_for_step(ref, D5_FLAG_EXP_PROC, D5_FLAG_EXP_VISIT);
    }
  }
}


/**
 * @d5_push_new_block
 *
 * publish a block of new states in D5_SH_BLOCK and D5_SH_BLOCK_DATA
 */
void d5_push_new_block
(d5_bid_t bid,
 uint32_t no_st,
 d5_st_t * st) {
#define d5_push_new_block_update_father_block() {                    \
    d5_bl_data_t fdata;                                                 \
    d5_st_t tmp_blk[CFG_D5_BLOCK_SIZE];                                 \
    while(shmem_int_cswap(&D5_SH_LOCK, 0, 1, fref_prev.blk.pe));        \
    d5_pull_block(fref_prev.blk, &fdata, tmp_blk, NULL);                \
    for(int i = 0; i < sizeof(d5_st_t) * fdata.no_st; i ++) {           \
      ((char *) tmp_blk)[i] ^= ((char *) fblk)[i];                      \
    }                                                                   \
    d5_update_block(fref_prev.blk, fdata, tmp_blk);                     \
    shmem_int_cswap(&D5_SH_LOCK, 1, 0, fref_prev.blk.pe);               \
  }
  
  const d5_bl_ref_t nblk_ref = {.pe = D5.pe_me, .bid = bid};
  const uint32_t nblk_sizeof = no_st * sizeof(d5_st_t);
  d5_st_ref_t fref_prev;
  d5_st_t fblk[CFG_D5_BLOCK_SIZE];

  /* copy the block data and the block content to D5_SH_BLOCK_DATA
     and D5_SH_BLOCK */
  memset(&D5_SH_BLOCK_DATA[bid], 0, sizeof(d5_bl_data_t));
  D5_SH_BLOCK_DATA[bid].ref = nblk_ref;
  D5_SH_BLOCK_DATA[bid].no_st = no_st;
  D5_SH_BLOCK_DATA[bid].pos = D5.next_block_pos;
  memcpy(D5_SH_BLOCK + D5.next_block_pos, st, nblk_sizeof);
  D5.next_block_pos += nblk_sizeof;
    
  for(int i = 0; i < no_st; i ++) {
    const d5_st_ref_t ref = {.blk = nblk_ref, .idx = i};
    d5_st_ref_t fref;
 
    d5_uncomp_st_ref(st[i].father, fref);

    /* check if the block of the father of this state is not the
       same as the block of the father of the previous state */
    if(0 == i || !d5_bl_ref_equal(fref.blk, fref_prev.blk)) {
      if(0 != i) {
        d5_push_new_block_update_father_block();
      }
      memset(fblk, 0, sizeof(fblk));
    }

    /* check if the father of this state is not the same as the
       previous one => update fst_child of the current father */
    if(0 == i || !d5_st_ref_equal(fref, fref_prev)) {
      d5_comp_st_ref(ref, fblk[fref.idx].fst_child);
    }
      
    fblk[fref.idx].no_children ++;
    fref_prev = fref;
  }
  d5_push_new_block_update_father_block();
  context_incr_stat(STAT_STATES_STORED, 0, no_st);
}


/**
 * @d5_insert_new_states
 *
 * called after duplicate detection.  insert in the shared heap new
 * blocks of states.  also inserts in D5.new_refs all hash values and
 * state references of the new states
 */
void d5_insert_new_states
() {
  int i = 0;
  d5_bl_ref_t new_blk = {.pe = D5.pe_me};
  d5_st_ref_t fref;
  const uint32_t no_cand = darray_size(D5.cd_idx);
  uint32_t no_st = 0;
  d5_st_t st[CFG_D5_BLOCK_SIZE];

  while(i < no_cand) {
    d5_cd_t * cd;
    uint8_t no_children = 0;
    int j = i;
    hset_ref_t href;

    darray_get(D5.cd_idx, i, &href);
    cd = hset_get_ptr(D5.cd, href);
    LNA_ASSERT(cd, D5_ERR_CAND_NOT_FOUND);
    fref = cd->father;
      
    /* count the number of new children the father has */
    while(j < no_cand) {
      darray_get(D5.cd_idx, j, &href);
      cd = hset_get_ptr(D5.cd, href);
      LNA_ASSERT(cd, D5_ERR_CAND_NOT_FOUND);
      if(!d5_st_ref_equal(fref, cd->father)) {
        break;
      }
      if(!cd->del) {
        no_children ++;
      }
      j ++;
    }

    /* if adding these new children states to the current block would
       exceed CFG_D5_BLOCK_SIZE, we first append a new block */
    if(no_children + no_st > CFG_D5_BLOCK_SIZE) {
      new_blk.bid = D5.next_block_id ++;
      list_append(D5.new_bl, &new_blk);
      d5_push_new_block(new_blk.bid, no_st, st);
      no_st = 0;
    }

    /* insert these children in the block */
    for(; i < j; i ++) {
      darray_get(D5.cd_idx, i, &href);
      cd = hset_get_ptr(D5.cd, href);
      LNA_ASSERT(cd, D5_ERR_CAND_NOT_FOUND);
      if(!cd->del) {
        d5_st_t new;
        d5_href_t href = {
          .hash = cd->hash,
          .ref = {
            .blk = {.bid = D5.next_block_id, .pe = D5.pe_me},
            .idx = no_st
          },
          .pe = D5.pe_me
        };

        memset(&new, 0, sizeof(d5_st_t));
        d5_comp_st_ref(fref, new.father);
        new.evt = cd->evt;
        st[no_st] = new;
        darray_push(D5.new_refs[d5_get_dp(cd->hash)], &href);
        no_st ++;
      }
    }
  }

  /* append block with overflowing states, if any */
  if(no_st > 0) {
    new_blk.bid = D5.next_block_id ++;
    list_append(D5.new_bl, &new_blk);
    d5_push_new_block(new_blk.bid, no_st, st);
  }
}


/**
 * @d5_exp_hook
 *
 * hook called on each state s of the last BFS level.  insert all
 * successor states in D5.cd
 */
void d5_exp_hook
(d5_st_ref_t ds,
 state_t s,
 list_t en) {
  list_iter_t it;
  uint8_t num = 0;
  const uint32_t en_size = list_size(en);
  
  heap_reset(D5.exp_heap);
  if(en_size + hset_num_items(D5.cd) > CFG_D5_CAND_SET_SIZE) {
    d5_step_dd(FALSE);
  }
  
  list_for_each(en, it) {
    const mevent_t e = * ((event_t *) list_iter_item(it));
    const mem_size_t cd_pos = heap_get_position(D5.cd_heap);
    const state_t succ = state_succ(s, e, D5.exp_heap);
    uint16_t size = state_char_size(succ);
    char * csucc = mem_alloc(D5.cd_heap, size);
    d5_cd_t c = {
      .evt = num,
      .del = TRUE,
      .pe = D5.pe_me,
      .father = ds,
      .size = size,
      .hash = state_hash(succ),
      .s = csucc
    };
    hset_mdata_t mdata;

    state_serialise(succ, csucc, &size);
    if(!hset_insert(D5.cd, &c, &mdata)) {
      heap_set_position(D5.cd_heap, cd_pos);
    } else {
      d5_cd_t * new = hset_get_ptr(D5.cd, mdata.ref);
      new->ref = mdata.ref;
      darray_push(D5.cd_idx, &mdata.ref);
      darray_push(D5.cd_idx_by_pe[d5_get_dp(mdata.hash)], &mdata.ref);
    }
    num ++;
  }

  /* update statistics */
  context_incr_stat(STAT_STATES_PROCESSED, 0, 1);
  context_incr_stat(STAT_EVENT_EXEC, 0, en_size);
  if(0 == en_size) {
    context_incr_stat(STAT_STATES_DEADLOCK, 0, 1);
  }
}


/**
 * @d5_balance
 *
 * this function rebalances the blocks of the last generated BFS
 * level between PEs
 */
void d5_balance
() {
  /* publish the number of new blocks and the id of the first new
     block */
  d5_new_blocks_data_t data_pes[D5.pes];
  data_pes[D5.pe_me].no = list_size(D5.new_bl);
  if(data_pes[D5.pe_me].no > 0) {
    data_pes[D5.pe_me].fst = * ((d5_bl_ref_t *) list_first(D5.new_bl));
  }
  memcpy(&D5_SH_NEW_BLOCKS_DATA, &data_pes[D5.pe_me],
         sizeof(d5_new_blocks_data_t));

  comm_barrier();

  /* retrieve the data published by peers */
  uint32_t tot = 0;
  for(int pe = 0; pe < D5.pes; pe ++) {
    if(pe != D5.pe_me) {
      comm_getmem
        (&data_pes[pe], &D5_SH_NEW_BLOCKS_DATA,
         sizeof(d5_new_blocks_data_t), pe);
    }
    tot += data_pes[pe].no;
  }
  const uint32_t load_base = tot / D5.pes;
  uint32_t load[D5.pes];
  for(int pe = 0; pe < D5.pes; pe ++) {
    load[pe] = load_base;
  }
  tot = tot - load_base * D5.pes;
  for(int i = 0; i < D5.pes; i ++) {
    int pe = (D5.pe_most + i) % D5.pes;
    if(tot > 0) {
      load[pe] ++;
      tot --;
    }
  }
  uint32_t before = 0;
  for(int pe = 0; pe < D5.pe_me; pe ++) {
    before += load[pe];
  }
  assert(tot == 0);

  uint32_t still = load[D5.pe_me];
  list_reset(D5.new_bl);
  for(int pe = 0; still != 0 && pe < D5.pes; pe ++) {
    if(before >= data_pes[pe].no) {
      before -= data_pes[pe].no;
    } else {
      for(uint32_t i = before; still != 0 && i < data_pes[pe].no; i ++) {
        d5_bl_ref_t new = {.pe = pe, .bid = i + data_pes[pe].fst.bid};
        list_append(D5.new_bl, &new);
        still --;
      }
      before = 0;
    }
  }
  D5.pe_most = (D5.pe_most + 1) % D5.pes;
}


/**
 * @d5_process_level
 */
void d5_process_level
() {  
  hmap_empty(D5.blk_map);
  //heap_reset(D5.blk_heap);
  d5_build_exp_tree();
  list_reset(D5.new_bl);
  comm_barrier();
  d5_traversal(D5_STEP_EXP, d5_exp_hook);
  while(d5_step_dd(TRUE));
  d5_balance();
}


/**
 * @d5_check_local_termination
 */
bool_t
d5_check_local_termination
() {
  return list_is_empty(D5.new_bl);
}


/**
 * @d5_tag_for_dd
 */
void
d5_tag_for_dd
(d5_st_ref_t st_ref) {
  d5_flag_for_step(st_ref, D5_FLAG_DDD_PROC, D5_FLAG_DDD_VISIT);
}


/**
 * @d5_initialisation
 */
void d5_initialisation
() {
  const uint32_t block_size = CFG_HASH_SIZE * sizeof(d5_st_t);
  
  d5_common_initialisation();
  D5.next_block_id = 0;
  D5.next_block_pos = 0;
  D5.pe_most = 0;
  D5.new_bl = list_new(NULL, sizeof(d5_bl_ref_t), NULL);
  D5.blk_heap = local_heap_new();
  D5.bwidth_pe = (uint8_t) ceil(log2(D5.pes));
  D5.bwidth_idx = (uint8_t) ceil(log2(CFG_D5_BLOCK_SIZE));
  D5.bwidth_bid = CHAR_BIT * D5_COMP_DS_CHAR_WIDTH;
  D5.bwidth_bid -= (D5.bwidth_pe + D5.bwidth_idx);
  D5.blk_map = hmap_new
    (sizeof(d5_bl_data_t),
     sizeof(d5_st_t *),
     100000,
     FALSE,
     &d5_bl_data_hash,
     &d5_bl_data_equal);
  D5.blk_cache = hmap_new
    (sizeof(d5_bl_data_t),
     sizeof(d5_st_t *),
     100000,
     FALSE,
     &d5_bl_data_hash,
     &d5_bl_data_equal);
  
  COMM_MALLOC(D5_SH_BLOCK, block_size);

  /* PE 0 computes and stores the initial state */
  if(0 == D5.pe_me) {
    d5_href_t href;
    state_t s0 = state_initial(NULL);
    d5_bl_ref_t b0_ref = {.pe = 0, .bid = 0};

    memset(&href, 0, sizeof(d5_href_t));
    memset(&D5_SH_BLOCK_DATA[0], 0, sizeof(d5_bl_data_t));
    D5.next_block_id = 1;
    D5.next_block_pos = sizeof(d5_st_t);
    D5_SH_BLOCK_DATA[0].no_st = 1;
    href.hash = state_hash(s0);
    hset_insert(D5.hash, &href, NULL);
    state_free(s0);
    list_append(D5.new_bl, &b0_ref);
    context_incr_stat(STAT_STATES_STORED, 0, 1);
  }
  comm_barrier();
}


/**
 * @d5_finalisation
 */
void
d5_finalisation
() {
  d5_common_finalisation();
  list_free(D5.new_bl);
  shmem_free(D5_SH_BLOCK);
  heap_free(D5.blk_heap);
  hmap_free(D5.blk_map);
  hmap_free(D5.blk_cache);
}

#endif
