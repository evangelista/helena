/**
 * @file d5.h
 * @brief Implementation of the distributed delta DDD algorithm.
 * @date 17 nov 2020
 * @author Sami Evangelista
 */

#ifndef LIB_D5
#define LIB_D5

#include "includes.h"
#include "hset.h"
#include "heap.h"
#include "darray.h"
#include "state.h"
#include "hmap.h"

/**
 * @brief d5
 */
void d5
();


#define D5_ERR_CAND_NOT_FOUND "candidate not found"
#define D5_ERR_TOO_MUCH_HASHES_RECEIVED "too much hash values received"
#define D5_ERR_BUFFER_TOO_SMALL "config-parameter D5_BUFFER_SIZE too small"


/**
 *  flags manipulation
 */
#define D5_STEP_EXP 0
#define D5_STEP_DDD 1
#define D5_FLAG_PROC 0
#define D5_FLAG_VISIT 1

#define D5_FLAG_EXP_PROC  1
#define D5_FLAG_EXP_VISIT 2
#define D5_FLAG_DDD_PROC  4
#define D5_FLAG_DDD_VISIT 8

#define d5_get_flag(flags, step, flag)          \
  ((flags) & 1 << (((step << 1) + flag)))
#define d5_set_flag(flags, step, flag)          \
  flags |= 1 << (((step << 1) + flag));
#define d5_unset_flag(flags, step, flag)        \
  flags &= ~ (1 << ((step << 1) + flag));


/*  meta data of an array of items pushed in the public space  */
typedef struct {
  uint32_t pos;
  uint32_t no;
  uint32_t size;
} d5_box_t;

typedef uint8_t d5_evt_num_t;

typedef uint8_t d5_st_flags_t;

typedef uint16_t d5_pid_t;

typedef struct {
  int level;
  int glob;
} d5_term_data_t;


/**
 *  typedefs specific for the load-balancing strategy
 */
#if CFG_D5_LB == 1

#define D5_COMP_DS_CHAR_WIDTH 5

typedef uint32_t d5_bid_t;

typedef uint16_t d5_ds_idx_t;

/*  compressed reference to a delta state  */
typedef char d5_cst_ref_t[D5_COMP_DS_CHAR_WIDTH];

/*  reference to a block  */
typedef struct {
  d5_pid_t pe;
  d5_bid_t bid;
} d5_bl_ref_t;

/*  reference to a delta state  */
typedef struct {
  d5_bl_ref_t blk;
  d5_ds_idx_t idx;
} d5_st_ref_t;

/*  delta state  */
typedef struct __attribute__((__packed__)) {
  uint8_t flags;
  uint8_t no_children;
  d5_evt_num_t evt;
  d5_cst_ref_t fst_child;
  d5_cst_ref_t father;
} d5_st_t;

/*  data associated to a block  */
typedef struct {
  d5_bl_ref_t ref;
  uint32_t no_st;
  uint32_t pos;
} d5_bl_data_t;

/*  a block of delta states  */
typedef d5_st_t d5_bl_t[CFG_D5_BLOCK_SIZE];

/*  data on new state blocks  */
typedef struct {
  uint32_t no;
  d5_bl_ref_t fst;
} d5_new_blocks_data_t;


/**
 *  typedefs specific for the work-stealing strategy
 */
#elif CFG_D5_WS == 1

#define D5_RO_NO_BLOCK (1 + CFG_D5_RO_SET_SIZE / CFG_D5_RO_BLOCK_SIZE)

typedef uint32_t d5_tree_id_t;

typedef uint32_t d5_st_lref_t;

typedef struct {
  d5_tree_id_t tr;
  d5_st_lref_t st;
} d5_st_ref_t;

typedef struct __attribute__((__packed__)) {
  d5_st_lref_t father;
  d5_st_lref_t fst_child;
  d5_evt_num_t evt;
  d5_st_flags_t flags;
  uint8_t no_children;
} d5_st_t;

typedef struct {
  uint32_t idx_fst_leaf;
  state_t root;
  darray_t nodes;
} d5_tree_t;

typedef struct {
  int level_done;
  int first_avail;
  int last_avail;
  int avail[D5_RO_NO_BLOCK];
  d5_box_t data[D5_RO_NO_BLOCK];
  uint32_t nxt_blk_idx;
  uint32_t size;
} d5_ro_data_t;

typedef darray_t d5_forest_t;

typedef struct {
  uint64_t open_now;
  uint64_t open_next;
} d5_stat_data_t;

typedef enum
  {
   D5_DD_STEAL,
   D5_DD_TIME
  } d5_dd_strategy_t;

typedef enum
  {
   D5_STEAL_INFO,
   D5_STEAL_MEM,
   D5_STEAL_NEIGH,
   D5_STEAL_RING,
   D5_STEAL_RND,
   D5_STEAL_FRND
  } d5_steal_strategy_t;

#endif


typedef struct __attribute__((__packed__)) {
  hkey_t hash;
  d5_st_ref_t ref;
  d5_pid_t pe;
} d5_href_t;


/*  candidate state  */
typedef struct {
  uint16_t size;
  char * s;
  d5_st_ref_t father;
  bool_t del;
  hkey_t hash;
  d5_evt_num_t evt;
  d5_pid_t pe;
  hset_ref_t ref;
} d5_cd_t;


/*  state hook  */
typedef void (* d5_st_hook_t) (d5_st_ref_t, state_t, list_t);


/**
 *  functions common to both strategy
 */
hkey_t d5_cd_hash(void * cand);
char d5_cd_equal(void * a, void * b);
char * d5_get_array(d5_box_t * dst_data, d5_box_t * src_data,
                    char * dst, char * src, int pe);

void d5_exchange_candidates();
void d5_exchange_to_reconstruct();
void d5_exchange_reconstructed();
void d5_exchange_new_states();
void d5_exchange_new_hashes();
void d5_insert_new_states();
void d5_traversal(int step, d5_st_hook_t hook);
void d5_dd_hook(d5_st_ref_t s_ref, state_t s, list_t en);
bool_t d5_step_dd(bool_t level_done);
void d5_process_level();
void d5_initialisation();
void d5_finalisation();
void d5_common_initialisation();
void d5_common_finalisation();
bool_t d5_check_local_termination();
void d5_tag_for_dd(d5_st_ref_t st);
int d5_get_dp(hkey_t hash);


typedef struct {
  int pes;
  int pe_me;
  int pe_prev;
  int pe_next;
  uint32_t depth;
  char * buf;
  hset_t hash;
  hset_t cd;
  hset_t recons;
  darray_t cd_idx;
  darray_t * cd_idx_by_pe;
  darray_t * recons_by_pe;
  darray_t * refs;
  darray_t * new_refs;
  heap_t cd_heap;
  heap_t exp_heap;
  char * sh_buf[2];
  d5_box_t * sh_box[2];
  int here;
  clock_t dd_last_time;
  lna_timer_t dd_timer;
  bool_t term;
#if CFG_D5_LB == 1
  int pe_most;
  uint8_t bwidth_pe;
  uint8_t bwidth_bid;
  uint8_t bwidth_idx;
  list_t new_bl;
  d5_bid_t next_block_id;
  uint32_t next_block_pos;
  bool_t balanced;
  hmap_t blk_cache;
  hmap_t blk_map;
  heap_t blk_heap;
#elif CFG_D5_WS == 1
  d5_forest_t forest;
  int last_victim;
  int steal_attempts;
  uint32_t no_ro;
  rseed_t rseed;
  d5_steal_strategy_t steal_strategy;
  d5_dd_strategy_t dd_strategy;
#endif
} d5_glob_data_t;


#define d5_check_all_here()  {                                          \
    int here;                                                           \
    comm_barrier();                                                     \
    shmem_int_finc(&D5.here, 0);                                        \
    comm_barrier();                                                     \
    shmem_int_get(&here, &D5.here, 1, 0);                               \
    LNA_ASSERT(here == D5.pes,                                          \
               "%d PEs are here while %d are expected \n",              \
               here, D5.pes);                                           \
    comm_barrier();                                                     \
    D5.here = 0;                                                        \
    comm_barrier();                                                     \
  }


extern d5_glob_data_t D5;

#endif
