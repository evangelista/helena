#include "comm.h"
#include "config.h"
#include "context.h"
#include "common.h"
#include "debug.h"


#if CFG_DISTRIBUTED == 1
#include "shmem.h"
#endif

size_t COMM_HEAP_SIZE;
void * COMM_HEAP = NULL;
int COMM_ME;
int COMM_PES;
lna_timer_t COMM_BARRIER_TIMER;

void init_comm
() {
#if CFG_DISTRIBUTED == 1
  LNA_DEBUG("init_comm started\n");
  shmem_init();
  COMM_ME = shmem_my_pe();
  COMM_PES = shmem_n_pes();
  lna_timer_init(&COMM_BARRIER_TIMER);
  LNA_DEBUG("init_comm done\n");
#endif
}

void finalise_comm
() {
#if CFG_DISTRIBUTED == 1
  LNA_DEBUG("finalise_comm_comm started\n");
  if(COMM_HEAP) {
    shmem_free(COMM_HEAP);
  }
  shmem_finalize();
  LNA_DEBUG("finalise_comm done\n");         
#endif
}

void * comm_malloc
(size_t heap_size) {
#if CFG_DISTRIBUTED == 1
  LNA_DEBUG("allocating heap\n");
  COMM_MALLOC(COMM_HEAP, heap_size);
  return COMM_HEAP;
#else
  return NULL;
#endif
}

int comm_me
() {
#if CFG_DISTRIBUTED == 0
  return 0;
#else
  return COMM_ME;
#endif
}

int comm_pes
() {
#if CFG_DISTRIBUTED == 0
  return 1;
#else
  return COMM_PES;
#endif
}

void comm_barrier
() {
#if CFG_DISTRIBUTED == 0
  LNA_UNREACHABLE_CODE();
#else
  lna_timer_start(&COMM_BARRIER_TIMER);
  shmem_barrier_all();
  lna_timer_stop(&COMM_BARRIER_TIMER);
  context_set_stat
    (STAT_TIME_BARRIER, 0, (double) lna_timer_value(COMM_BARRIER_TIMER));
  context_incr_stat(STAT_BARRIERS, 0, 1);
#endif
}

void comm_put
(uint32_t pos,
 void * src,
 int size,
 int pe) {
  comm_putmem(COMM_HEAP + pos, src, size, pe);
}

void comm_putmem
(void * dst,
 void * src,
 int size,
 int pe) {
#if CFG_DISTRIBUTED == 0
  LNA_UNREACHABLE_CODE();
#else
  LNA_DEBUG("put %d bytes at %p to %d\n", size, dst, pe);
  while(size) {
    if(size <= CFG_COMM_BLOCK_SIZE) {
      shmem_putmem(dst, src, size, pe);
      size = 0;
    } else {
      shmem_putmem(dst, src, CFG_COMM_BLOCK_SIZE, pe);
      size -= CFG_COMM_BLOCK_SIZE;
      dst += CFG_COMM_BLOCK_SIZE;
      src += CFG_COMM_BLOCK_SIZE;
    }
  }
  LNA_DEBUG("put done\n");
#endif
}

void comm_get
(void * dst,
 uint32_t pos,
 int size,
 int pe) {
  comm_getmem(dst, COMM_HEAP + pos, size, pe);
}

void comm_getmem
(void * dst,
 void * src,
 int size,
 int pe) {
#if CFG_DISTRIBUTED == 0
  LNA_UNREACHABLE_CODE();
#else
  LNA_DEBUG("get %d bytes at %p from %d\n", size, src, pe);
  while(size) {
    if(size <= CFG_COMM_BLOCK_SIZE) {
      shmem_getmem(dst, src, size, pe);
      size = 0;
    } else {
      shmem_getmem(dst, src, CFG_COMM_BLOCK_SIZE, pe);
      size -= CFG_COMM_BLOCK_SIZE;
      src += CFG_COMM_BLOCK_SIZE;
      dst += CFG_COMM_BLOCK_SIZE;
    }
  }
  LNA_DEBUG("get done\n");
#endif
}
