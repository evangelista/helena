#include "compression.h"
#include "dist_compression.h"
#include "context.h"
#include "observer.h"
#include "config.h"
#include "comm.h"

#define NO_STATS 50

typedef enum
  {
   STAT_TYPE_TIME,
   STAT_TYPE_GRAPH,
   STAT_TYPE_OTHERS
  } stat_type_t;

typedef struct {
  struct timeval start_time;
  struct timeval end_time;
  unsigned int no_workers;
  unsigned long cpu_total;
  unsigned long utime;
  unsigned long stime;
  bool_t keep_searching;
  bool_t error_raised;
  char * error_msg;
  term_state_t term_state;

  /*  for the trace context  */
  bool_t faulty_state_found;
  state_t faulty_state;
  event_list_t trace;
  pthread_mutex_t ctx_mutex;

  /*  statistics field  */
  uint64_t * stat[NO_STATS];
  bool_t stat_set[NO_STATS];

  /*  threads  */
  pthread_t observer;
  pthread_t * workers;
} struct_context_t;

typedef struct_context_t * context_t;

context_t CTX;


void
init_context
() {
  worker_id_t w;
  unsigned int i;
  unsigned int no_workers = CFG_NO_WORKERS;

  CTX = mem_alloc(SYSTEM_HEAP, sizeof(struct_context_t));

  CTX->error_msg = NULL;
  CTX->error_raised = FALSE;

  if(CFG_ACTION_SIMULATE) {
    return;
  }

  /*
   * initialisation of statistic related fields
   */
  for(i = 0; i < NO_STATS; i ++) {
    CTX->stat_set[i] = FALSE;
    CTX->stat[i] = mem_alloc(SYSTEM_HEAP, no_workers * sizeof(double));
    for(w = 0; w < no_workers; w ++) {
      CTX->stat[i][w] = 0.0;
    }
  }

  /*
   * statistics that must be outputed must be set to 0 to appear in
   * the report
   */
  context_set_stat(STAT_STATES_STORED, 0, 0);
  context_set_stat(STAT_STATES_DEADLOCK, 0, 0);
  if(CFG_ACTION_CHECK_LTL) {
    context_set_stat(STAT_STATES_ACCEPTING, 0, 0);
  }

  CTX->no_workers = no_workers;
  CTX->faulty_state_found = FALSE;
  CTX->trace = NULL;
  CTX->keep_searching = TRUE;
  gettimeofday(&CTX->start_time, NULL);
  if(!CFG_ACTION_CHECK) {
    CTX->term_state = TERM_SEARCH_TERMINATED;
  } else {
    if(CFG_HASH_COMPACTION) {
      CTX->term_state = TERM_NO_ERROR;
    } else {
      CTX->term_state = TERM_SUCCESS;
    }
  }
  CTX->cpu_total = 0;
  CTX->utime = 0;
  CTX->stime = 0;
  cpu_usage(&CTX->cpu_total, &CTX->utime, &CTX->stime);

  /*
   * launch the observer thread
   */
  pthread_create(&CTX->observer, NULL, &observer_worker, (void *) CTX);
  CTX->workers = mem_alloc(SYSTEM_HEAP, sizeof(pthread_t) * CTX->no_workers);
  pthread_mutex_init(&CTX->ctx_mutex, NULL);
}

#define context_state_to_xml(s, out) {          \
    fprintf(out, "<state>\n");                  \
    state_to_xml(s, out);                       \
    fprintf(out, "</state>\n");                 \
  }

#define context_event_to_xml(e, out) {          \
    fprintf(out, "<event>\n");                  \
    event_to_xml(e, out);                       \
    fprintf(out, "</event>\n");                 \
  }


void
context_output_trace
(FILE * out) {
  event_t e;
  state_t s = state_initial(SYSTEM_HEAP);
  list_iter_t it;

  if(CTX->trace) {
    if(list_size(CTX->trace) > CFG_MAX_TRACE_LENGTH) {
      fprintf(out, "<traceTooLong/>\n");
    } else {
      context_state_to_xml(s, out);
      list_for_each(CTX->trace, it) {
        e = * ((event_t *) list_iter_item(it));
        if(!event_is_dummy(e)) {
          context_event_to_xml(e, out);
          event_exec(e, s);
          if(CFG_TRACE_FULL) {
            context_state_to_xml(s, out);
          }
        }
      }
      if(CFG_TRACE_EVENTS && !list_is_empty(CTX->trace)) {
        context_state_to_xml(s, out);
      }
    }
  }
  state_free(s);
}


bool_t
context_stat_is_list
(stat_t stat) {
  switch(stat) {
  case STAT_STATES_PROCESSED: return TRUE;
  default: return FALSE;
  }
}


char *
context_stat_xml_name
(stat_t stat) {
  switch(stat) {
  case STAT_STATES_PROCESSED: return "statesProcessed";
  case STAT_STATES_STORED: return "statesStored";
  case STAT_STATES_STORED_DELTA: return "statesStoredDelta";
  case STAT_STATES_STORED_FULL: return "statesStoredFull";
  case STAT_STATES_DEADLOCK: return "statesTerminal";
  case STAT_STATES_ACCEPTING: return "statesAccepting";
  case STAT_ARCS: return "arcs";
  case STAT_DUPLICATE_DETECTIONS: return "duplicateDetections";
  case STAT_BFS_LEVELS: return "bfsLevels";
  case STAT_EVENT_EXEC: return "eventsExecuted";
  case STAT_SHMEM_COMMS: return "shmemComms";
  case STAT_AVG_CPU_USAGE: return "avgCPUUsage";
  case STAT_MAX_RAM_USAGE: return "maxRAMUsage";
  case STAT_TIME_SEARCH: return "searchTime";
  case STAT_TIME_BARRIER: return "barrierTime";
  case STAT_TIME_SLEEP: return "sleepTime";
  case STAT_TIME_DD: return "ddTime";
  case STAT_STEAL_ATTEMPTS: return "stealAttempts";
  case STAT_STEAL_SUCCESSES: return "stealSuccesses";
  case STAT_BLOCK_PULLS: return "blockPulls";
  case STAT_BARRIERS: return "barriers";
  default:
    assert(FALSE);
  }
}


stat_type_t
context_stat_type
(stat_t stat) {
  switch(stat) {
  case STAT_STATES_STORED:
  case STAT_STATES_STORED_DELTA:
  case STAT_STATES_STORED_FULL:
  case STAT_STATES_PROCESSED:
  case STAT_STATES_DEADLOCK:
  case STAT_STATES_ACCEPTING:
  case STAT_ARCS:
  case STAT_BFS_LEVELS: return STAT_TYPE_GRAPH;
  case STAT_EVENT_EXEC:
  case STAT_SHMEM_COMMS:
  case STAT_AVG_CPU_USAGE:
  case STAT_DUPLICATE_DETECTIONS:
  case STAT_STEAL_ATTEMPTS:
  case STAT_STEAL_SUCCESSES:
  case STAT_BLOCK_PULLS:
  case STAT_MAX_RAM_USAGE:
  case STAT_BARRIERS: return STAT_TYPE_OTHERS;
  case STAT_TIME_SEARCH:
  case STAT_TIME_BARRIER:
  case STAT_TIME_SLEEP:
  case STAT_TIME_DD: return STAT_TYPE_TIME;
  default:
    assert(FALSE);
  }
}


void
context_stat_to_xml
(uint8_t stat,
 FILE * out) {
  char * name = context_stat_xml_name(stat);
  double val = context_get_stat(stat);

  fprintf(out, "<%s>", name);
  if(STAT_AVG_CPU_USAGE == stat || STAT_MAX_RAM_USAGE == stat) {
    fprintf(out, "%.2lf", val / 100.0);
  } else if(STAT_TYPE_TIME == context_stat_type(stat)) {
    fprintf(out, "%.2lf", val / 1000000000.0);
  } else {
    fprintf(out, "%lu", (uint64_t) val);
  }
  fprintf(out, "</%s>", name);
}


void
context_stats_to_xml
(stat_type_t stat_type,
 FILE * out) {
  int i;

  for(i = 0; i < NO_STATS; i ++) {
    if(CTX->stat_set[i] && context_stat_type(i) == stat_type) {
      context_stat_to_xml(i, out);
    }
  }
}


void
finalise_context
() {
  FILE * out;
  void * dummy;
  char name[1024], file_name[1024];
  size_t n = 0;
  int i;

  /**
   *  context already finalised
   */
  if(!CTX) {
    return;
  }

  if(!CFG_ACTION_SIMULATE) {
    gettimeofday(&CTX->end_time, NULL);
    context_set_stat(STAT_TIME_SEARCH, 0,
                     duration(CTX->start_time, CTX->end_time));
    CTX->keep_searching = FALSE;
    pthread_join(CTX->observer, &dummy);

    /**
     *  make the report
     */
    if(CFG_DISTRIBUTED) {
      sprintf(file_name, "%s.%d", CFG_REPORT_FILE, context_proc_id());
      out = fopen(file_name, "w");
    } else {
      out = fopen(CFG_REPORT_FILE, "w");
    }
    fprintf(out, "<helenaReport>\n");
    fprintf(out, "<infoReport>\n");
    fprintf(out, "<model>%s</model>\n", model_name());
#if defined(MODEL_HAS_XML_PARAMETERS)
    model_xml_parameters(out);
#endif
    fprintf(out, "<language>%s</language>\n", CFG_LANGUAGE);
    fprintf(out, "<date>%s</date>\n", CFG_DATE);
#if defined(CFG_COMMAND)
    fprintf(out, "<command>%s</command>\n", CFG_COMMAND);
#endif
    fprintf(out, "<filePath>%s</filePath>\n", CFG_FILE_PATH);
    fprintf(out, "</infoReport>\n");
    fprintf(out, "<searchReport>\n");
    fprintf(out, "<action>");
    if(CFG_ACTION_CHECK) {
      fprintf(out, "check");
    } else if(CFG_ACTION_EXPLORE) {
      fprintf(out, "explore");
    }
    fprintf(out, "</action>\n");
    if(strcmp("", CFG_PROPERTY)) {
      char buf[65536];
      xml_encode(CFG_PROPERTY, buf);
      fprintf(out, "<property>%s</property>\n", buf);
    }
    gethostname(name, 1024);
    fprintf(out, "<host>%s (pid = %d)</host>\n", name, getpid());
    fprintf
      (out, "<searchResult>%s</searchResult>\n",
       TERM_STATE_LIMIT_REACHED == CTX->term_state ? "stateLimitReached"
       : TERM_MEMORY_LIMIT_REACHED == CTX->term_state ? "memoryLimitReached"
       : TERM_TIME_LIMIT_REACHED == CTX->term_state ? "timeLimitReached"
       : TERM_INTERRUPTION == CTX->term_state ? "interruption"
       : TERM_SEARCH_TERMINATED == CTX->term_state ? "searchTerminated"
       : TERM_NO_ERROR == CTX->term_state ? "noCounterExample"
       : TERM_SUCCESS == CTX->term_state ? "propertyHolds"
       : TERM_FAILURE == CTX->term_state ? "propertyViolated"
       : TERM_ERROR == CTX->term_state ? "error"
       : "NA");
    if(CTX->term_state == TERM_ERROR && CTX->error_raised && CTX->error_msg) {
      fprintf(out, "<errorMessage>%s</errorMessage>\n", CTX->error_msg);
    }
    fprintf(out, "<searchOptions>\n");
    fprintf(out, "<searchAlgorithm>%s</searchAlgorithm>",
            CFG_ALGO_DFS ? "DFS"
            : CFG_ALGO_BFS ? "BFS"
            : CFG_ALGO_DBFS ? "DBFS"
            : CFG_ALGO_D4 ? "D4"
            : CFG_ALGO_D5 ? "D5"
            : "ERROR");
    fprintf(out,
            "<partialOrder>%s</partialOrder>\n",
            CFG_POR ? "ON" : "OFF");
    if(CFG_POR) {
      fprintf(out,
              "<partialOrderProviso>%s</partialOrderProviso>\n",
              CFG_POR_PROVISO ?
              (CFG_POR_PROVISO_DEST ? "DEST"
               : CFG_POR_PROVISO_SRC ? "SRC"
               : CFG_POR_PROVISO_TSORT ? "TSORT"
               : CFG_POR_PROVISO_TSORT2 ? "TSORT2"
               : "ERROR")
              : "NONE");
    }
    fprintf(out,
            "<hashCompaction>%s</hashCompaction>\n",
            CFG_HASH_COMPACTION ? "ON" : "OFF");
    fprintf(out,
            "<stateCompression>%s</stateCompression>\n",
            CFG_STATE_COMPRESSION ? "ON" : "OFF");
    if(CFG_DISTRIBUTED) {
      fprintf(out,
              "<distributedStateCompression>%s</distributedStateCompression>\n",
              CFG_DISTRIBUTED_STATE_COMPRESSION ? "ON" : "OFF");
    }
    fprintf(out,
            "<randomSuccs>%s</randomSuccs>\n",
            CFG_RANDOM_SUCCS ? "ON" : "OFF");
    if(CFG_HASH_STORAGE || CFG_D4_STORAGE) {
      fprintf(out, "<hashTableSize>%lu</hashTableSize>\n",
              (long unsigned) CFG_HASH_SIZE);
    }
    fprintf(out, "<workers>%d</workers>\n", CTX->no_workers);
    if(CFG_ALGO_D4) {
      fprintf(out, "<candidateSetSize>%d</candidateSetSize>\n",
              CFG_D4_CAND_SET_SIZE);
    }
    if(CFG_ALGO_D5) {
      fprintf(out, "<candidateSetSize>%d</candidateSetSize>\n",
              CFG_D5_CAND_SET_SIZE);
    }
    if(CFG_DISTRIBUTED_STATE_COMPRESSION || CFG_STATE_COMPRESSION) {
      fprintf(out, "<stateCompressionBits>%d</stateCompressionBits>\n",
              CFG_STATE_COMPRESSION_BITS);
    }
    fprintf(out, "</searchOptions>\n");
    fprintf(out, "</searchReport>\n");
    fprintf(out, "<statisticsReport>\n");
    fprintf(out, "<modelStatistics>\n");
    model_xml_statistics(out);
#if defined(MODEL_STATE_SIZE)
    fprintf(out, "<stateSize>%d</stateSize>\n", MODEL_STATE_SIZE);
#endif
#if CFG_STATE_COMPRESSION && defined(MODEL_HAS_STATE_COMPRESSION)
    fprintf(out, "<compressedStateSize>%d</compressedStateSize>\n",
            state_compressed_char_size());
#endif
#if CFG_DISTRIBUTED_STATE_COMPRESSION && defined(MODEL_HAS_STATE_COMPRESSION)
    fprintf(out, "<compressedStateSize>%d</compressedStateSize>\n",
            state_dist_compressed_char_size());
#endif
    fprintf(out, "</modelStatistics>\n");
    fprintf(out, "<timeStatistics>\n");
    context_stats_to_xml(STAT_TYPE_TIME, out);
    fprintf(out, "</timeStatistics>\n");
    fprintf(out, "<graphStatistics>\n");
    context_stats_to_xml(STAT_TYPE_GRAPH, out);
    fprintf(out, "</graphStatistics>\n");
    fprintf(out, "<otherStatistics>\n");
    context_stats_to_xml(STAT_TYPE_OTHERS, out);
    compression_output_statistics(out);
    dist_compression_output_statistics(out);
    fprintf(out, "</otherStatistics>\n");
    fprintf(out, "</statisticsReport>\n");
    if(CTX->term_state == TERM_FAILURE) {
      fprintf(out, "<traceReport>\n");
      if(CFG_TRACE_STATE) {
        fprintf(out, "<traceState>\n");
        context_state_to_xml(CTX->faulty_state, out);
        fprintf(out, "</traceState>\n");
      } else if(CFG_TRACE_FULL) {
        fprintf(out, "<traceFull>\n");
        context_output_trace(out);
        fprintf(out, "</traceFull>\n");
      } else if(CFG_TRACE_EVENTS) {
        fprintf(out, "<traceEvents>\n");
        context_output_trace(out);
        fprintf(out, "</traceEvents>\n");
      }
      fprintf(out, "</traceReport>\n");
    }
    fprintf(out, "</helenaReport>\n");
    fclose(out);

    /**
     *  in distributed mode the report file must be printed to the
     *  standard output so that it can be sent to the main process.
     *  we prefix each line with [xml-PID].  reports are printed one
     *  after the other to avoid mixed lines.
     */
    if(CFG_DISTRIBUTED) {
      struct timespec t = { 0, 10000000 };
      for(i = 0; i < comm_pes(); i ++) {
        comm_barrier();
        if(i == comm_me()) {
          char * buf = NULL;
          out = fopen(file_name, "r");
          while(getline(&buf, &n, out) != -1) {
            printf("[xml-%d] %s", context_proc_id(), buf);
          }
          free(buf);
          fclose(out);
        }
        nanosleep(&t, NULL);
        comm_barrier();
      }
    }

    /**
     *  free everything
     */
    free(CTX->workers);
    if(CTX->trace) {
      list_free(CTX->trace);
    }
    if(CTX->faulty_state_found) {
      state_free(CTX->faulty_state);
    }
    for(i = 0; i < NO_STATS; i ++) {
      free(CTX->stat[i]);
    }
    pthread_mutex_destroy(&CTX->ctx_mutex);
  }
  if(CTX->error_raised) {
    free(CTX->error_msg);
  }
  free(CTX);
  CTX = NULL;
}


void
context_interruption_handler
(int signal) {
  CTX->term_state = TERM_INTERRUPTION;
  CTX->keep_searching = FALSE;
}


void
context_stop_search
() {
  CTX->keep_searching = FALSE;
}


void
context_faulty_state
(state_t s) {
  pthread_mutex_lock(&CTX->ctx_mutex);
  if(CTX->faulty_state) {
    state_free(CTX->faulty_state);
  }
  CTX->faulty_state = state_copy(s, SYSTEM_HEAP);
  CTX->keep_searching = FALSE;
  CTX->term_state = TERM_FAILURE;
  CTX->faulty_state_found = TRUE;
  pthread_mutex_unlock(&CTX->ctx_mutex);
}


void
context_set_trace
(event_list_t trace) {
  pthread_mutex_lock(&CTX->ctx_mutex);
  if(CTX->trace) {
    list_free(CTX->trace);
  }
  CTX->trace = trace;
  CTX->keep_searching = FALSE;
  CTX->term_state = TERM_FAILURE;
  pthread_mutex_unlock(&CTX->ctx_mutex);
}


bool_t
context_keep_searching
() {
  return CTX->keep_searching;
}


uint16_t
context_no_workers
() {
  return CTX->no_workers;
}


pthread_t *
context_workers
() {
  return CTX->workers;
}


void
context_set_termination_state
(term_state_t term_state) {
  CTX->term_state = term_state;
  CTX->keep_searching = FALSE;
}


struct timeval
context_start_time
() {
  return CTX->start_time;
}


term_state_t
context_termination_state
() {
  return CTX->term_state;
}


void
context_error
(char * msg) {
  pthread_mutex_lock(&CTX->ctx_mutex);
  if(CTX->error_raised) {
    free(CTX->error_msg);
  }
  CTX->error_msg = mem_alloc(SYSTEM_HEAP, sizeof(char) * strlen(msg) + 1);
  strcpy(CTX->error_msg, msg);
  if(!CFG_ACTION_SIMULATE) {
    CTX->term_state = TERM_ERROR;
    CTX->keep_searching = FALSE;
  }
  CTX->error_raised = TRUE;
  pthread_mutex_unlock(&CTX->ctx_mutex);
}


void
context_flush_error
() {
  if(CFG_ACTION_SIMULATE) {
    if(CTX->error_raised) {
      mem_free(SYSTEM_HEAP, CTX->error_msg);
    }
    CTX->error_msg = NULL;
  }
  CTX->error_raised = FALSE;
}


bool_t
context_error_raised
() {
  return CTX->error_raised;
}


char *
context_error_msg
() {
  return CTX->error_msg;
}


uint32_t
context_proc_id
() {
  return comm_me();
}


float
context_cpu_usage
() {
  return cpu_usage(&CTX->cpu_total, &CTX->utime, &CTX->stime);
}


void
context_barrier_wait
(pthread_barrier_t * b) {
  pthread_barrier_wait(b);
}


void
context_sleep
(struct timespec t) {
  nanosleep(&t, NULL);
}


void
context_incr_stat
(stat_t stat,
 worker_id_t w,
 double val) {
  CTX->stat[stat][w] += val;
  CTX->stat_set[stat] = TRUE;
}


void
context_set_stat
(stat_t stat,
 worker_id_t w,
 double val) {
  CTX->stat[stat][w] = val;
  CTX->stat_set[stat] = TRUE;
}


double
context_get_stat
(stat_t stat) {
  worker_id_t w;
  double result = 0;

  for(w = 0; w < CTX->no_workers; w ++) {
    result += CTX->stat[stat][w];
  }
  return result;
}


void context_set_max_stat
(stat_t stat,
 worker_id_t w,
 double val) {
  if(val > CTX->stat[stat][w]) {
    CTX->stat[stat][w] = val;
  }
  CTX->stat_set[stat] = TRUE;
}
