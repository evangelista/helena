#include "config.h"
#include "darray.h"
#include "dfs.h"
#include "model.h"
#include "dfs_stack.h"
#include "prop.h"
#include "reduction.h"
#include "workers.h"
#include "debug.h"

#if defined(MODEL_HAS_EVENT_UNDOABLE)
#define dfs_recover_state() {                   \
    dfs_stack_event_undo(ctx->S, ctx->now);     \
  }
#else
#define dfs_recover_state() {                                   \
    if(dfs_stack_store_states(ctx->S)) {                        \
      ctx->now = dfs_stack_top_state(ctx->S, heap);             \
    } else {                                                    \
      ctx->now = ctx->retrieve_state                            \
        (dfs_stack_top(ctx->S), heap, ctx, data);               \
    }                                                           \
  }
#endif

htbl_t DFS_H;


/**
 *  dfs_por_is_new
 */
bool_t
dfs_por_is_new
(mstate_t s) {
#if CFG_ACTION_CHECK_LTL == 1
  return FALSE;
#else
  htbl_meta_data_t mdata;

  htbl_meta_data_init(mdata, s);
  return !htbl_lookup(DFS_H, &mdata);
#endif
}


/**
 * dfs_compute_now_events
 */
void
dfs_compute_now_events
(dfs_context_t * ctx,
 bool_t reduce,
 list_t included,
 bool_t * fully_expanded) {
  const heap_t heap = dfs_stack_current_heap(ctx->S);
  list_t en;
  bool_t reduced = FALSE;
  por_data_t por_data;

  por_data_init(&por_data);
  por_data.is_new_func = &dfs_por_is_new;
  por_data.included = included;
  if(reduce && ctx->por) {
    en = state_events_reduced(ctx->now, &por_data, &reduced, heap);
  } else {
    en = state_events(ctx->now, heap);
  }
  *fully_expanded = !reduced;
  dfs_stack_set_top_events(ctx->S, en);
}


/**
 *  dfs_traversal_push_new_state
 */
void
dfs_traversal_push_new_state
(dfs_context_t * ctx,
 void * data,
 htbl_id_t id,
 bool_t is_new,
 bool_t * fully_expanded) {
  dfs_stack_push(ctx->S, id, ctx->now);
  dfs_compute_now_events(ctx, TRUE, NULL, fully_expanded);
  if(is_new && ctx->new_node_hook) {
    ctx->new_node_hook(id, ctx, data);
  }
}


/**
 *  dfs_stat_new_node_hook
 */
void
dfs_stat_new_node_hook
(htbl_id_t id,
 dfs_context_t * ctx,
 void * data_void) {
  const list_t en = dfs_stack_top_events(ctx->S);
  context_incr_stat(STAT_STATES_STORED, ctx->w, 1);
  if(0 == list_size(en)) {
    context_incr_stat(STAT_STATES_DEADLOCK, ctx->w, 1);
  }
  if(state_accepting(ctx->now)) {
    context_incr_stat(STAT_STATES_ACCEPTING, ctx->w, 1);
  }
}


/**
 *  dfs_traversal
 */
void
dfs_traversal
(worker_id_t w,
 dfs_context_t * ctx,
 void * data,
 htbl_id_t id_root) {
  heap_t heap = local_heap_new();
  htbl_id_t id, id_succ, id_popped = - 1;
  bool_t is_new, state_popped = FALSE, fully_expanded, push;
  event_t e;
  dfs_edge_type_t et;
  state_t copy;

  /**
   *  push either the initial state either the root state (if one is
   *  provided)
   */
  if(htbl_null_id != id_root) {
    ctx->now = ctx->retrieve_state(id_root, heap, ctx, data);
    id = id_root;
    is_new = FALSE;
  } else {
    ctx->now = state_initial(heap);
    ctx->find_or_put(&id, &is_new, ctx, data);
  }
  dfs_traversal_push_new_state(ctx, data, id, is_new, &fully_expanded);
  if(ctx->pre_node_hook) {
    ctx->pre_node_hook(id, fully_expanded, ctx, data);
  }

  /**
   *  main search loop
   */
  while(dfs_stack_size(ctx->S) && context_keep_searching()) {
  loop_start:

    id = dfs_stack_top(ctx->S);

    /**
     *  reinitialise the heap if its current size exceeds
     *  CFG_DFS_MAX_HEAP_SIZE
     */
    if(heap_size(heap) >= CFG_DFS_MAX_HEAP_SIZE) {
      copy = state_copy(ctx->now, SYSTEM_HEAP);
      heap_reset(heap);
      ctx->now = state_copy(copy, heap);
      state_free(copy);
    }

    /**
     *  before handling the state on top of the stack we look at the
     *  state that has just been popped, if any
     */
    if(state_popped) {
      state_popped = FALSE;
      if(ctx->popped_node_hook) {
        ctx->popped_node_hook(id, id_popped, ctx, data);
      }
    }

    /**
     *  1st case for the state on top of the stack: all its events
     *  have been executed => it must be popped
     **/
    if(dfs_stack_top_expanded(ctx->S)) {
      if(ctx->post_node_hook
         && DFS_REVISIT == ctx->post_node_hook(id, ctx, data)) {
        goto loop_start;
      }
      dfs_stack_pop(ctx->S);
      if(dfs_stack_size(ctx->S)) {
        dfs_recover_state();
      }
      state_popped = TRUE;
      id_popped = id;
    }

    /**
     *  2nd case for the state on top of the stack: some of its events
     *  remain to be executed => we execute the next one
     **/
    else {

      /**
       *  get the next event to process on the top state and execute
       *  it
       */
      dfs_stack_pick_event(ctx->S, &e);
      event_exec(e, ctx->now);
      context_incr_stat(STAT_EVENT_EXEC, w, 1);

      /**
       *  try to insert the successor
       */
      ctx->find_or_put(&id_succ, &is_new, ctx, data);

      /**
       *  see if it must be processed and if so, push it
       */
      push = is_new ||
        (ctx->process_check && ctx->process_check(id_succ, ctx, data));
      if(push) {
        dfs_traversal_push_new_state(ctx, data, id_succ, is_new,
                                     &fully_expanded);
        et = DFS_FORWARD_EDGE;
      } else {
        dfs_recover_state();
        et = DFS_CROSS_EDGE;
      }

      /**
       *  the edge hook may tell us to revisit the state (e.g., due to
       *  the por proviso)
       */
      if(ctx->edge_hook
         && DFS_REVISIT == ctx->edge_hook(e, id, et, id_succ, ctx, data)) {
        goto loop_start;
      }

      /**
       *  call the pre-node-hook on the pushed state
       */
      if(push && ctx->pre_node_hook) {
        ctx->pre_node_hook(id_succ, fully_expanded, ctx, data);
      }
    }
  }

  /**
   *  free everything
   */
  heap_free(heap);
}


#if CFG_ALGO_DFS == 0

void
dfs() {
  LNA_UNREACHABLE_CODE();
}

#else

#define DFS_NDFS                                                \
  (CFG_ACTION_CHECK_LTL && CFG_ALGO_DFS && !CFG_PARALLEL)
#define DFS_CNDFS                                               \
  (CFG_ACTION_CHECK_LTL && CFG_ALGO_DFS && CFG_PARALLEL)

const struct timespec DFS_WAIT_RED_SLEEP_TIME = { 0, 10 };

/**
 *  definitions and hooks for the main dfs procedure
 */
typedef struct {
  bool_t accepting;
  htbl_id_t id;
} dfs_red_processed_t;

typedef bool_t (* dfs_state_check_func_t) (state_t, list_t);

typedef struct {
  htbl_t H;
  bool_t blue;
  dfs_state_check_func_t check_func;

  /*  data for (C)NDFS algorithm  */
  darray_t ndfs_red_states;
  uint64_t ndfs_red_stack_size;
  htbl_id_t ndfs_id_seed;
} dfs_main_data_t;


/**
 *  dfs_main_find_or_put
 */
void
dfs_main_find_or_put
(htbl_id_t * sid,
 bool_t * is_new,
 dfs_context_t * ctx,
 void * data_void) {
  dfs_main_data_t * data = (dfs_main_data_t *) data_void;
  htbl_meta_data_t mdata;
  
  htbl_meta_data_init(mdata, ctx->now);
  stbl_insert(data->H, mdata, *is_new);
  *sid = mdata.id;
}


/**
 *  dfs_main_retrieve_state
 */
state_t
dfs_main_retrieve_state
(htbl_id_t id,
 heap_t heap,
 dfs_context_t * ctx,
 void * data_void) {
  dfs_main_data_t * data = (dfs_main_data_t *) data_void;

  return htbl_get(data->H, id, heap);
}


/**
 *  dfs_main_process_check
 */
bool_t
dfs_main_process_check
(htbl_id_t id,
 dfs_context_t * ctx,
 void * data_void) {
  dfs_main_data_t * data = (dfs_main_data_t *) data_void;
  
  if(data->blue) {
    return
      !htbl_get_worker_attr(data->H, id, ATTR_CYAN, ctx->w)
      && !htbl_get_attr(data->H, id, ATTR_BLUE);
  } else {
    return
      !htbl_get_worker_attr(data->H, id, ATTR_PINK, ctx->w)
      && !htbl_get_attr(data->H, id, ATTR_RED);
  }
}


/**
 *  dfs_main_popped_node_hook
 */
void
dfs_main_popped_node_hook
(htbl_id_t id,
 htbl_id_t id_popped,
 dfs_context_t * ctx,
 void * data_void) {
  dfs_main_data_t * data = (dfs_main_data_t *) data_void;

  /**
   *  the POR proviso-dest is on
   *
   *  if the popped state is red then if the top state is orange, it
   *  becomes purple
   */
  if(CFG_POR_PROVISO_DEST
     && data->blue
     && PROVISO_ORANGE == htbl_get_attr(data->H, id, ATTR_PROV_COLOR)
     && PROVISO_RED == htbl_get_attr(data->H, id_popped, ATTR_PROV_COLOR)) {
    htbl_set_attr(data->H, id, ATTR_PROV_COLOR, PROVISO_PURPLE);
  }
}


/**
 *  dfs_main_new_node_hook
 */
void
dfs_main_new_node_hook
(htbl_id_t id,
 dfs_context_t * ctx,
 void * data_void) {
  dfs_main_data_t * data = (dfs_main_data_t *) data_void;
  const list_t en = dfs_stack_top_events(ctx->S);

  /* update statistics */
  if(data->blue) {
    dfs_stat_new_node_hook(id, ctx, data_void);
  }
  
  /*check the state property if defined */
  if(data->check_func && (*(data->check_func))(ctx->now, en)) {
    context_faulty_state(ctx->now);
    dfs_stack_create_trace(ctx->S);
  }

  /* call tsort proviso new-node-hook for the node */
  if((CFG_POR_PROVISO_TSORT || CFG_POR_PROVISO_TSORT2) && data->blue) {
    tsort_proviso_new_node_hook(data->H, id);
  }
}


/**
 *  dfs_main_pre_node_hook
 */
void
dfs_main_pre_node_hook
(htbl_id_t id,
 bool_t fully_expanded,
 dfs_context_t * ctx,
 void * data_void) {
  dfs_main_data_t * data = (dfs_main_data_t *) data_void;
    
  /**
   *  set the different attributes of the pushed state
   */
  if(!data->blue) {
    htbl_set_worker_attr(data->H, id, ATTR_PINK, ctx->w, TRUE);
  } else {
    htbl_set_worker_attr(data->H, id, ATTR_CYAN, ctx->w, TRUE);
    if(htbl_has_attr(data->H, ATTR_PROV_FEXPANDED)) {
      htbl_set_attr(data->H, id, ATTR_PROV_FEXPANDED, ctx->fexpanded);
    }
    if(fully_expanded) {
      ctx->fexpanded ++;
      if(htbl_has_attr(data->H, ATTR_PROV_COLOR)) {
        htbl_set_attr(data->H, id, ATTR_PROV_COLOR, PROVISO_GREEN);
      }
    }
  }

  if(!data->blue) {
    data->ndfs_red_stack_size ++;
  }
}


/**
 *  dfs_main_post_node_hook
 */
dfs_action_t
dfs_main_post_node_hook
(htbl_id_t id,
 dfs_context_t * ctx,
 void * data_void) {
  dfs_main_data_t * data = (dfs_main_data_t *) data_void;
  bool_t fully_expanded;

  /**
   *  the POR proviso-dest is on.  when popped:
   *  - an orange state becomes green
   *  - a green state is unchanged
   *  - a red state should not be possible here
   *  - a purple state:
   *       + becomes fully expanded (and thus green) if marked to revisit
   *       + becomes red otherwise
   */
  if(CFG_POR_PROVISO_DEST && data->blue) {
    switch(htbl_get_attr(data->H, id, ATTR_PROV_COLOR)) {
    case PROVISO_ORANGE:
      htbl_set_attr(data->H, id, ATTR_PROV_COLOR, PROVISO_GREEN);
      break;
    case PROVISO_GREEN:
      break;
    case PROVISO_RED:
      LNA_UNREACHABLE_CODE();
      break;
    case PROVISO_PURPLE:
      if(htbl_get_attr(data->H, id, ATTR_PROV_REVISIT)) {
        htbl_set_attr(data->H, id, ATTR_PROV_COLOR, PROVISO_GREEN);
        dfs_compute_now_events(ctx, FALSE, NULL, &fully_expanded);
        return DFS_REVISIT;
      } else {
        htbl_set_attr(data->H, id, ATTR_PROV_COLOR, PROVISO_RED);
      }
      break;
    }
  }

  /**
   *  the POR proviso-src is on
   */
  if(CFG_POR_PROVISO_SRC && data->blue) {
    switch(htbl_get_attr(data->H, id, ATTR_PROV_COLOR)) {
    case PROVISO_GREEN:
      assert(ctx->fexpanded);
      ctx->fexpanded --;
      break;
    case PROVISO_ORANGE:
      htbl_set_attr(data->H, id, ATTR_PROV_COLOR, PROVISO_GREEN);
      break;
    case PROVISO_PURPLE:
      htbl_set_attr(data->H, id, ATTR_PROV_COLOR, PROVISO_RED);
      break;
    case PROVISO_RED:
      LNA_UNREACHABLE_CODE();
      break;
    }
  }

  /**
   *  we check an ltl property => launch the red search if the state
   *  is accepting
   */
  if(CFG_ACTION_CHECK_LTL
     && data->blue
     && state_accepting(ctx->now)) {
    data->blue = FALSE;
    data->ndfs_id_seed = dfs_stack_top(ctx->S);
    data->ndfs_red_stack_size = 1;
    dfs_compute_now_events(ctx, TRUE, NULL, &fully_expanded);
    if(DFS_CNDFS) {
      darray_reset(data->ndfs_red_states);
    }
    return DFS_REVISIT;
  }

  /**
   *  in (c)ndfs: put new colors on the popped state as it leaves the
   *  stack
   */
 pop_blue:
  if(data->blue) {
    htbl_set_worker_attr(data->H, id, ATTR_CYAN, ctx->w, FALSE);
    htbl_set_attr(data->H, id, ATTR_BLUE, TRUE);
  } else {  /* nested search of ndfs or cndfs */

    /**
     *  in cdnfs we put the popped state in red_states.  in sequential
     *  ndfs we can directly mark it as red.
     */
    if(DFS_CNDFS) {
      dfs_red_processed_t proc = {
        .accepting = state_accepting(ctx->now),
        .id = id
      };
      darray_push(data->ndfs_red_states, &proc);
    } else {
      htbl_set_attr(data->H, id, ATTR_RED, TRUE);
    }
    data->ndfs_red_stack_size --;

    /**
     *  termination of the red DFS.  in cndfs we wait for all accepting
     *  states of the red_states set to become red and then mark all
     *  states of this set as red
     */
    if(0 == data->ndfs_red_stack_size) {
      data->blue = TRUE;
      if(DFS_CNDFS) {
        dfs_red_processed_t proc;
        for(int i = 0; i < darray_size(data->ndfs_red_states); i ++) {
          darray_get(data->ndfs_red_states, i, &proc);
          if(proc.accepting && proc.id != id) {
            while(!htbl_get_attr(data->H, proc.id, ATTR_RED)
                  && context_keep_searching()) {
              context_sleep(DFS_WAIT_RED_SLEEP_TIME);
            }
          }
        }
        for(int i = 0; i < darray_size(data->ndfs_red_states); i ++) {
          darray_get(data->ndfs_red_states, i, &proc);
          htbl_set_attr(data->H, proc.id, ATTR_RED, TRUE);
        }
      }
      goto pop_blue;
    }
  }
  context_incr_stat(STAT_STATES_PROCESSED, ctx->w, 1);

  return DFS_CONTINUE;
}


/**
 *  dfs_main_edge_hook
 */
dfs_action_t
dfs_main_edge_hook
(event_t e,
 htbl_id_t id,
 dfs_edge_type_t etype,
 htbl_id_t id_succ,
 dfs_context_t * ctx,
 void * data_void) {
  bool_t dummy;
  dfs_main_data_t * data = (dfs_main_data_t *) data_void;
  const bool_t check_proviso = data->blue && etype != DFS_FORWARD_EDGE;

  if(data->blue) {
    context_incr_stat(STAT_ARCS, ctx->w, 1);
  }

  /**
   *  if we check an LTL property and are in the red search, test
   *  whether the state reached is the seed.  exit the loop if this is
   *  the case
   */
  if(!data->blue && (id_succ == data->ndfs_id_seed)) {
    dfs_stack_create_trace(ctx->S);
    return DFS_CONTINUE;
  }

  /**
   *  the POR proviso-src is on
   *
   *  if both the source state and the destination state are not green
   *  then the source state must be fully expanded if the destination
   *  state is red or if the number of fully expanded states above the
   *  two states are the same.  
   */
  if(CFG_POR_PROVISO_SRC
     && check_proviso
     && PROVISO_GREEN != htbl_get_attr(data->H, id, ATTR_PROV_COLOR)) {
    const char color_succ = htbl_get_attr(data->H, id_succ, ATTR_PROV_COLOR);
    if(PROVISO_GREEN != color_succ) {
      if(PROVISO_RED != color_succ
         && (htbl_get_attr(data->H, id, ATTR_PROV_FEXPANDED)
             != htbl_get_attr(data->H, id_succ, ATTR_PROV_FEXPANDED))) {
        htbl_set_attr(data->H, id, ATTR_PROV_COLOR, PROVISO_PURPLE);
      } else {
        htbl_set_attr(data->H, id, ATTR_PROV_COLOR, PROVISO_GREEN);
        ctx->fexpanded ++;
        dfs_compute_now_events(ctx, FALSE, NULL, &dummy);
        return DFS_REVISIT;
      }
    }
  }

  /**
   *  the POR proviso-dest is on
   */
  if(CFG_POR_PROVISO_DEST
     && check_proviso
     && PROVISO_GREEN != htbl_get_attr(data->H, id, ATTR_PROV_COLOR)
     && PROVISO_GREEN != htbl_get_attr(data->H, id_succ, ATTR_PROV_COLOR)) {
    htbl_set_attr(data->H, id, ATTR_PROV_COLOR, PROVISO_PURPLE);
    htbl_set_attr(data->H, id_succ, ATTR_PROV_REVISIT, TRUE);
  }

  /**
   *  call the tsort proviso hook for the edge
   */
  if((CFG_POR_PROVISO_TSORT || CFG_POR_PROVISO_TSORT2) && data->blue) {
    tsort_proviso_new_edge_hook(data->H, id, e, id_succ);
  }
  return DFS_CONTINUE;
}


/**
 *  dfs_worker
 */
void *
dfs_worker
(void * arg) {
  const worker_id_t w = (worker_id_t) (unsigned long int) arg;
  const bool_t shuffle = CFG_PARALLEL || CFG_RANDOM_SUCCS;
  const bool_t states_stored =
#if defined(MODEL_HAS_EVENT_UNDOABLE)
    FALSE
#else
    CFG_HASH_COMPACTION
#endif
    ;
  dfs_main_data_t data;
  dfs_context_t ctx;

  if(CFG_POR_PROVISO_TSORT || CFG_POR_PROVISO_TSORT2) {
    tsort_proviso_init_data();
  }

  /**
   *  set the different attributes of the DFS context
   */
  ctx.w = w;
  ctx.por = CFG_POR;
  ctx.S = dfs_stack_new(CFG_DFS_STACK_BLOCK_SIZE, shuffle, states_stored);
  ctx.find_or_put = &dfs_main_find_or_put;
  ctx.retrieve_state = &dfs_main_retrieve_state;
  ctx.process_check = &dfs_main_process_check;
  ctx.new_node_hook = &dfs_main_new_node_hook;
  ctx.pre_node_hook = &dfs_main_pre_node_hook;
  ctx.post_node_hook = &dfs_main_post_node_hook;
  ctx.popped_node_hook = &dfs_main_popped_node_hook;
  ctx.edge_hook = &dfs_main_edge_hook;
  ctx.fexpanded = 0;

  /**
   *  set the different attributes according to the search algorithm
   *  used
   */
  data.H = DFS_H;
  data.blue = TRUE;
  data.check_func =
#if CFG_ACTION_CHECK_SAFETY == 1
    state_check_property
#else
    NULL;
#endif
  ;
  data.ndfs_red_states = DFS_CNDFS ?
    darray_new(SYSTEM_HEAP, sizeof(dfs_red_processed_t)) : NULL;

  dfs_traversal(w, &ctx, &data, htbl_null_id);

  /**
   *  apply tsort proviso
   */
  if(CFG_POR_PROVISO_TSORT || CFG_POR_PROVISO_TSORT2) {
    tsort_proviso(DFS_H, &ctx, &data);
  }

  /**
   *  free data
   */
  if(data.ndfs_red_states) {
    darray_free(data.ndfs_red_states);
  }
  dfs_stack_free(ctx.S);

  return NULL;
}


/**
 *  dfs
 */
void
dfs
() {
  DFS_H = stbl_default_new();
  launch_and_wait_workers(&dfs_worker);
  htbl_free(DFS_H);
}

#endif
