/**
 * @file debug.h
 * @brief Debugging routines.
 * @date 23 feb 2018
 * @author Sami Evangelista
 */

#ifndef LIB_DEBUG
#define LIB_DEBUG

#include "config.h"
#include "comm.h"

#if CFG_DEBUG == 1
#define LNA_DEBUG LNA_DEBUG_MSG
#else
#define LNA_DEBUG(...) {}
#endif

#define LNA_DEBUG_MSG(...)   {                                  \
    fprintf(stderr, "[");                                       \
    if(CFG_DISTRIBUTED) {                                       \
      fprintf(stderr, "pe=%d,pid=%d,", comm_me(), getpid());    \
    }                                                           \
    fprintf(stderr, "%s:%d] ", __FILE__, __LINE__);             \
    fprintf(stderr, __VA_ARGS__);                               \
    fprintf(stderr, "\n");                                      \
  }

#if CFG_ASSERTS == 1
#define LNA_ASSERT(cond, ...) {                     \
    if(!(cond)) {                                   \
      LNA_DEBUG_MSG(__VA_ARGS__);                   \
      assert(0);                                    \
    }                                               \
  }
#else
#define LNA_ASSERT(...) {}
#endif

#define LNA_UNREACHABLE_CODE() LNA_ASSERT(0, "unreachable code")

#endif
