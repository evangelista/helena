/**
 * @file dfs.h
 * @brief Implementation of DFS based algorithms.
 *        Nested DFS algorithms for LTL verification (sequential or multi-core)
 *        are also implemented here.
 * @date 12 sep 2017
 * @author Sami Evangelista
 */

#ifndef LIB_DFS
#define LIB_DFS

#include "includes.h"
#include "context.h"
#include "dfs_stack.h"


/**
 * @brief launch the DFS based algorithm
 */
void dfs
();


typedef enum
  {
   DFS_FORWARD_EDGE,
   DFS_CROSS_EDGE,
   DFS_BACKWARD_EDGE
  } dfs_edge_type_t;

typedef enum
  {
   DFS_REVISIT,
   DFS_CONTINUE
  } dfs_action_t;

struct struct_dfs_context_t;
typedef struct struct_dfs_context_t dfs_context_t;

typedef void (* dfs_find_or_put_func_t)
(htbl_id_t *,
 bool_t *,
 dfs_context_t *,
 void *);

typedef state_t (* dfs_retrieve_state_func_t)
(htbl_id_t,
 heap_t,
 dfs_context_t *,
 void *);

typedef bool_t (* dfs_process_check_func_t)
(htbl_id_t,
 dfs_context_t *,
 void *);

typedef void (* dfs_new_node_hook_func_t)
(htbl_id_t,
 dfs_context_t *,
 void *);

typedef void (* dfs_pre_node_hook_func_t)
(htbl_id_t,
 bool_t,
 dfs_context_t *,
 void *);

typedef dfs_action_t (* dfs_post_node_hook_func_t)
(htbl_id_t,
 dfs_context_t *,
 void *);

typedef void (* dfs_popped_node_hook_func_t)
(htbl_id_t,
 htbl_id_t,
 dfs_context_t *,
 void *);

typedef dfs_action_t (* dfs_edge_hook_func_t)
(event_t,
 htbl_id_t,
 dfs_edge_type_t,
 htbl_id_t,
 dfs_context_t *,
 void *);

struct struct_dfs_context_t {
  worker_id_t w;
  dfs_stack_t S;
  state_t now;
  bool_t por;
  dfs_find_or_put_func_t find_or_put;
  dfs_retrieve_state_func_t retrieve_state;
  dfs_process_check_func_t process_check;
  dfs_new_node_hook_func_t new_node_hook;
  dfs_pre_node_hook_func_t pre_node_hook;
  dfs_post_node_hook_func_t post_node_hook;
  dfs_popped_node_hook_func_t popped_node_hook;
  dfs_edge_hook_func_t edge_hook;
  uint32_t fexpanded;
};


/**
 * @brief generic DFS algorithm
 */
void
dfs_traversal
(worker_id_t w,
 dfs_context_t * ctx,
 void * data,
 htbl_id_t id_root);


void
dfs_stat_new_node_hook
(htbl_id_t id,
 dfs_context_t * ctx,
 void * data_void);

#endif
