/**
 * @file reduction.h
 * @brief Implementation of various reduction methods.
 * @date 12 sep 2017
 * @author Sami Evangelista
 */

#ifndef LIB_REDUCTION
#define LIB_REDUCTION

#include "includes.h"
#include "context.h"
#include "event.h"
#include "stbl.h"
#include "dfs.h"


/**
 * @typedef reduction_algo_t
 */
typedef struct {
  rseed_t seed;
  bool_t init;
  por_algo_t por_algo;
  bool_t por_algo_opt;
  por_trans_choice_t trans_choice;
  por_scapegoat_choice_t scapegoat_choice;
} reduction_algo_t;


/**
 * @typedef por_data_t
 */
typedef struct {
  list_t included;
  bool_t (* is_new_func) (mstate_t);
  bool_t (* is_safe_func) (mstate_t);
} por_data_t;


/**
 * @brief por_data_init
 */
void
por_data_init
(por_data_t * data);


/**
 * @brief mstate_events_reduced
 */
list_t
mstate_events_reduced
(mstate_t s,
 por_data_t * data,
 bool_t * reduced,
 heap_t heap);


/**
 * @brief tsort_proviso_init_data
 */
void
tsort_proviso_init_data
();


/**
 *  @brief tsort_proviso
 */
void
tsort_proviso
(htbl_t H,
 dfs_context_t * ctx,
 void * data);


/**
 *  @brief tsort_proviso_new_node_hook
 */
void
tsort_proviso_new_node_hook
(htbl_t H,
 htbl_id_t id);


/**
 *  @brief tsort_proviso_new_edge_hook
 */
void
tsort_proviso_new_edge_hook
(htbl_t H,
 htbl_id_t id,
 event_t e,
 htbl_id_t id_succ);

#endif
