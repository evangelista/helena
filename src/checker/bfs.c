#include "bfs.h"
#include "list.h"
#include "bfs_queue.h"
#include "config.h"
#include "context.h"
#include "dbfs_comm.h"
#include "stbl.h"
#include "prop.h"
#include "reduction.h"
#include "workers.h"
#include "state.h"
#include "dist_compression.h"
#include "debug.h"

#if CFG_ALGO_BFS == 0 && CFG_ALGO_DBFS == 0

void
bfs
() {
  LNA_UNREACHABLE_CODE();
}

#else

typedef struct {
  htbl_t H;
  bfs_queue_t Q;
  pthread_barrier_t BARRIER;
  bool_t AT_BARRIER;
} bfs_glob_data_t;

bfs_glob_data_t BFS_DATA;


worker_id_t
bfs_thread_owner
(hkey_t h) {
  uint8_t result = 0;
  int i;

  for(i = 0; i < sizeof(hkey_t); i++) {
    result += (h >> (i * 8)) & 0xff;
  }
  return result % CFG_NO_WORKERS;
}


bool_t
bfs_check_termination
(worker_id_t w) {
  bool_t result = FALSE;

  if(!CFG_ALGO_DBFS && !CFG_PARALLEL) {
    result = !context_keep_searching() || bfs_queue_is_empty(BFS_DATA.Q);
  } else if(CFG_ALGO_DBFS) {
    result = dbfs_comm_idle();
  } else {
    if(bfs_queue_is_empty(BFS_DATA.Q) ||
       !context_keep_searching() || BFS_DATA.AT_BARRIER) {
      BFS_DATA.AT_BARRIER = TRUE;
      context_barrier_wait(&BFS_DATA.BARRIER);
      BFS_DATA.AT_BARRIER = FALSE;
      result = !context_keep_searching() || bfs_queue_is_empty(BFS_DATA.Q);
      context_barrier_wait(&BFS_DATA.BARRIER);
    }
  }
  return result;
}


void
bfs_report_trace
(htbl_id_t id) {
  list_t trace = list_new(SYSTEM_HEAP, sizeof(event_t), event_free_void);
  list_t trace_id = stbl_get_trace(BFS_DATA.H, id);
  list_iter_t it;
  state_t s = state_initial(SYSTEM_HEAP);
  event_t e;

  for(it = list_get_iter(trace_id);
      !list_iter_at_end(it);
      it = list_iter_next(it)) {
    e = state_event(s, * ((event_id_t *) list_iter_item(it)), SYSTEM_HEAP);
    event_exec(e, s);
    list_append(trace, &e);
  }
  state_free(s);
  list_free(trace_id);
  context_set_trace(trace);
}

#if defined(MODEL_HAS_EVENT_UNDOABLE)
#define bfs_goto_succ() {event_exec(e, s); succ = s;}
#define bfs_back_to_s() {event_undo(e, s);}
#else
#define bfs_goto_succ() {succ = state_succ(s, e, heap);}
#define bfs_back_to_s() {state_free(succ);}
#endif

#define bfs_enqueue_real(item, from, to) {                              \
    bfs_queue_enqueue(BFS_DATA.Q, item, from, to);                      \
    htbl_set_worker_attr(BFS_DATA.H, item.id, ATTR_CYAN, to, TRUE);     \
  }


bool_t
bfs_no_state_to_process
() {
  return bfs_queue_is_empty(BFS_DATA.Q);
}


void
bfs_enqueue
(bfs_queue_item_t item,
 worker_id_t from,
 worker_id_t to) {
  bfs_enqueue_real(item, from, to);
}


bool_t
bfs_por_is_new
(mstate_t s) {
#if CFG_ACTION_CHECK_LTL == 1
  return FALSE;
#else
  htbl_meta_data_t mdata;

  htbl_meta_data_init(mdata, s);
  return !htbl_lookup(BFS_DATA.H, &mdata);
#endif
}


void *
bfs_worker
(void * arg) {
  const worker_id_t w = (worker_id_t) (unsigned long int) arg;
  const bool_t states_in_queue = bfs_queue_states_stored(BFS_DATA.Q);
  const bool_t with_trace = CFG_ACTION_CHECK_SAFETY && CFG_ALGO_BFS;
  const bfs_queue_t Q = BFS_DATA.Q;
  const uint16_t no_workers = bfs_queue_no_workers(Q);
  const htbl_t H = BFS_DATA.H;
  const bool_t has_safe_attr = htbl_has_attr(BFS_DATA.H, ATTR_PROV_SAFE);
  state_t s, succ;
  htbl_id_t id_succ;
  event_list_t en;
  worker_id_t x;
  unsigned int arcs;
  heap_t heap = local_heap_new();
  bfs_queue_item_t item, succ_item;
  event_t e;
  bool_t mine, is_new, reduced;
  hkey_t h;
  unsigned int dbfs_ctr = CFG_DBFS_CHECK_COMM_PERIOD;
  htbl_meta_data_t mdata;
  por_data_t por_data;

  por_data_init(&por_data);
  por_data.is_new_func = &bfs_por_is_new;
  do {
    for(x = 0; x < no_workers && context_keep_searching(); x ++) {
      while(!bfs_queue_slot_is_empty(Q, x, w) && context_keep_searching()) {

        /**
         * in DBFS with polling strategy we check incoming messages
         * every CFG_DBFS_CHECK_COMM_PERIOD^th state processed
         */
        if(CFG_ALGO_DBFS && CFG_DBFS_POLL && (0 == (-- dbfs_ctr))) {
          dbfs_comm_check_communications();
          if(!context_keep_searching()) {
            goto check_termination;
          }
          dbfs_ctr = CFG_DBFS_CHECK_COMM_PERIOD;
        }

        /**
         * get the next state sent by thread x.  if the states are not
         * stored in the queue we get it from the hash table
         */
        item = bfs_queue_next(Q, x, w);
        heap_reset(heap);
        if(states_in_queue) {
          s = state_copy(item.s, heap);
        } else {
          s = htbl_get(H, item.id, heap);
        }
        htbl_set_worker_attr(H, item.id, ATTR_CYAN, w, FALSE);

        /**
         * compute enabled events and apply POR
         */
        if(!CFG_POR || item.fully_expand) {
          en = state_events(s, heap);
          reduced = FALSE;
        } else {
          en = state_events_reduced(s, &por_data, &reduced, heap);
        }
        if(CFG_POR && CFG_POR_PROVISO && has_safe_attr && !reduced) {
          htbl_set_attr(H, item.id, ATTR_PROV_SAFE, TRUE);
        }

        /**
         * check the state property
         */
        if(CFG_ACTION_CHECK_SAFETY && state_check_property(s, en)) {
          context_faulty_state(s);
          if(with_trace) {
            bfs_report_trace(item.id);
          }
        }

        /**
         * expand the current state and put its unprocessed successors
         * in the queue
         */
      state_expansion:
        arcs = 0;
        while(!list_is_empty(en)) {
          arcs ++;
          list_pick_first(en, &e);
          bfs_goto_succ();
          htbl_meta_data_init(mdata, succ);

          /**
           * in DBFS we check if the successor state is ours.
           * otherwise we go back to s to get the next successor
           */
          if(CFG_ALGO_DBFS) {
            mine = dbfs_comm_process_state(&mdata, !reduced);
            if(!context_keep_searching()) {
              goto check_termination;
            } else if(!mine) {
              bfs_back_to_s();
              continue;
            }
          }

          /**
           * try to insert the successor state in the table
           */
          stbl_insert(H, mdata, is_new);
          id_succ = mdata.id;
          h = mdata.h;

          /**
           * the successor state is new => enqueue it
           */
          if(is_new) {
            succ_item.id = id_succ;
            succ_item.s = succ;
            succ_item.fully_expand = FALSE;
            bfs_enqueue_real(succ_item, w, bfs_thread_owner(h));
            if(with_trace) {
              htbl_set_attr(H, succ_item.id, ATTR_PRED, item.id);
              htbl_set_attr(H, succ_item.id, ATTR_EVT, event_id(e));
            }
            context_incr_stat(STAT_STATES_STORED, w, 1);

          } else {

            /**
             * POR combined with SRC- or DEST-PROVISO
             *
             * the successor state is not new => if the current state
             * is reduced then the successor must be in the queue
             * (i.e., cyan for some worker) or safe.  otherwise the
             * proviso if not verified.
             */
            if(CFG_POR && CFG_POR_PROVISO
               && (CFG_POR_PROVISO_SRC
                   || CFG_POR_PROVISO_DEST) && reduced
               && !htbl_get_any_cyan(H, id_succ)
               && !htbl_get_attr(H, id_succ, ATTR_PROV_SAFE)) {
              
              if(CFG_POR_PROVISO_SRC) {

                /**
                 * SRC-PROVISO: the current state must be fully
                 * expanded
                 */
                if(has_safe_attr) {
                  htbl_set_attr(H, item.id, ATTR_PROV_SAFE, TRUE);
                }
                reduced = FALSE;
                list_free(en);
                bfs_back_to_s();
                en = state_events(s, heap);
                goto state_expansion;

              } else if(CFG_POR_PROVISO_DEST) {

                /**
                 * DEST-PROVISO: the successor state is put in the
                 * queue to be fully expanded
                 */
                htbl_set_attr(H, id_succ, ATTR_PROV_SAFE, TRUE);
                succ_item.id = id_succ;
                succ_item.s = succ;
                succ_item.fully_expand = TRUE;
                bfs_enqueue_real(succ_item, w, bfs_thread_owner(h));
                context_incr_stat(STAT_STATES_PROCESSED, w, -1);
              }
            }
          }
          bfs_back_to_s();
        }  /*  end of the loop on events  */
        state_free(s);
        list_free(en);

        /**
         * state processed => update some statistics
         */
        if(0 == arcs) {
          context_incr_stat(STAT_STATES_DEADLOCK, w, 1);
        } else {
          context_incr_stat(STAT_ARCS, w, arcs);
          context_incr_stat(STAT_EVENT_EXEC, w, arcs);
        }
        context_incr_stat(STAT_STATES_PROCESSED, w, 1);

        /**
         *  the state leaves the queue
         */
        bfs_queue_dequeue(Q, x, w);
      }
    }
  check_termination: {}
  } while(!bfs_check_termination(w));
  heap_free(heap);
  return NULL;
}


void
bfs
() {
  const bool_t with_trace = CFG_ACTION_CHECK_SAFETY && CFG_ALGO_BFS;
  const bool_t states_in_queue = CFG_HASH_COMPACTION;
  state_t s = state_initial(SYSTEM_HEAP);
  bool_t is_new, enqueue = TRUE;
  bfs_queue_item_t item;
  hkey_t h;
  htbl_meta_data_t mdata;

  /**
   * initialise global data
   */
  BFS_DATA.H = stbl_default_new();
  BFS_DATA.AT_BARRIER = FALSE;
  BFS_DATA.Q = bfs_queue_new(CFG_NO_WORKERS, CFG_BFS_QUEUE_BLOCK_SIZE,
                             states_in_queue);
  if(CFG_ALGO_DBFS) {
    dbfs_comm_start(BFS_DATA.H);
  }
  pthread_barrier_init(&BFS_DATA.BARRIER, NULL, CFG_NO_WORKERS);

  /**
   * enqueue the initial state
   */
  if(CFG_ALGO_DBFS) {
    h = state_hash(s);
    enqueue = dbfs_comm_state_owned(h);
  }
  if(enqueue) {
    htbl_meta_data_init(mdata, s);
    stbl_insert(BFS_DATA.H, mdata, is_new);
    item.id = mdata.id;
    item.s = s;
    item.fully_expand = FALSE;
    if(with_trace) {
      htbl_set_attr(BFS_DATA.H, mdata.id, ATTR_PRED, mdata.id);
      htbl_set_attr(BFS_DATA.H, mdata.id, ATTR_EVT, 0);
    }
    bfs_enqueue_real(item, 0, 0);
    context_incr_stat(STAT_STATES_STORED, 0, 1);
  }
  state_free(s);

  launch_and_wait_workers(&bfs_worker);

  /**
   * free everything
   */
  if(CFG_ALGO_DBFS) {
    dbfs_comm_end();
  }
  htbl_free(BFS_DATA.H);
  bfs_queue_free(BFS_DATA.Q);
}

#endif
