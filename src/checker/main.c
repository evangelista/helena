#include "compression.h"
#include "config.h"
#include "context.h"
#include "dfs.h"
#include "bfs.h"
#include "d4.h"
#include "d5.h"
#include "simulator.h"
#include "bit_stream.h"
#include "comm.h"
#include "dist_compression.h"
#include "debug.h"
#include "por_graph.h"


int main
(int argc,
 char ** argv) {

  /**
   * initialisation of all libraries
   */
  init_heap();
  init_bit_stream();
  init_model();
  init_comm();
  init_dist_compression();
  init_context();
  init_compression();

  if(CFG_ACTION_SIMULATE) {
    simulator();
  } else if(CFG_ACTION_POR_GRAPH) {
    por_graph();
  } else {

    /**
     * catch SIGINT by changing the context state
     */
    signal(SIGINT, context_interruption_handler);

    /**
     * launch the appropriate search algorithm
     */
    if(CFG_ALGO_BFS || CFG_ALGO_DBFS) {
      bfs();
    } else if(CFG_ALGO_DFS) {
      dfs();
    } else if(CFG_ALGO_D4) {
      d4();
    } else if(CFG_ALGO_D5) {
      d5();
    } else {
      LNA_UNREACHABLE_CODE();
    }

    /**
     * termination of all libraries
     */
    finalise_context();
    finalise_dist_compression();
    finalise_comm();
    finalise_compression();
    finalise_model();
  }

  exit(EXIT_SUCCESS);
}
