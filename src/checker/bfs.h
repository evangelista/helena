/**
 * @file bfs.h
 * @brief Implementation of BFS based algorithms: BFS, DBFS (distributed BFS)
 * @date 12 sep 2017
 * @author Sami Evangelista
 */

#ifndef LIB_BFS
#define LIB_BFS

#include "includes.h"
#include "context.h"
#include "state.h"
#include "event.h"
#include "bfs_queue.h"


/**
 * @brief Launch the BFS based algorithm.
 */
void
bfs
();


/**
 * @brief Enqueue an item.
 */
void
bfs_enqueue
(bfs_queue_item_t item,
 worker_id_t from,
 worker_id_t to);


/**
 * @brief Check if there are no more states to process.
 */
bool_t
bfs_no_state_to_process
();

#endif
