#include "hmap.h"

struct struct_hmap_t {
  hset_t H;
  uint32_t sizeof_key;
  uint32_t sizeof_item;
};
typedef struct struct_hmap_t struct_hmap_t;


hmap_t
hmap_new
(uint32_t sizeof_key,
 uint32_t sizeof_item,
 uint32_t base_size,
 char store_hash,
 hset_hash_func_t hash_key_func,
 hset_cmp_func_t cmp_key_func) {
  hmap_t result = malloc(sizeof(struct_hmap_t));

  result->H = hset_new
    (sizeof_key + sizeof_item, base_size, store_hash,
     hash_key_func, cmp_key_func);
  result->sizeof_key = sizeof_key;
  result->sizeof_item = sizeof_item;
  hset_set_hash_size(result->H, sizeof_key);
  return result;
}


void
hmap_free
(hmap_t hmap) {
  hset_free(hmap->H);
  free(hmap);
}


void
hmap_empty
(hmap_t hmap) {
  hset_empty(hmap->H);
}


uint32_t
hmap_num_items
(hmap_t hmap) {
  return hset_num_items(hmap->H);
}


char
hmap_insert
(hmap_t hmap,
 void * key,
 void * item,
 hset_mdata_t * mdata) {
  char new[hmap->sizeof_key + hmap->sizeof_item];
  memcpy(new, key, hmap->sizeof_key);
  memcpy(new + hmap->sizeof_key, item, hmap->sizeof_item);
  return hset_insert(hmap->H, &new, mdata);
}


char
hmap_lookup
(hmap_t hmap,
 void * key,
 void * item) {
  char result, looked_up[hmap->sizeof_key + hmap->sizeof_item];
  hset_mdata_t mdata;

  memcpy(looked_up, key, hmap->sizeof_key);
  result = hset_contains(hmap->H, looked_up, &mdata);
  if(result && item) {
    char * ptr = hset_get_ptr(hmap->H, mdata.ref);
    memcpy(key, ptr, hmap->sizeof_key);
    memcpy(item, ptr + hmap->sizeof_key, hmap->sizeof_item);
  }
  return result;
}
