#include "por_graph.h"
#include "dfs.h"
#include "htbl.h"
#include "stbl.h"


typedef struct {
  FILE * fdg;
  htbl_t H;
} por_graph_data_t;


dfs_action_t
por_graph_edge_hook
(event_t e,
 htbl_id_t id,
 dfs_edge_type_t etype,
 htbl_id_t id_succ,
 dfs_context_t * ctx,
 void * data_void) {
#if CFG_ACTION_POR_GRAPH == 1
  por_graph_data_t * data = (por_graph_data_t *) data_void;
  fprintf(data->fdg,
          "%llu;%u;%llu\n",
          (long long unsigned int) id,
          (unsigned int) e,
          (long long unsigned int) id_succ);
#endif  
  return DFS_CONTINUE;
}


void
por_graph_find_or_put
(htbl_id_t * sid,
 bool_t * is_new,
 dfs_context_t * ctx,
 void * data_void) {
  por_graph_data_t * data = (por_graph_data_t *) data_void;
  htbl_meta_data_t mdata;

  htbl_meta_data_init(mdata, ctx->now);
  stbl_insert(data->H, mdata, *is_new);
  *sid = mdata.id;
}


void
por_graph
() {
  dfs_context_t ctx;
  por_graph_data_t data;

  memset(&ctx, 0, sizeof(dfs_context_t));
  ctx.S = dfs_stack_new(10000, 0, 1);
  ctx.find_or_put = &por_graph_find_or_put;
  ctx.edge_hook = &por_graph_edge_hook;

  data.fdg = fopen("por-graph.dat", "w");
  data.H = stbl_default_new();

  dfs_traversal(0, &ctx, &data, htbl_null_id);

  /* free everything */
  dfs_stack_free(ctx.S);
  htbl_free(data.H);
  fclose(data.fdg);
}
