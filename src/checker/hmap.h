/**
 * @file hmap.h
 * @brief Implementation of mappings based on hashing.
 * @date 12 dec 2020
 * @author Sami Evangelista
 */

#ifndef LIB_HMAP
#define LIB_HMAP

#include "hset.h"


typedef struct struct_hmap_t * hmap_t;


/**
 * @brief hmap_new
 */
hmap_t
hmap_new
(uint32_t sizeof_key,
 uint32_t sizeof_item,
 uint32_t base_size,
 char store_hash,
 hset_hash_func_t hash_key_func,
 hset_cmp_func_t cmp_key_func);


/**
 * @brief hmap_free
 */
void
hmap_free
(hmap_t hmap);


/**
 * @brief hmap_num_items
 */
uint32_t
hmap_num_items
(hmap_t hmap);


/**
 * @brief hmap_empty
 */
void
hmap_empty
(hmap_t hmap);


#define hmap_delete hset_delete


/**
 * @brief hmap_insert
 */
char
hmap_insert
(hmap_t hmap,
 void * key,
 void * item,
 hset_mdata_t * mdata);


/**
 * @brief hmap_lookup
 */
char
hmap_lookup
(hmap_t hmap,
 void * key,
 void * item);

#endif
