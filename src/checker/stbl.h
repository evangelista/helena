/**
 * @file stbl.h
 * @brief Implementation of a state table.
 * @date 12 sep 2017
 * @author Sami Evangelista
 */

#ifndef LIB_STBL
#define LIB_STBL

#include "list.h"
#include "htbl.h"
#include "context.h"


/**
 * @brief stbl_default_new
 */
htbl_t stbl_default_new
();


/**
 * @brief stbl_get_trace
 */
list_t stbl_get_trace
(htbl_t tbl,
 htbl_id_t id);


/**
 * @brief stbl_insert
 *
 * TODO: set an error message when table is full
 */
#define stbl_insert(tbl, meta, is_new) {                                \
    htbl_insert_code_t ic;                                              \
    if(HTBL_INSERT_FULL == (ic = htbl_insert(tbl, &meta))) {            \
      context_set_termination_state(TERM_MEMORY_LIMIT_REACHED);         \
    }                                                                   \
    (is_new) = ic == HTBL_INSERT_OK;                                    \
  }

#endif
