/**
 * @file d4.h
 * @brief Implementation of the delta DDD algorithm.
 * @date 12 sep 2017
 * @author Sami Evangelista
 */

#ifndef LIB_D4
#define LIB_D4

#include "includes.h"
#include "common.h"
#include "state.h"
#include "event.h"


/**
 * @brief d4
 */
void d4
();

#endif
