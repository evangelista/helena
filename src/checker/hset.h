/**
 * @file hset.h
 * @brief Implementation of sets based on hashing.
 * @date 12 sep 2020
 * @author Sami Evangelista
 *
 * TODO:
 *   * merge hset_insert and hset_contains
 */

#ifndef LIB_HSET
#define LIB_HSET

#include "stdint.h"
#include "stdlib.h"
#include "string.h"


/**
 * @typedef hset_t
 * @brief a set
 */
typedef struct struct_hset_t * hset_t;


/**
 * @typedef hval_t
 * @brief an hash value
 */
typedef uint64_t hval_t;


/**
 * @typedef hset_cmp_func_t
 * @brief a function comparing two items and returning 1 if they are
 * the same, 0 else
 */
typedef char (* hset_cmp_func_t) (void *, void *);


/**
 * @typedef hset_hash_func_t
 * @brief a function mapping items to hash values
 */
typedef hval_t (* hset_hash_func_t) (void *);


/**
 * @typedef hset_ref_t
 * @brief a reference to an item stored in a set
 */
typedef uint64_t hset_ref_t;


/**
 * @typedef hset_mdata_t
 * @brief meta data that can be associated to an item
 */
typedef struct {
  hval_t hash;  /**< hash value computed for the item */
  hset_ref_t ref;  /**< reference to the item in the set */
} hset_mdata_t;


/**
 * @brief hset_new
 *
 * return a new hset.  sizeof_item is the number of chars that each
 * item occupies.  base_size is the initial size of the hash table
 * storing the set.  if store_hash = 1 then each item in the state is
 * stored with its hash value.  hash_func is the function used to
 * compute hash values of items.  cmp_func is the function used
 * to compare items
 */
hset_t
hset_new
(uint32_t sizeof_item,
 uint32_t base_size,
 char store_hash,
 hset_hash_func_t hash_func,
 hset_cmp_func_t cmp_func);


/**
 * @brief hset_free
 *
 * free hset
 */
void
hset_free
(hset_t hset);


/**
 * @brief hset_empty
 *
 * empty hset from all its items
 */
void
hset_empty
(hset_t hset);


/**
 * @brief hset_num_items
 *
 * get the number of items in hset
 */
uint32_t
hset_num_items
(hset_t hset);


/**
 * @brief hset_set_hash_size
 *
 * if the default hash function is used (i.e., hset_new was called
 * with hash_func == NULL), use the hash_size first chars of the item
 * to compute its hash value
 */
void
hset_set_hash_size
(hset_t hset,
 uint32_t hash_size);


/**
 * @brief hset_insert
 *
 * insert item in hset and return 1 if the item has been inserted, 0
 * otherwise.  update *mdata if mdata is not NULL 
 */
char
hset_insert
(hset_t hset,
 void * item,
 hset_mdata_t * mdata);


/**
 * @brief hset_delete
 *
 * delete from hset item referenced by ref.  no effect if ref does not
 * reference an item of hset
 */
void
hset_delete
(hset_t hset,
 hset_ref_t ref);


/**
 * @brief hset_contains
 *
 * return 1 if an item equal to argument item is found to be in hset,
 * 0 otherwise.  if an item has been found, it is copied in item.
 * update *mdata if mdata is not NULL
 */
char
hset_contains
(hset_t hset,
 void * item,
 hset_mdata_t * mdata);


/**
 * @brief hset_get
 *
 * return 1 if ref references an item of hset, 0 otherwise.  if item
 * is not NULL, and 1 is returned, then the referenced item is copied
 * in argument item
 */
char
hset_get
(hset_t hset,
 hset_ref_t ref,
 void * item);


/**
 * @brief hset_get_ptr
 *
 * return a pointer to the item in hset that is referenced by ref, or
 * NULL if not item is referenced by ref
 */
void *
hset_get_ptr
(hset_t hset,
 hset_ref_t ref);


/**
 * @brief hset_fold
 *
 * apply fold_func(item, data) to all the items of hset
 */
void
hset_fold
(hset_t hset,
 void (* fold_func) (void *, void *),
 void * data);


/**
 * @brief hset_hash_fold
 * 
 * apply fold_func(h, item, data) to all the items of hset that have h
 * as hash value
 */
void
hset_hash_fold
(hset_t hset,
 hval_t h,
 void (* fold_func) (hval_t, void *, void *),
 void * data);

#endif
