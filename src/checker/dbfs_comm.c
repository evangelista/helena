#include "config.h"
#include "dbfs_comm.h"
#include "comm.h"
#include "stbl.h"
#include "debug.h"
#include "dist_compression.h"
#include "bfs.h"
#include "bfs_queue.h"
#include "darray.h"

#if CFG_ALGO_DBFS == 1

#include "shmem.h"


int PES;  /* number of pes */
int ME;  /* my pe number */
htbl_t H;  /* the hash table storing states */
char ** BUF;  /* communication buffers for outgoing data */
uint32_t * LEN;  /* array containing number of bytes to send to each pe */
darray_t CACHE;
bool_t CACHE_EXPANDING = FALSE;
int TERM_DETECTED = FALSE;
char * BUFC;
uint32_t * FIRSTC;
uint32_t LASTC;
uint32_t SIZEC;
heap_t CW_HEAP, CACHE_HEAP;
static int SH_TERM = 0;

#define DBFS_COMM_RPOS (ME * CFG_DBFS_BUFFER_SIZE)

#if CFG_DBFS_POLL
#define DBFS_COMM_SHIFT 0
#define DBFS_COMM_TO_ADD sizeof(uint32_t)
#elif CFG_DBFS_BARRIER
#define DBFS_COMM_SHIFT sizeof(uint32_t)
#define DBFS_COMM_TO_ADD 0
#endif

#define DBFS_COMM_BUFFER_IS_EMPTY(pe) (LEN[pe] <= DBFS_COMM_SHIFT)
#define DBFS_COMM_BUFFER_INIT(pe) {                             \
    memset(BUF[pe], 0, CFG_DBFS_BUFFER_SIZE);                   \
    LEN[pe] = DBFS_COMM_SHIFT;                                  \
  }
#define DBFS_COMM_BUFFER_OVERFLOW(pe, added)               \
  (LEN[pe] + LASTC - FIRSTC[pe] + added + DBFS_COMM_TO_ADD \
   > CFG_DBFS_BUFFER_SIZE)



void
dbfs_comm_poll_pe
(int pe);


void
dbfs_comm_on_buffer_full
(int pe);


void
dbfs_comm_send_buffer
(int pe);


uint16_t
dbfs_comm_state_owner
(hkey_t h) {
  return h % PES;
}


bool_t
dbfs_comm_state_owned
(hkey_t h) {
  return dbfs_comm_state_owner(h) == ME;
}


bool_t
dbfs_comm_process_state
(htbl_meta_data_t * mdata,
 bool_t pred_safe) {
  int pe;
  uint16_t size, full_size;

  if(!CFG_DISTRIBUTED_STATE_COMPRESSION) {
    mdata->h = state_hash((state_t) mdata->item);
    if(CFG_DBFS_SEND_HASH) {
      mdata->h_set = TRUE;
    }
  } else {
    state_dist_compress((state_t) mdata->item, mdata->v, &size);
    mdata->v_set = TRUE;
    mdata->v_size = size;
    mdata->h = string_hash(mdata->v, size);
    mdata->h_set = TRUE;
  }
  pe = dbfs_comm_state_owner(mdata->h);

  /**
   *  state is mine => exit (state is processed in the bfs main
   *  procedure)
   */
  if(ME == pe) {
    return TRUE;
  }

  /**
   *  put state in the cache
   */
  if(CFG_DBFS_CACHE_SIZE > 0 && darray_size(CACHE) < CFG_DBFS_CACHE_SIZE) {
    state_t copy = state_copy(mdata->item, CACHE_HEAP);
    darray_push(CACHE, &copy);
  }

  /**
   *  otherwise put state in the send buffer.  first send the buffer
   *  if putting the new state in the buffer would cause it to
   *  overflow
   */
  if(!CFG_DISTRIBUTED_STATE_COMPRESSION) {
    size = state_char_size((state_t) mdata->item);
  }
  full_size = 1 + sizeof(uint16_t) + size;
  if(CFG_DBFS_SEND_HASH) {
    full_size += sizeof(hkey_t);
  }
  if(CFG_POR && CFG_POR_PROVISO && CFG_POR_PROVISO_DEST) {
    full_size ++;
  }
  if(DBFS_COMM_BUFFER_OVERFLOW(pe, full_size)) {
    if(CACHE_EXPANDING) {
      return FALSE;
    }
    dbfs_comm_on_buffer_full(pe);
    if(TERM_DETECTED) {
      return FALSE;
    }
  }
  * ((char *) (BUF[pe] + LEN[pe])) = DBFS_COMM_STATE;
  LEN[pe] += 1;
  * ((uint16_t *) (BUF[pe] + LEN[pe])) = size;
  LEN[pe] += sizeof(uint16_t);
  if(CFG_DBFS_SEND_HASH) {
    * ((hkey_t *) (BUF[pe] + LEN[pe])) = mdata->h;
    LEN[pe] += sizeof(hkey_t);
  }
  if(CFG_POR && CFG_POR_PROVISO && CFG_POR_PROVISO_DEST) {
    * ((char *) (BUF[pe] + LEN[pe])) = pred_safe;
    LEN[pe] += 1;
  }
  if(CFG_DISTRIBUTED_STATE_COMPRESSION) {
    memcpy(BUF[pe] + LEN[pe], mdata->v, size);
  } else {
    state_serialise((state_t) mdata->item, BUF[pe] + LEN[pe], &size);
  }
  LEN[pe] += size;
  return FALSE;
}


void
dbfs_comm_put_in_comp_buffer
(char * buffer,
 int len) {
  int pe;

  /**
   * send all output buffers that would overflow if adding the buffer
   */
  for(pe = 0; pe < PES; pe ++) {
    if(DBFS_COMM_BUFFER_OVERFLOW(pe, len)) {
      dbfs_comm_send_buffer(pe);
    }
  }

  /**
   * reallocate the compression buffer if necessary
   */
  if(LASTC + len > SIZEC) {
    SIZEC <<= 1;
    BUFC = realloc(BUFC, SIZEC);
  }

  /**
   * and copy the buffer in the compression buffer
   */
  memcpy(BUFC + LASTC, buffer, len);
  LASTC += len;
}


void
dbfs_comm_send_buffer
(int pe) {
  uint32_t len, lenc = LASTC - FIRSTC[pe];
  char buffer[CFG_DBFS_BUFFER_SIZE];

  assert(LEN[pe] + lenc <= CFG_DBFS_BUFFER_SIZE);
  dbfs_comm_poll_pe(pe);
  if(TERM_DETECTED) {
    return;
  }
  if(CFG_DISTRIBUTED_STATE_COMPRESSION && lenc > 0) {
    memcpy(buffer, BUFC + FIRSTC[pe], lenc);
    memcpy(buffer + lenc, BUF[pe], LEN[pe]);
    len = LEN[pe] + lenc;
    comm_put(DBFS_COMM_RPOS + sizeof(uint32_t), buffer, len, pe);
    comm_put(DBFS_COMM_RPOS, &len, sizeof(uint32_t), pe);
    FIRSTC[pe] = LASTC;
  } else {
#if CFG_DBFS_POLL
    comm_put(DBFS_COMM_RPOS + sizeof(uint32_t), BUF[pe], LEN[pe], pe);
    comm_put(DBFS_COMM_RPOS, &LEN[pe], sizeof(uint32_t), pe);
#elif CFG_DBFS_BARRIER
    LEN[pe] -= sizeof(uint32_t);
    * ((uint32_t *) BUF[pe]) = LEN[pe];
    comm_put(DBFS_COMM_RPOS, BUF[pe], LEN[pe] + sizeof(uint32_t), pe);
#endif
  }
  DBFS_COMM_BUFFER_INIT(pe);
}


bool_t
dbfs_comm_send_all_buffers
() {
  bool_t result = FALSE;
  for(int pe = 0; pe < PES; pe ++) {
    if(!DBFS_COMM_BUFFER_IS_EMPTY(pe)) {
      dbfs_comm_send_buffer(pe);
      result = TRUE;
    }
  }
  return result;
}


void
dbfs_comm_process_buffer
(int pe,
 uint32_t len,
 int pos) {
  uint32_t stored = 0;
  bool_t is_new, pred_safe, enqueue;
  char buffer[CFG_DBFS_BUFFER_SIZE];
  htbl_meta_data_t mdata;
  bfs_queue_item_t item;
  char * b, * b_end;
  uint16_t size;
  char t;

  comm_get(buffer, pos + sizeof(uint32_t), len, ME);
  b = buffer;
  b_end = b + len;
  len = 0;
  comm_put(pos, &len, sizeof(uint32_t), ME);
  while(b != b_end) {
    t = * ((char *) b);
    b ++;
    size = * ((uint16_t *) b);
    b += sizeof(uint16_t);
    switch(t) {
    case DBFS_COMM_STATE:
      htbl_meta_data_init(mdata, NULL);
      if(CFG_DBFS_SEND_HASH) {
        mdata.h = * ((hkey_t *) b);
        mdata.h_set = TRUE;
        b += sizeof(hkey_t);
      }
      if(CFG_POR && CFG_POR_PROVISO && CFG_POR_PROVISO_DEST) {
        pred_safe = * ((char *) b);
        b += 1;
      }
      if(CFG_DISTRIBUTED_STATE_COMPRESSION) {
        memcpy(&mdata.v, b, size);
        mdata.v_set = TRUE;
        mdata.v_size = size;
      } else {
        heap_reset(CW_HEAP);
        mdata.item = state_unserialise(b, CW_HEAP);
      }
      b += size;
      stbl_insert(H, mdata, is_new);
      enqueue = FALSE;
      if(is_new) {
        stored ++;
        item.fully_expand = FALSE;
        enqueue = TRUE;
      } else if(CFG_POR && CFG_POR_PROVISO && CFG_POR_PROVISO_DEST &&
                !pred_safe && !htbl_get_any_cyan(H, mdata.id) &&
                !htbl_get_attr(H, mdata.id, ATTR_PROV_SAFE)) {
        htbl_set_attr(H, mdata.id, ATTR_PROV_SAFE, TRUE);
        item.fully_expand = TRUE;
        context_incr_stat(STAT_STATES_PROCESSED, 0, -1);
        enqueue = TRUE;
      }
      if(enqueue) {
        item.id = mdata.id;
        bfs_enqueue(item, 0, 0);
      }
      break;
    case DBFS_COMM_COMP_DATA:
      dist_compression_process_serialised_component(b);
      b += size;
      break;
    default:
      printf("unknown element type: %d\n", t);
      assert(0);
    }
  }
  context_incr_stat(STAT_STATES_STORED, 0, stored);
}


bool_t
dbfs_comm_process_all_buffers
() {
  int pe;
  bool_t result = FALSE;
  uint32_t pos = 0, len;

  for(pe = 0; pe < PES; pe ++) {
    comm_get(&len, pos, sizeof(uint32_t), ME);
    if(len > 0) {
      dbfs_comm_process_buffer(pe, len, pos);
      result = TRUE;
    }
    pos += CFG_DBFS_BUFFER_SIZE;
  }
  return result;
}


uint32_t
dbfs_comm_expand_cache
() {
  if(0 == darray_size(CACHE)) {
    return 0;
  }
  uint32_t result = 0;
  heap_t heap = local_heap_new();
  CACHE_EXPANDING = TRUE;
  for(darray_index_t i = 0; i < darray_size(CACHE); i ++) {
    state_t s;
    list_t en;
    uint32_t arcs = 0;
    darray_get(CACHE, i, &s);
    en = state_events(s, heap);
    while(!list_is_empty(en)) {
      event_t e;
      state_t succ;
      htbl_meta_data_t mdata;
      arcs ++;
      list_pick_first(en, &e);
      succ = state_succ(s, e, heap);
      htbl_meta_data_init(mdata, succ);
      dbfs_comm_process_state(&mdata, FALSE);
    }
    context_incr_stat(STAT_EVENT_EXEC, 0, arcs);
    result ++;
  }
  darray_reset(CACHE);
  heap_reset(CACHE_HEAP);
  heap_free(heap);
  CACHE_EXPANDING = FALSE;
  return result;
}


#if CFG_DBFS_POLL

/*****************************************************************************
 *
 * POLL strategy
 *
 *****************************************************************************/

bool_t TERM_CHECKED = FALSE;
static int SH_TOKEN;
static int SH_TERM_DETECTION_ASKED;

#define DBFS_COMM_NO_TERM 0
#define DBFS_COMM_TERM 1
#define DBFS_COMM_FORCE_TERM 2


bool_t dbfs_comm_process_all_buffers
();


void
dbfs_comm_ask_for_term_detection
() {
  const int b = TRUE;
  for(int pe = 0; pe < PES; pe ++) {
    shmem_int_put(&SH_TERM_DETECTION_ASKED, &b, 1, pe);
  }
}


bool_t
dbfs_comm_term_detection_asked
() {
  int result;
  shmem_int_get(&result, &SH_TERM_DETECTION_ASKED, 1, ME);
  return result;
}


void
dbfs_comm_unset_term_detection_asked
() {
  const int b = FALSE;
  shmem_int_put(&SH_TERM_DETECTION_ASKED, &b, 1, ME);
}


void
dbfs_comm_send_token
() {
  const int token = TRUE;
  shmem_int_put(&SH_TOKEN, &token, 1, (ME + 1) % PES);
}


void
dbfs_comm_clear_token
() {
  const int token = FALSE;
  shmem_int_put(&SH_TOKEN, &token, 1, ME);
}


bool_t
dbfs_comm_token_received
() {
  int result;
  shmem_int_get(&result, &SH_TOKEN, 1, ME);
  return (bool_t) result;
}


void
dbfs_comm_check_termination
(bool_t idle) {
  int term, rterm;
  int pe;
  bool_t all_term, force_term;

  /**
   * termination already detected => nothing to do
   */
  if(TERM_DETECTED) {
    return;
  }

  /**
   * wait for all PEs to be here and process incoming states
   */
  comm_barrier();
  dbfs_comm_process_all_buffers();

  /**
   * publish local termination result
   */
  if(!context_keep_searching()) {
    term = DBFS_COMM_FORCE_TERM;
  } else if(idle && bfs_no_state_to_process()) {
    term = DBFS_COMM_TERM;
  } else {
    term = DBFS_COMM_NO_TERM;
  }
  dbfs_comm_unset_term_detection_asked();
  shmem_int_put(&SH_TERM, &term, 1, ME);

  /**
   * wait for all PEs to publish their result
   */
  comm_barrier();

  /**
   * termination is detected if all PEs have terminated normally
   * (i.e., local termination state == DBFS_COMM_TERM) or if a single
   * PE has terminated earlier (i.e., local termination state ==
   * DBFS_COMM_FORCE_TERM)
   */
  all_term = TRUE;
  force_term = FALSE;
  for(pe = 0; pe < PES; pe ++) {
    shmem_int_get(&rterm, &SH_TERM, 1, pe);
    if(DBFS_COMM_NO_TERM == rterm) {
      all_term = FALSE;
    } else if(DBFS_COMM_FORCE_TERM == rterm) {
      force_term = TRUE;
      break;
    }
  }

  /**
   * process result by updating the context
   */
  if(all_term || force_term) {
    TERM_DETECTED = TRUE;
    context_stop_search();
    if(force_term && term != DBFS_COMM_FORCE_TERM) {
      context_set_termination_state(TERM_INTERRUPTION);
    }
  }

  /**
   *  this last barrier is perhaps not useful
   */
  comm_barrier();
  TERM_CHECKED = TRUE;
}


bool_t
dbfs_comm_check_communications_aux
(bool_t idle) {
  bool_t result;

  if(TERM_DETECTED) {
    return FALSE;
  }
  result = dbfs_comm_process_all_buffers();
  if(dbfs_comm_token_received()) {
    dbfs_comm_clear_token();
    if(idle && bfs_no_state_to_process()) {
      if(0 == ME) {
        dbfs_comm_ask_for_term_detection();
      } else {
        dbfs_comm_send_token();
      }
    }
  }
  if(dbfs_comm_term_detection_asked()) {
    dbfs_comm_check_termination(idle);
  }
  return result;
}


bool_t
dbfs_comm_check_communications
() {
  return dbfs_comm_check_communications_aux(FALSE);
}


bool_t
dbfs_comm_idle
() {
  clock_t start = clock();
  uint64_t duration;

  if(TERM_DETECTED) {
    return TRUE;
  }
  if(!context_keep_searching()) {
    dbfs_comm_ask_for_term_detection();
    dbfs_comm_check_termination(TRUE);
    return TRUE;
  }
  dbfs_comm_send_all_buffers();
  while(TRUE) {
    dbfs_comm_check_communications_aux(TRUE);
    if(TERM_DETECTED) {
      return TRUE;
    } else if(!bfs_no_state_to_process()) {
      return FALSE;
    } else {
      duration = 1000 * (clock() - start) / CLOCKS_PER_SEC;
      if(duration >= CFG_DBFS_CHECK_TERM_PERIOD_MS && 0 == ME) {
        dbfs_comm_send_token();
        start = clock();
      }
    }
  }
}


void
dbfs_comm_poll_pe
(int pe) {
  int len;

  if(TERM_DETECTED) {
    return;
  }
  do {
    comm_get(&len, DBFS_COMM_RPOS, sizeof(uint32_t), pe);
    if(len > 0) {
      dbfs_comm_check_communications_aux(FALSE);
      if(TERM_DETECTED) {
        return;
      }
    }
  }
  while(len > 0);
}


void
dbfs_comm_on_buffer_full
(int pe) {
  dbfs_comm_send_buffer(pe);
}


#elif CFG_DBFS_BARRIER

/*****************************************************************************
 *
 * BARRIER strategy
 *
 *****************************************************************************/


void
dbfs_comm_poll_pe
(int pe) {
}


void
dbfs_comm_meeting
(bool_t buffer_full) {
  if(!buffer_full) {
    dbfs_comm_expand_cache();
    if(!bfs_no_state_to_process()) {
      return;
    }
  }
  SH_TERM = !dbfs_comm_send_all_buffers() && !buffer_full;
  comm_barrier();
  TERM_DETECTED = TRUE;
  for(int pe = 0; TERM_DETECTED && pe < PES; pe ++) {
    shmem_int_get(&TERM_DETECTED, &SH_TERM, 1, pe);
  }
  dbfs_comm_process_all_buffers();
  comm_barrier();
}


void
dbfs_comm_on_buffer_full
(int pe) {
  dbfs_comm_meeting(TRUE);
}


bool_t
dbfs_comm_idle
() {
  if(TERM_DETECTED) {
    return TRUE;
  }
  dbfs_comm_meeting(FALSE);
  return TERM_DETECTED;
}

#endif


void
dbfs_comm_start
(htbl_t h) {
  uint32_t len = 0;
  size_t hs;
  PES = comm_pes();
  ME = comm_me();
  H = h;
  SIZEC = CFG_DBFS_BUFFER_SIZE;
  LASTC = 0;
  CW_HEAP = local_heap_new();
  CACHE = darray_new(SYSTEM_HEAP, sizeof(state_t));
  CACHE_HEAP = local_heap_new();
  BUF = mem_alloc0(SYSTEM_HEAP, sizeof(char *) * PES);
  LEN = mem_alloc0(SYSTEM_HEAP, sizeof(uint32_t) * PES);
  FIRSTC = mem_alloc0(SYSTEM_HEAP, sizeof(uint32_t) * PES);
  BUFC = mem_alloc0(SYSTEM_HEAP, CFG_DBFS_BUFFER_SIZE);
  hs = PES * CFG_DBFS_BUFFER_SIZE;
  comm_malloc(hs);
  for(int pe = 0; pe < PES; pe ++) {
    BUF[pe] = mem_alloc0(SYSTEM_HEAP, CFG_DBFS_BUFFER_SIZE);
    DBFS_COMM_BUFFER_INIT(pe);
  }
  comm_barrier();
}


void
dbfs_comm_end
() {
  for(int pe = 0; pe < PES; pe ++) {
    mem_free(SYSTEM_HEAP, BUF[pe]);
  }
  mem_free(SYSTEM_HEAP, BUF);
  mem_free(SYSTEM_HEAP, LEN);
  mem_free(SYSTEM_HEAP, BUFC);
  mem_free(SYSTEM_HEAP, FIRSTC);
  darray_free(CACHE);
  heap_free(CW_HEAP);
  heap_free(CACHE_HEAP);
}

#endif
