/**
 * @file d5_lb.h
 * @brief Implementation of the distributed delta DDD algorithm based
 *        on work stealing.
 * @date 12 apr 2021
 * @author Sami Evangelista
 */


#ifndef LIB_D5_WS
#define LIB_D5_WS

void
d5_get_stats
();

void
d5_put_stats
();

#endif
