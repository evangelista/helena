#include "d5.h"
#include "d5_ws.h"
#include "darray.h"
#include "debug.h"
#include "event.h"
#include "hset.h"
#include "context.h"


#if CFG_ALGO_D5 == 0 || CFG_D5_WS == 0

void d5_ws() { LNA_UNREACHABLE_CODE(); }

#else

#include "shmem.h"

#define D5_DEBUG(...)   {                                       \
    fprintf(stderr, "[pe=%d,depth=%d] ", D5.pe_me, D5.depth);   \
    fprintf(stderr, __VA_ARGS__);                               \
    fprintf(stderr, "\n");                                      \
  }

#define D5_RO_EMPTY 0
#define D5_RO_AVAIL 1
#define D5_RO_READ  2


/**
 *  global shared variables (prefix = D5_SH_)
 */
static d5_term_data_t D5_SH_TERM;
static d5_ro_data_t D5_SH_RO;
d5_stat_data_t * D5_SH_STAT;
char * D5_SH_RO_ST;
uint32_t * D5_SH_MOVES;


void d5_explore_mine();
void d5_reset_ro_set();
bool_t d5_do_dd();


/**
 *  d5_tag_path
 *
 *  tag nodes from node up to the root for step (D5_STEP_EXP or
 *  D5_STEP_DDD).  node is tagged as to process (D5_FLAG_PROC) and
 *  nodes on the path are tagged as to visit (D5_FLAG_VISIT)
 */
void
d5_tag_path
(d5_st_ref_t node,
 uint8_t step) {
  d5_tree_t * tree = darray_get_ptr(D5.forest, node.tr);
  darray_t nodes = tree->nodes;
  d5_st_t * st;
  bool_t loop = TRUE;

  st = (d5_st_t *) darray_get_ptr(nodes, node.st);
  d5_set_flag(st->flags, step, D5_FLAG_PROC);
  while(loop) {
    st = (d5_st_t *) darray_get_ptr(nodes, st->father);
    if(d5_get_flag(st->flags, step, D5_FLAG_VISIT)) {
      loop = FALSE;
    } else {
      d5_set_flag(st->flags, step, D5_FLAG_VISIT);
    }
  }
}


/**
 * @d5_tag_for_dd
 */
void
d5_tag_for_dd
(d5_st_ref_t st) {
  d5_tag_path(st, D5_STEP_DDD);
}


/**
 *  d5_plant_new_tree
 *
 *  create a new tree with s as root state and put it in the forest
 */
d5_st_ref_t
d5_plant_new_tree
(state_t root,
 bool_t stolen) {
  d5_tree_t new_tree;
  d5_st_t croot;
  const d5_st_ref_t result = { .tr = darray_size(D5.forest), .st = 0 };
  new_tree.idx_fst_leaf = stolen ? 1 : 0;
  new_tree.root = root;
  new_tree.nodes = darray_new(NULL, sizeof(d5_st_t));
  memset(&croot, 0, sizeof(d5_st_t));
  darray_push(new_tree.nodes, &croot);
  darray_push(D5.forest, &new_tree);
  context_incr_stat(STAT_STATES_STORED, 0, 1);
  if(stolen) {
    D5_SH_STAT[D5.pe_me].open_now ++;
  } else {
    D5_SH_STAT[D5.pe_me].open_next ++;
  }
  return result;
}


/**
 *  d5_traversal
 *
 *  generic traversal procedure.  call hook on each state to be processed
 */
void
d5_traversal
(int step,
 d5_st_hook_t hook) {
  typedef struct {
    event_t e;
    state_t now;
    list_t en;
    d5_st_lref_t ref;
    d5_evt_num_t nxt;
    mem_size_t pos;
  } d5_traversal_stack_item_t;
#define d5_traversal_push(_now, _e, _ref) {     \
    depth ++;                                   \
    stack[depth].pos = heap_get_position(heap); \
    stack[depth].now = _now;                    \
    stack[depth].ref = _ref;                    \
    stack[depth].e = _e;                        \
    stack[depth].en = state_events(_now, heap); \
    stack[depth].nxt = 0;                       \
}
  heap_t heap = local_heap_new();
  state_t now;
  for(int i = 0; i < darray_size(D5.forest); i ++) {
    d5_tree_t tree;
    d5_st_t * st;
    int32_t depth = - 1;
    d5_traversal_stack_item_t stack[D5.depth + 2];

    heap_reset(heap);
    darray_get(D5.forest, i, &tree);
#ifdef MODEL_HAS_EVENT_UNDOABLE
    now = state_copy(tree.root, heap);
#else
    now = tree.root;
#endif
    d5_traversal_push(now, 0, 0);
    while(depth >= 0) {

      /* reinitialise the heap if it grows too large */
#ifdef MODEL_HAS_EVENT_UNDOABLE
      if(heap_size(heap) >= 1000000) {
        state_t copy = state_copy(now, SYSTEM_HEAP);
        heap_reset(heap);
        now = state_copy(copy, heap);
        state_free(copy);
      }
#endif

      /* get the state on top of the stack */
      const d5_traversal_stack_item_t top = stack[depth];
      st = darray_get_ptr(tree.nodes, top.ref);
            
      /* top state needs to be processed.  call the hook on it */
      if(d5_get_flag(st->flags, step, D5_FLAG_PROC)) {
        const d5_st_ref_t ref = { .tr = i, .st = top.ref };
        hook(ref, top.now, top.en);
        /*!!!*/
        /* the hook may have changed pointers in D5.forest. so st and
          tree are not valid anymore */
        darray_get(D5.forest, i, &tree);
        st = darray_get_ptr(tree.nodes, top.ref);
        /*!!!*/ 
        d5_unset_flag(st->flags, step, D5_FLAG_PROC);
      }

      /* top state does not need to be visited.  just pop it and
         update the heap position */
      if(!d5_get_flag(st->flags, step, D5_FLAG_VISIT)) {
#ifdef MODEL_HAS_EVENT_UNDOABLE
        if(depth > 0) {
          event_undo(top.e, now);
        }
#endif
        depth --;
        heap_set_position(heap, top.pos);

      } else {

        /* all events of the top state have been processed.  unset its
           visit bit (=> it will be popped at the next iteration) */
        if(top.nxt >= st->no_children) {
          d5_unset_flag(st->flags, step, D5_FLAG_VISIT);
        } else {

          /* check if the next child must be visited or processed.  if
             so we push it on the stack */
          const d5_st_lref_t child_ref = st->fst_child + top.nxt;
          const d5_st_t * st_child = darray_get_ptr(tree.nodes, child_ref);
          const bool_t visit_child =
            d5_get_flag(st_child->flags, step, D5_FLAG_VISIT) ||
            d5_get_flag(st_child->flags, step, D5_FLAG_PROC);
          stack[depth].nxt ++;

          if(visit_child) {
            const event_t e = * (event_t *) list_nth(top.en, st_child->evt);
#ifdef MODEL_HAS_EVENT_UNDOABLE
            event_exec(e, now);
#else
            now = state_succ(top.now, e, heap);
#endif
            d5_traversal_push(now, e, child_ref);
            context_incr_stat(STAT_EVENT_EXEC, 0, 1);
          }
        }
      }
    }
  }
  heap_free(heap);
}


/**
 *  d5_dd_hook +
 *
 */
void
d5_dd_hook
(d5_st_ref_t s_ref,
 state_t s,
 list_t en) {
  const uint16_t size = state_char_size(s);
  const mem_size_t heap_pos = heap_get_position(D5.cd_heap);
  hset_mdata_t mdata;
  d5_cd_t cd = {
    .pe = D5.pe_me,
    .size = size,
    .s = mem_alloc(D5.cd_heap, size),
    .hash = state_hash(s)
  };
  state_serialise(s, cd.s, &cd.size);
  if(hset_insert(D5.recons, &cd, &mdata)) {
    darray_push(D5.recons_by_pe[d5_get_dp(cd.hash)], &mdata.ref);
  } else {
    heap_set_position(D5.cd_heap, heap_pos);
  }
}


/**
 * d5_insert_new_states
 */
void
d5_insert_new_states
() {
  const uint32_t no_cand = darray_size(D5.cd_idx);
  for(uint32_t i = 0; i < no_cand; i ++) {
    d5_cd_t * cd;
    hset_ref_t ref;

    darray_get(D5.cd_idx, i, &ref);
    cd = hset_get_ptr(D5.cd, ref);
    LNA_ASSERT(cd, D5_ERR_CAND_NOT_FOUND);
    if(!cd->del) {
      d5_tree_t * tr = (d5_tree_t *) darray_get_ptr(D5.forest, cd->father.tr);
      d5_st_t * father = (d5_st_t *) darray_get_ptr(tr->nodes, cd->father.st);
      d5_st_t new_st = {
        .father = cd->father.st,
        .evt = cd->evt,
        .fst_child = 0,
        .flags = 0,
        .no_children = 0
      };
      const d5_st_ref_t new = {
        .tr = cd->father.tr,
        .st = darray_size(tr->nodes)
      };
      d5_href_t href = {
        .hash = cd->hash,
        .ref = new,
        .pe = D5.pe_me
      };
      if(0 == father->no_children) {
        father->fst_child = darray_size(tr->nodes);
      } 
      father->no_children ++;
      darray_push(tr->nodes, &new_st);
      darray_push(D5.new_refs[d5_get_dp(cd->hash)], &href);
      context_incr_stat(STAT_STATES_STORED, 0, 1);
      D5_SH_STAT[D5.pe_me].open_next ++;
    }
  }
}


/**
 *  d5_push_ro_block
 */
void
d5_push_ro_block
(bool_t last) {
  uint32_t pos = 0;

  if(last) {
    if(D5_SH_RO.data[D5_SH_RO.nxt_blk_idx].no == 0) {
      return;
    }
  } else if(D5_SH_RO.data[D5_SH_RO.nxt_blk_idx].no < CFG_D5_RO_BLOCK_SIZE) {
    return;
  }
  if(D5_SH_RO.nxt_blk_idx > 0) {
    const int prev = D5_SH_RO.nxt_blk_idx - 1;
    pos = D5_SH_RO.data[prev].pos + D5_SH_RO.data[prev].size;
  }
  D5_SH_RO.data[D5_SH_RO.nxt_blk_idx].pos = pos;
  D5_SH_RO.data[D5_SH_RO.nxt_blk_idx].size = D5_SH_RO.size - pos;
  D5_SH_RO.avail[D5_SH_RO.nxt_blk_idx] = D5_RO_AVAIL;
  shmem_int_finc(&D5_SH_RO.last_avail, D5.pe_me);
  D5_SH_RO.nxt_blk_idx ++;
  D5_SH_RO.data[D5_SH_RO.nxt_blk_idx].no = 0;
  D5_SH_RO.data[D5_SH_RO.nxt_blk_idx].size = 0;
}


/**
 *  d5_tag_leaves_for_exp
 *
 *  all the leaves of the forest are tagged for D5_STEP_EXP as well as
 *  the paths to these
 */
void
d5_tag_leaves_for_exp
() {
  for(int i = 0; i < darray_size(D5.forest); i ++) {
    d5_tree_t * tree = darray_get_ptr(D5.forest, i);
    const darray_size_t no = darray_size(tree->nodes);
    for(int j = tree->idx_fst_leaf; j < no; j ++) {
      const d5_st_ref_t ref = { .tr = i, .st = j };
      d5_tag_path(ref, D5_STEP_EXP);
    }
    tree->idx_fst_leaf = no;
  }
}


/**
 *  d5_exp_leaf +
 *
 *  expand a leaf state s referenced in the forest by s_ref: all
 *  successors of s are put in D5.cd
 */
void
d5_exp_leaf
(state_t s,
 d5_st_ref_t s_ref) {
  list_t en = state_events(s, D5.exp_heap);
  list_iter_t it;
  d5_evt_num_t evt = 0;

  list_for_each(en, it) {
    const mem_size_t heap_pos = heap_get_position(D5.cd_heap);
    const event_t e = * ((event_t *) list_iter_item(it));
    const state_t succ = state_succ(s, e, D5.exp_heap);
    const uint16_t succ_size = state_char_size(succ);
    hset_mdata_t mdata;
    d5_cd_t cd = {
      .evt = evt,
      .del = TRUE,
      .pe = D5.pe_me,
      .father = s_ref,
      .size = succ_size,
      .hash = state_hash(succ),
      .s = mem_alloc(D5.cd_heap, succ_size)
    };
    state_serialise(succ, cd.s, &cd.size);
    if(!hset_insert(D5.cd, &cd, &mdata)) {
      heap_set_position(D5.cd_heap, heap_pos);
    } else {
      d5_cd_t * new = hset_get_ptr(D5.cd, mdata.ref);
      new->ref = mdata.ref;
      darray_push(D5.cd_idx, &mdata.ref);
      darray_push(D5.cd_idx_by_pe[d5_get_dp(mdata.hash)], &mdata.ref);
    }
    evt ++;
  }
  assert(D5_SH_STAT[D5.pe_me].open_now > 0);
  D5_SH_STAT[D5.pe_me].open_now --;
  if(hset_num_items(D5.cd) >= CFG_D5_CAND_SET_SIZE) {
    d5_step_dd(FALSE);
  }
  context_incr_stat(STAT_STATES_PROCESSED, 0, 1);
  context_incr_stat(STAT_ARCS, 0, evt);
  context_incr_stat(STAT_EVENT_EXEC, 0, evt);
  if(0 == evt) {
    context_incr_stat(STAT_STATES_DEADLOCK, 0, 1);
  }
}


/**
 *  d5_exp_hook
 *
 *  hook called during the state expansion step on each frontier
 *  state.  serialise the state in D5_SH_RO_ST
 */
void
d5_exp_hook
(d5_st_ref_t s_ref,
 state_t s,
 list_t en) {
  const bool_t full = (++ D5.no_ro) >= CFG_D5_RO_SET_SIZE;
  uint16_t s_size = state_char_size(s);

  LNA_ASSERT
    ((D5_SH_RO.size + s_size + sizeof(d5_st_ref_t) + sizeof(uint16_t))
     < CFG_D5_BUFFER_SIZE / 2,
     "RO set full");

  /* serialise state length */
  memcpy(D5_SH_RO_ST + D5_SH_RO.size, &s_ref, sizeof(d5_st_ref_t));
  D5_SH_RO.size += sizeof(d5_st_ref_t);

  /* serialise state vector */  
  state_serialise(s, D5_SH_RO_ST + D5_SH_RO.size + sizeof(uint16_t), &s_size);
  memcpy(D5_SH_RO_ST + D5_SH_RO.size, &s_size, sizeof(uint16_t));

  D5_SH_RO.size += s_size + sizeof(uint16_t);
  D5_SH_RO.data[D5_SH_RO.nxt_blk_idx].no ++;

  /* potentially push the block and explore RO if set is full */
  d5_push_ro_block(full);
  if(full) {
    d5_explore_mine();
    d5_reset_ro_set();
  }
}


/**
 *  d5_steal_ro_frnd
 */
void
d5_steal_ro_frnd
(int pe,
 uint32_t * no_stolen,
 char ** stolen) {
  if(pe == D5.pe_me) {
    if(D5_SH_RO.last_avail < 0) {
      return;
    } else if(D5_RO_AVAIL ==
              shmem_int_cswap(&D5_SH_RO.avail[D5_SH_RO.last_avail],
                              D5_RO_AVAIL, D5_RO_EMPTY, pe)) {
      d5_box_t data;
      *stolen = d5_get_array
        (&data, &D5_SH_RO.data[D5_SH_RO.last_avail], NULL, D5_SH_RO_ST, pe);
      *no_stolen = data.no;
      D5_SH_RO.last_avail --;
      return;
    } else {
      int avail;
      do {
        shmem_int_get
          (&avail, &D5_SH_RO.avail[D5_SH_RO.last_avail], 1, D5.pe_me);
      } while(avail != D5_RO_EMPTY);
      D5_SH_RO.last_avail --;
      d5_steal_ro_frnd(pe, no_stolen, stolen);
    }
  } else {
    int avail[D5_RO_NO_BLOCK];
    const uint32_t rnd = (uint32_t) random_int(&D5.rseed);
    shmem_getmem(avail, D5_SH_RO.avail, sizeof(int) * D5_RO_NO_BLOCK, pe);
    for(int i = 0; i < D5_RO_NO_BLOCK; i ++) {
      const int idx = (rnd + i) % D5_RO_NO_BLOCK;
      if(D5_RO_AVAIL == avail[idx]) {
        assert(idx <= D5_RO_NO_BLOCK && idx >= 0);
        if(D5_RO_AVAIL ==
           shmem_int_cswap(&D5_SH_RO.avail[idx],
                           D5_RO_AVAIL, D5_RO_READ, pe)) {
          d5_box_t data;
          *stolen = d5_get_array
            (&data, &D5_SH_RO.data[idx], NULL, D5_SH_RO_ST, pe);
          *no_stolen = data.no;
          assert(idx <= D5_RO_NO_BLOCK && idx >= 0);
          LNA_ASSERT
            (D5_RO_READ ==
             shmem_int_cswap(&D5_SH_RO.avail[idx],
                             D5_RO_READ, D5_RO_EMPTY, pe),
             "invalid RO block status");
          context_incr_stat(STAT_STEAL_SUCCESSES, 0, 1);
        }
        return;
      }
    }
  }
}


/**
 *  d5_steal_ro
 *
 *  try to steal a block of reconstructed open states from PE
 *  pe.  if no states have been stolen, *stolen = NULL and
 *  *no_stolen = 0, after the call.  otherwise *stolen is a pointer to
 *  stolen states
 */
void
d5_steal_ro
(int pe,
 uint32_t * no_stolen,
 char ** stolen) {
  int idx, add;
  int * ptr;

  if(pe != D5.pe_me) {
    context_incr_stat(STAT_STEAL_ATTEMPTS, 0, 1);
  }
  *stolen = NULL;
  *no_stolen = 0;
  if(D5.steal_strategy == D5_STEAL_FRND) {
    d5_steal_ro_frnd(pe, no_stolen, stolen);
    return;
  }
  if(pe == D5.pe_me) {
    ptr = &D5_SH_RO.last_avail;
    add = - 1;
  } else {
    ptr = &D5_SH_RO.first_avail;
    add = 1;
  } 
  shmem_int_get(&idx, ptr, 1, pe);
  if(idx < 0) {
    return;
  }
  assert(idx < D5_RO_NO_BLOCK);
  if(D5_RO_AVAIL ==
     shmem_int_cswap(&D5_SH_RO.avail[idx], D5_RO_AVAIL, D5_RO_READ, pe)) {
    d5_box_t data;
    shmem_int_fadd(ptr, add, pe);
    *stolen = d5_get_array(&data, &D5_SH_RO.data[idx], NULL, D5_SH_RO_ST, pe);
    *no_stolen = data.no;
    LNA_ASSERT
      (D5_RO_READ ==
       shmem_int_cswap(&D5_SH_RO.avail[idx], D5_RO_READ, D5_RO_EMPTY, pe),
       "invalid RO block status");
    if(pe != D5.pe_me) {
      context_incr_stat(STAT_STEAL_SUCCESSES, 0, 1);
    }
  }
}


/**
 *  d5_process_stolen
 *
 *  process a block of stolen states: unserialise all states from
 *  stolen and call d5_exp_leaf on each of these states.  if the
 *  states that have been stolen are not mine then a new tree is
 *  planted for all of these states
 */
void
d5_process_stolen
(bool_t stole_mine,
 uint32_t no_stolen,
 char * stolen) {
  char * pos = stolen;
  for(int i = 0; i < no_stolen; i ++) {
    d5_st_ref_t ref;
    uint16_t s_size;
    state_t s;
    heap_reset(D5.exp_heap);
    memcpy(&ref, pos, sizeof(d5_st_ref_t));
    pos += sizeof(d5_st_ref_t);
    memcpy(&s_size, pos, sizeof(uint16_t));
    pos += sizeof(uint16_t);
    if(stole_mine) {
      s = state_unserialise(pos, D5.exp_heap);
      d5_exp_leaf(s, ref);
    } else {
      s = state_unserialise(pos, NULL);
      ref = d5_plant_new_tree(s, TRUE);
      d5_exp_leaf(s, ref);
    }
    pos += s_size;
  }
}


/**
 *  d5_reset_ro_set
 *
 *  reset the set of reconstructed open states
 */
void
d5_reset_ro_set
() {
  memset(&D5_SH_RO, 0, sizeof(d5_ro_data_t));
  D5_SH_RO.last_avail = - 1;
  D5.no_ro = 0;
}


/**
 *  d5_check_local_termination
 *
 *  return TRUE if and only if the PE does not have any leaf to expand
 *  in its forest
 */
bool_t
d5_check_local_termination
() {
  for(int i = darray_size(D5.forest) - 1; i >= 0; i --) {
    const d5_tree_t * tree = darray_get_ptr(D5.forest, i);
    if(tree->idx_fst_leaf != darray_size(tree->nodes)) {
      return FALSE;
    }
  }
  return TRUE;
}


/**
 *  d5_put_stats
 */
void
d5_put_stats
() {
  shmem_putmem
    (&D5_SH_STAT[D5.pe_me], &D5_SH_STAT[D5.pe_me], sizeof(d5_stat_data_t), 0);
}


/**
 *  d5_get_stats
 */
void
d5_get_stats
() {
  shmem_getmem(D5_SH_STAT, D5_SH_STAT, D5.pes * sizeof(d5_stat_data_t), 0);
}


/**
 *  d5_explore_mine
 *
 *  explore states in my RO set
 */
void
d5_explore_mine
() {
  char * stolen;
  uint32_t no_stolen;
  do {
    d5_steal_ro(D5.pe_me, &no_stolen, &stolen);
    if(stolen) {
      d5_process_stolen(TRUE, no_stolen, stolen);
      free(stolen);
    }
  } while(stolen);

  /**
   *  wait that all ro block have been consumed
   */
  int idx = D5_SH_RO.last_avail;
  while(idx >= 0) {
    int avail;
    shmem_int_get(&avail, &D5_SH_RO.avail[idx], 1, D5.pe_me);
    if(avail == D5_RO_AVAIL) {
      D5_SH_RO.last_avail = idx;
      d5_steal_ro(D5.pe_me, &no_stolen, &stolen);
      LNA_ASSERT(stolen, "could not steal a forgotten RO block");
      d5_process_stolen(TRUE, no_stolen, stolen);
      free(stolen);
    } else {
      do {
        shmem_int_get(&avail, &D5_SH_RO.avail[idx], 1, D5.pe_me);
      } while(avail != D5_RO_EMPTY);
      idx --;
    }
  }
}


int
d5_sort_victims
(const void * pex,
 const void * pey) {
  int x = * (int *) pex;
  int y = * (int *) pey;
  if(D5_SH_STAT[x].open_now < D5_SH_STAT[y].open_now) {
    return 1;
  }
  if(D5_SH_STAT[x].open_now > D5_SH_STAT[y].open_now) {
    return -1;
  }
  return 0;
}


/**
 *  d5_do_dd
 *
 *  decide if we perform duplicate detection
 */
bool_t
d5_do_dd
() {
  switch(D5.dd_strategy) {
  case D5_DD_STEAL:
    return D5.steal_attempts >= D5.pes - 1;
  case D5_DD_TIME: {
    double elapsed = clock() - D5.dd_last_time;
    return (1000 * elapsed / CLOCKS_PER_SEC) >= CFG_D5_DD_STRATEGY_TIME_MS;
  }
  }
  assert(0);
}


/**
 *  d5_explore_others
 *
 *  explore states in a peer RO set
 */
void
d5_explore_others
() {
  char * stolen;
  uint32_t no_stolen;
  int victim = 0;
  bool_t loop = TRUE;
  int victims[D5.pes];  //  strategy INFO

  D5.steal_attempts = 0;

  /*  set the initial victim  */
  switch(D5.steal_strategy) {
  case D5_STEAL_RING:
  case D5_STEAL_NEIGH:
    victim = D5.pe_prev;
    break;
  case D5_STEAL_MEM:
    victim = D5.last_victim;
    break;
  case D5_STEAL_RND:
  case D5_STEAL_FRND:
    do {
      victim = random_int(&D5.rseed) % D5.pes;
    } while(victim == D5.pe_me);
    break;
  case D5_STEAL_INFO:
    for(int i = 0; i < D5.pes; i ++) {
      victims[i] = i;
    }
    qsort(victims, D5.pes, sizeof(int), d5_sort_victims);
    victim = victims[0];
    break;
  }

  while(loop) {
    d5_steal_ro(victim, &no_stolen, &stolen);
    if(stolen) {
      D5.steal_attempts = 0;
      d5_process_stolen(FALSE, no_stolen, stolen);
      free(stolen);
      D5.last_victim = victim;
    } else {
      D5.steal_attempts ++;
      switch(D5.steal_strategy) {
      case D5_STEAL_MEM:
      case D5_STEAL_RING:
        do {
          victim = (0 == victim) ? (D5.pes - 1) : (victim - 1);
        } while(victim == D5.pe_me);
        break;
      case D5_STEAL_INFO:
        victim = victims[D5.steal_attempts % D5.pes];
        break;
      case D5_STEAL_FRND:
      case D5_STEAL_RND:
        do {
          victim = random_int(&D5.rseed) % D5.pes;
        } while(victim == D5.pe_me);
        break;
      case D5_STEAL_NEIGH:
        break;
      }
    }

    //  check if we need to trigger dd
    if(d5_do_dd()) {
      D5.steal_attempts = 0;
      loop = d5_step_dd(TRUE);
      if(D5.steal_strategy == D5_STEAL_INFO) {
        qsort(victims, D5.pes, sizeof(int), d5_sort_victims);
        victim = victims[0];
      }
    }
  }
  D5.steal_attempts = 0;
}


/**
 *  d5_process_level
 */
void
d5_process_level
() {
  /* initialise some stuff */
  D5_SH_STAT[D5.pe_me].open_now = D5_SH_STAT[D5.pe_me].open_next;
  D5_SH_STAT[D5.pe_me].open_next = 0;
  D5.dd_last_time = clock();
  
  d5_tag_leaves_for_exp();
  d5_reset_ro_set();
  d5_traversal(D5_STEP_EXP, d5_exp_hook);
  d5_push_ro_block(TRUE);
  d5_explore_mine();
  d5_explore_others();
}


/**
 *  d5_initialisation
 *
 *  initialise all data structures (private or shared)
 */
void
d5_initialisation
() {
  d5_common_initialisation();
  D5.no_ro = 0;
  D5.forest = darray_new(NULL, sizeof(d5_tree_t));
  D5.last_victim = (D5.pe_me == 0) ? (D5.pes - 1) : (D5.pe_me - 1);
  D5.rseed = random_seed(D5.pe_me);
  D5.term = 0;
  COMM_MALLOC(D5_SH_RO_ST, CFG_D5_BUFFER_SIZE / 2);
  COMM_MALLOC(D5_SH_STAT, sizeof(d5_stat_data_t) * D5.pes);
  COMM_MALLOC(D5_SH_MOVES, sizeof(uint32_t) * D5.pes * D5.pes);
  d5_reset_ro_set();

  /* set the stealing strategy */
  if(0 == strcmp(CFG_D5_STEAL_STRATEGY, "info")) {
    D5.steal_strategy = D5_STEAL_INFO;
  } else if(0 == strcmp(CFG_D5_STEAL_STRATEGY, "mem")) {
    D5.steal_strategy = D5_STEAL_MEM;
  } else if(0 == strcmp(CFG_D5_STEAL_STRATEGY, "neigh")) {
    D5.steal_strategy = D5_STEAL_NEIGH;
  } else if(0 == strcmp(CFG_D5_STEAL_STRATEGY, "ring")) {
    D5.steal_strategy = D5_STEAL_RING;
  } else if(0 == strcmp(CFG_D5_STEAL_STRATEGY, "rnd")) {
    D5.steal_strategy = D5_STEAL_RND;
  } else if(0 == strcmp(CFG_D5_STEAL_STRATEGY, "frnd")) {
    D5.steal_strategy = D5_STEAL_FRND;
  } else {
    LNA_ASSERT(0, "no stealing strategy set\n");
  }

  /* set the dd strategy */
  if(0 == strcmp(CFG_D5_DD_STRATEGY, "steal")) {
    D5.dd_strategy = D5_DD_STEAL;
  } else if(0 == strcmp(CFG_D5_DD_STRATEGY, "time")) {
    D5.dd_strategy = D5_DD_TIME;
  } else {
    LNA_ASSERT(0, "no dd strategy set\n");
  }
 
  /* PE 0 plants a new tree with s0 as root */
  state_t s = state_initial(NULL);
  hval_t hash = state_hash(s);
  if(0 == D5.pe_me) {
    d5_plant_new_tree(s, FALSE);
  } else {
    state_free(s);
  }

  /* dp of initial state inserts a reference in D5.hash */
  if(d5_get_dp(hash) == D5.pe_me) {
    d5_href_t new = {
      .pe = 0,
      .hash = hash,
      .ref = {.tr = 0, .st = 0}
    };
    hset_insert(D5.hash, &new, NULL);
  }
  comm_barrier();
}


/**
 *  d5_finalisation
 *
 *  free all data structures (private or shared)
 */
void
d5_finalisation
() {
  uint64_t delta = 0;

  /* compute statistics */
  context_set_stat(STAT_STATES_STORED_FULL, 0, darray_size(D5.forest));
  for(int i = 0; i < darray_size(D5.forest); i ++) {
    const d5_tree_t * tree = darray_get_ptr(D5.forest, i);
    delta += darray_size(tree->nodes) - 1;
  }
  context_set_stat(STAT_STATES_STORED_DELTA, 0, delta);

  /* free everything */
  for(int i = 0; i < darray_size(D5.forest); i ++) {
    const d5_tree_t * tree = darray_get_ptr(D5.forest, i);
    darray_free(tree->nodes);
    free(tree->root);
  }
  darray_free(D5.forest);
  shmem_free(D5_SH_RO_ST);
  shmem_free(D5_SH_STAT);
  d5_common_finalisation();
}

#endif
