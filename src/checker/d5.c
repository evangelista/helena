#include "common.h"
#include "config.h"
#include "debug.h"
#include "d5_lb.h"
#include "d5_ws.h"
#include "d5.h"
#include "context.h"

#if CFG_ALGO_D5 == 1

#include "shmem.h"


static d5_term_data_t D5_SH_TERM;

d5_glob_data_t D5;
int D5_BUF_IDX = 0;

#define d5_buf_get(_val, _size, _pos) {                 \
    LNA_ASSERT((_pos) + (_size) <= CFG_D5_BUFFER_SIZE,  \
               D5_ERR_BUFFER_TOO_SMALL);                \
    memcpy(_val, D5.buf + _pos, _size);                 \
    _pos += _size;                                      \
  }

#define d5_sh_buf_put(_val, _size, _pos) {              \
    LNA_ASSERT((_pos) + (_size) <= CFG_D5_BUFFER_SIZE,  \
               D5_ERR_BUFFER_TOO_SMALL);                \
    memcpy(D5.sh_buf[D5_BUF_IDX] + _pos, _val, _size);  \
    _pos += _size;                                      \
  }

#define D5_PUSH_LOOP_START(_darr)                                     \
  uint32_t pos = 0;                                                   \
  memset(D5.sh_box[D5_BUF_IDX], 0, sizeof(d5_box_t) * D5.pes);        \
  for(int pe = 0; pe < D5.pes; pe ++) {                               \
  const uint32_t old_pos = pos;                                       \
  const uint32_t no = darray_size(_darr[pe]);
#define D5_PUSH_LOOP_END(_no)                                         \
  D5.sh_box[D5_BUF_IDX][pe].no = _no;                                 \
  D5.sh_box[D5_BUF_IDX][pe].pos = old_pos;                            \
  D5.sh_box[D5_BUF_IDX][pe].size = pos - old_pos;                     \
  }

#define D5_PULL_LOOP_START()                                            \
  for(int n = 0; n < D5.pes; n ++) {                                    \
  const int pe = (n + D5.pe_me) % D5.pes;                               \
  d5_box_t box;                                                         \
  uint32_t pos = 0;                                                     \
  d5_get_array(&box, &D5.sh_box[D5_BUF_IDX][D5.pe_me],                  \
               D5.buf, D5.sh_buf[D5_BUF_IDX], pe);                      \
  for(int i = 0; i < box.no; i ++) {
#define D5_PULL_LOOP_END() }}


hkey_t
d5_cd_hash
(void * cand) {
  return ((d5_cd_t *) cand)->hash;
}


char
d5_cd_equal
(void * a,
 void * b) {
  d5_cd_t * ca = (d5_cd_t *) a;
  d5_cd_t * cb = (d5_cd_t *) b;
  return ca->hash == cb->hash
    && ca->size == cb->size
    && 0 == memcmp(ca->s, cb->s, ca->size);
}


hkey_t
d5_href_hash
(void * href) {
  return ((d5_href_t *) href)->hash;
}


char *
d5_get_array
(d5_box_t * dst_box,
 d5_box_t * src_box,
 char * dst,
 char * src,
 int pe) {
  char * result;
  comm_getmem(dst_box, src_box, sizeof(d5_box_t), pe);
  result = (!dst) ? malloc(dst_box->size) : dst;
  if(dst_box->size > 0) {
    comm_getmem(result, src + dst_box->pos, dst_box->size, pe);
  }
  return result;
}


/**
 * @d5_reset_candidate_set +
 */
void
d5_reset_candidate_set
() {
  hset_empty(D5.recons);
  hset_empty(D5.cd);
  heap_reset(D5.cd_heap);
  darray_reset(D5.cd_idx);
  for(int pe = 0; pe < D5.pes; pe ++) {
    darray_reset(D5.cd_idx_by_pe[pe]);
    darray_reset(D5.recons_by_pe[pe]);
    darray_reset(D5.new_refs[pe]);
    darray_reset(D5.refs[pe]);
  }
}


/**
 * @d5_exchange_candidates
 */
void
d5_hash_received
(hval_t hash,
 void * item,
 void * data) {
  d5_href_t * href = (d5_href_t *) item;
  darray_push(D5.refs[href->pe], &href->ref);
}

void
d5_exchange_candidates
() {
  D5_BUF_IDX = 0;
  D5_PUSH_LOOP_START(D5.cd_idx_by_pe) {
    for(int i = 0; i < no; i ++) {
      hset_ref_t * ref = darray_get_ptr(D5.cd_idx_by_pe[pe], i);
      d5_cd_t * cd = hset_get_ptr(D5.cd, *ref);
      LNA_ASSERT(cd, D5_ERR_CAND_NOT_FOUND);
      d5_sh_buf_put(ref, sizeof(hset_ref_t), pos);
      d5_sh_buf_put(&cd->hash, sizeof(hkey_t), pos);
      d5_sh_buf_put(&cd->size, sizeof(uint16_t), pos);
      d5_sh_buf_put(cd->s, cd->size, pos);
      hset_hash_fold(D5.hash, cd->hash, &d5_hash_received, NULL);
    }
  } D5_PUSH_LOOP_END(no);
  /*!!!*/  comm_barrier();
  for(int pe = 0; pe < D5.pes; pe ++) {
    darray_reset(D5.cd_idx_by_pe[pe]);
  }
  d5_cd_t cd = { .del = FALSE };
  D5_PULL_LOOP_START() {
    const mem_size_t heap_pos = heap_get_position(D5.cd_heap);
    hset_mdata_t mdata;
    cd.pe = pe;
    d5_buf_get(&cd.ref, sizeof(hset_ref_t), pos);
    d5_buf_get(&cd.hash, sizeof(hkey_t), pos);
    d5_buf_get(&cd.size, sizeof(uint16_t), pos);
    cd.s = mem_alloc(D5.cd_heap, cd.size);
    d5_buf_get(cd.s, cd.size, pos);
    if(hset_insert(D5.cd, &cd, &mdata)) {
      hset_hash_fold(D5.hash, cd.hash, &d5_hash_received, NULL);
    } else {
      heap_set_position(D5.cd_heap, heap_pos);
    }
    ((d5_cd_t *) hset_get_ptr(D5.cd, mdata.ref))->del = FALSE;
    darray_push(D5.cd_idx_by_pe[pe], &mdata.ref);
  } D5_PULL_LOOP_END();
}


/**
 * @d5_exchange_to_reconstruct
 */
void
d5_exchange_to_reconstruct
() {
  D5_BUF_IDX = 1;
  D5_PUSH_LOOP_START(D5.refs) {
    d5_sh_buf_put
      (darray_get_ptr(D5.refs[pe], 0), no * sizeof(d5_st_ref_t), pos);
  } D5_PUSH_LOOP_END(no);
  /*!!!*/  comm_barrier();
  D5_PULL_LOOP_START() { 
    d5_st_ref_t ref;
    d5_buf_get(&ref, sizeof(d5_st_ref_t), pos);
    d5_tag_for_dd(ref);
  } D5_PULL_LOOP_END();
}


/**
 * @d5_exchange_reconstructed
 */
void
d5_exchange_reconstructed
() {
  D5_BUF_IDX = 0;
  D5_PUSH_LOOP_START(D5.recons_by_pe) {
    for(int i = 0; i < no; i ++) {
      hset_ref_t * ref = darray_get_ptr(D5.recons_by_pe[pe], i);
      d5_cd_t * cd = hset_get_ptr(D5.recons, *ref);
      LNA_ASSERT(cd, D5_ERR_CAND_NOT_FOUND);
      d5_sh_buf_put(&cd->hash, sizeof(hkey_t), pos);
      d5_sh_buf_put(&cd->size, sizeof(uint16_t), pos);
      d5_sh_buf_put(cd->s, cd->size, pos);
    }
  } D5_PUSH_LOOP_END(no);
  /*!!!*/  comm_barrier();
  D5_PULL_LOOP_START() {
    hset_mdata_t mdata;
    d5_cd_t cd;
    d5_buf_get(&cd.hash, sizeof(hkey_t), pos);
    d5_buf_get(&cd.size, sizeof(uint16_t), pos);
    cd.s = D5.buf + pos;
    pos += cd.size;
    if(hset_contains(D5.cd, &cd, &mdata)) {
      ((d5_cd_t *) hset_get_ptr(D5.cd, mdata.ref))->del = TRUE;
    }
  } D5_PULL_LOOP_END();
}


/**
 * @d5_exchange_new_states
 */
void
d5_exchange_new_states
() {
  uint32_t new;
  D5_BUF_IDX = 1;
  D5_PUSH_LOOP_START(D5.cd_idx_by_pe) {
    new = 0;
    for(int i = 0; i < no; i ++) {
      hset_ref_t * ref = darray_get_ptr(D5.cd_idx_by_pe[pe], i);
      d5_cd_t * cd = hset_get_ptr(D5.cd, *ref);
      LNA_ASSERT(cd, D5_ERR_CAND_NOT_FOUND);
      if(!cd->del && cd->pe == pe) {
        d5_sh_buf_put(&cd->ref, sizeof(hset_ref_t), pos);
        new ++;
      }
    }
  } D5_PUSH_LOOP_END(new);
  /*!!!*/  comm_barrier();
  D5_PULL_LOOP_START() {
    hset_ref_t ref;
    d5_cd_t * cd;
    d5_buf_get(&ref, sizeof(hset_ref_t), pos);
    cd = hset_get_ptr(D5.cd, ref);
    LNA_ASSERT(cd, D5_ERR_CAND_NOT_FOUND);
    cd->del = FALSE;
  } D5_PULL_LOOP_END();
}


/**
 * @d5_exchange_new_hashes
 */
void
d5_exchange_new_hashes
() {
  D5_BUF_IDX = 0;
  D5_PUSH_LOOP_START(D5.new_refs) {
    d5_sh_buf_put
      (darray_get_ptr(D5.new_refs[pe], 0), no * sizeof(d5_href_t), pos);
  } D5_PUSH_LOOP_END(no);
  /*!!!*/  comm_barrier();
  D5_PULL_LOOP_START() {
    d5_href_t ref;
    d5_buf_get(&ref, sizeof(d5_href_t), pos);
    hset_insert(D5.hash, &ref, NULL);
  } D5_PULL_LOOP_END();
}


/**
 * @d5_step_dd
 */
bool_t
d5_step_dd
(bool_t level_done) {
  d5_term_data_t term;

  context_incr_stat(STAT_DUPLICATE_DETECTIONS, 0, 1);
  lna_timer_start(&D5.dd_timer);

  /*  step 1  */
  d5_exchange_candidates();
  D5_SH_TERM.level = 0;
  D5_SH_TERM.glob = 0;

  /*  step 2 */
  d5_exchange_to_reconstruct();
  if(level_done) {
    shmem_int_finc((int *) &D5_SH_TERM.level, 0);
  }
  d5_traversal(D5_STEP_DDD, d5_dd_hook);

  /*  step 3  */
  d5_exchange_reconstructed();

  /*  step 4  */
  d5_exchange_new_states();
  d5_insert_new_states();
  if(d5_check_local_termination()) {
    shmem_int_finc((int *) &D5_SH_TERM.glob, 0);
  }

#if CFG_D5_WS == 1  /*  work-stealing strategy  */
  d5_put_stats();
#endif

  /*  step 5  */
  d5_exchange_new_hashes();
  
#if CFG_D5_WS == 1  /*  work-stealing strategy  */
  d5_get_stats();
#endif

  /*  check for local and global termination  */
  shmem_getmem(&term, &D5_SH_TERM, sizeof(d5_term_data_t), 0);
  D5.term = term.glob == D5.pes;
  d5_reset_candidate_set();
  comm_barrier();
  lna_timer_stop(&D5.dd_timer);
  D5.dd_last_time = clock();
  return term.level != D5.pes;
}


/**
 *  d5_common_initialisation
 */
void
d5_common_initialisation
() {
  const uint32_t buffer_size =
    CFG_D5_BUFFER_SIZE / (CFG_D5_WS ? 4 : 2);
  D5.depth = 0;
  D5.here = 0;
  D5.pe_me = comm_me();
  D5.pes = comm_pes();
  D5.pe_prev = (0 == D5.pe_me) ? (D5.pes - 1) : (D5.pe_me - 1);
  D5.pe_next = (D5.pe_me + 1) % D5.pes;
  D5.cd_heap = local_heap_new();
  D5.exp_heap = local_heap_new();
  D5.buf = malloc(buffer_size);
  lna_timer_init(&D5.dd_timer);
  D5.cd =
    hset_new(sizeof(d5_cd_t), CFG_D5_CAND_SET_SIZE,
             FALSE, &d5_cd_hash, &d5_cd_equal);
  D5.recons =
    hset_new(sizeof(d5_cd_t), CFG_D5_CAND_SET_SIZE,
     FALSE, &d5_cd_hash, &d5_cd_equal);
  D5.hash =
    hset_new(sizeof(d5_href_t), CFG_HASH_SIZE,
             FALSE, &d5_href_hash, NULL);
  D5.cd_idx = darray_new(NULL, sizeof(hset_ref_t));
  D5.cd_idx_by_pe = malloc(sizeof(darray_t) * D5.pes);
  D5.recons_by_pe = malloc(sizeof(darray_t) * D5.pes);
  D5.new_refs = malloc(sizeof(darray_t) * D5.pes);
  D5.refs = malloc(sizeof(darray_t) * D5.pes);
  for(int pe = 0; pe < D5.pes; pe ++) {
    D5.cd_idx_by_pe[pe] = darray_new(NULL, sizeof(hset_ref_t));
    D5.recons_by_pe[pe] = darray_new(NULL, sizeof(hset_ref_t));
    D5.new_refs[pe] = darray_new(NULL, sizeof(d5_href_t));
    D5.refs[pe] = darray_new(NULL, sizeof(d5_st_ref_t));
  }
  for(int i = 0; i < 2; i ++) {
    COMM_MALLOC(D5.sh_buf[i], buffer_size);
    COMM_MALLOC(D5.sh_box[i], sizeof(d5_box_t) * D5.pes);
  }
}


/**
 *  d5_common_finalisation
 */
void
d5_common_finalisation
(){
  hset_free(D5.hash);
  hset_free(D5.recons);
  hset_free(D5.cd);
  heap_free(D5.cd_heap);
  heap_free(D5.exp_heap);
  free(D5.buf);
  darray_free(D5.cd_idx);
  for(int pe = 0; pe < D5.pes; pe ++) {
    darray_free(D5.cd_idx_by_pe[pe]);
    darray_free(D5.new_refs[pe]);
    darray_free(D5.refs[pe]);
  }
  free(D5.cd_idx_by_pe);
  free(D5.new_refs);
  free(D5.refs);
  for(int i = 0; i < 2; i ++) {
    shmem_free(D5.sh_box[i]);
    shmem_free(D5.sh_buf[i]);
  }
  context_set_stat(STAT_BFS_LEVELS, 0, D5.depth);
  context_set_stat(STAT_TIME_DD, 0, (double) lna_timer_value(D5.dd_timer));
}


void
d5
() {
  d5_initialisation();
  while(!D5.term) {
    d5_process_level();
    D5.depth ++;
  }
  d5_finalisation();
}


int
d5_get_dp
(hkey_t hash) {
  return hash % D5.pes;
}

#endif
