/**
 * @file darray.h
 * @brief Implementation of dynamic arrays.
 * @date 6 nov 2017
 * @author Sami Evangelista
 */

#ifndef LIB_DARRAY
#define LIB_DARRAY

#include "heap.h"

typedef uint32_t darray_size_t;

typedef uint32_t darray_index_t;

typedef struct struct_darray_t * darray_t;


/**
 * @brief darray_new
 */
darray_t darray_new
(heap_t heap,
 uint32_t sizeof_item);


/**
 * @brief darray_free
 */
void darray_free
(darray_t darray);


/**
 * @brief darray_size
 */
darray_size_t darray_size
(darray_t darray);



/**
 * @brief darray_reset
 */
void darray_reset
(darray_t darray);


/**
 * @brief darray_sizeof_item
 */
uint32_t darray_sizeof_item
(darray_t darray);


/**
 * @brief darray_push
 */
void darray_push
(darray_t darray,
 void * item);


/**
 * @brief darray_pop
 */
void darray_pop
(darray_t darray,
 void * item);


/**
 * @brief darray_top
 */
void darray_top
(darray_t darray,
 void * item);


/**
 * @brief darray_get
 */
void darray_get
(darray_t darray,
 darray_index_t i,
 void * item);


/**
 * @brief darray_get_ptr
 */
void * darray_get_ptr
(darray_t darray,
 darray_index_t i);


/**
 * @brief darray_set
 */
void darray_set
(darray_t darray,
 darray_index_t i,
 void * item);


/**
 * @brief darray_foreach_beg
 */
#define darray_foreach_beg(_darr, _idx, _size, _type, _var) {   \
  const darray_size_t _size = darray_size(_darr);               \
  for(darray_index_t _idx = 0; _idx < _size; _idx ++) {         \
  _type * _var = darray_get_ptr(_darr, _idx);


/**
 * @brief darray_foreach_end
 */
#define darray_foreach_end() }}
  
  
#endif
