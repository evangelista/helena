#include "hset.h"
#include "assert.h"

#define HSET_SLOT_EMPTY 0
#define HSET_SLOT_OCCUPIED 1
#define HSET_SLOT_DELETED 2

#define HSET_MAX_ROW_LOOP 100

#define HSET_REF_PACK(_row, _slot)              \
  (((uint64_t) _row << 32) | _slot)
#define HSET_REF_UNPACK(_ref, _row, _slot) {    \
    _row = _ref >> 32;                          \
    _slot = _ref & 0xffffffff;                  \
  }

#define HSET_SLOT_ADDR(_hset, _row, _slot)                              \
  ((_hset)->rows[_row] +                                                \
   ((size_t) (((size_t) (_hset)->sizeof_slot) * ((size_t) (_slot)))))

#define HSET_SLOT_ADDR_STATUS(hset, ptr)        \
  (ptr)
#define HSET_SLOT_GET_STATUS(hset, ptr, status)                         \
  memcpy(status, HSET_SLOT_ADDR_STATUS(hset, ptr), sizeof(hset_slot_status_t));
#define HSET_SLOT_SET_STATUS(hset, ptr, status)                         \
  memcpy(HSET_SLOT_ADDR_STATUS(hset, ptr), status, sizeof(hset_slot_status_t));

#define HSET_SLOT_ADDR_HASH(hset, ptr)                          \
  ((HSET_SLOT_ADDR_STATUS(hset, ptr)) + sizeof(hset_slot_status_t))
#define HSET_SLOT_GET_HASH(hset, ptr, hash)                           \
  memcpy(hash, HSET_SLOT_ADDR_HASH(hset, ptr), sizeof(hval_t));
#define HSET_SLOT_SET_HASH(hset, ptr, hash)                         \
  memcpy(HSET_SLOT_ADDR_HASH(hset, ptr), hash, sizeof(hval_t));

#define HSET_SLOT_ADDR_ITEM(hset, ptr)                  \
  ((HSET_SLOT_ADDR_HASH(hset, ptr)) +                   \
   ((hset)->store_hash ? sizeof(hval_t) : 0))
#define HSET_SLOT_GET_ITEM(hset, ptr, item)                             \
  memcpy(item, HSET_SLOT_ADDR_ITEM(hset, ptr), hset->sizeof_item);
#define HSET_SLOT_SET_ITEM(hset, ptr, item)                             \
  memcpy(HSET_SLOT_ADDR_ITEM(hset, ptr), item, hset->sizeof_item);

#define HSET_MOVE_PTR_TO_NEXT_SLOT(hset, row, ptr) {                    \
    if((ptr += (hset)->sizeof_slot) ==                                  \
       HSET_SLOT_ADDR(hset, row, (hset)->base_size << row)) {           \
      ptr = (hset)->rows[row];                                          \
    }                                                                   \
  }

#define HSET_HASH_ITEM(_hset, _item)                    \
  ((_hset)->hash_func ?                                 \
   (_hset)->hash_func(_item) :                          \
   hset_default_hash(_item, (_hset)->hash_size))

#define HSET_CMP_ITEM(_hset, _item1, _item2)             \
  ((_hset)->cmp_func ?                                   \
   (_hset)->cmp_func(_item1, _item2) :                   \
   (!memcmp(_item1, _item2, (_hset)->hash_size)))


typedef uint8_t hset_slot_status_t;

struct struct_hset_t {
  uint32_t sizeof_item;
  uint32_t sizeof_slot;
  uint32_t num_items;
  uint32_t base_size;
  uint32_t hash_size;
  char store_hash;
  char ** rows;
  uint32_t no_rows;
  hset_hash_func_t hash_func;
  hset_cmp_func_t cmp_func;
};
typedef struct struct_hset_t struct_hset_t;


const uint64_t hset_crc64_tab[256] = {
  0x0000000000000000, 0x7ad870c830358979, 0xf5b0e190606b12f2,
  0x8f689158505e9b8b, 0xc038e5739841b68f, 0xbae095bba8743ff6,
  0x358804e3f82aa47d, 0x4f50742bc81f2d04, 0xab28ecb46814fe75,
  0xd1f09c7c5821770c, 0x5e980d24087fec87, 0x24407dec384a65fe,
  0x6b1009c7f05548fa, 0x11c8790fc060c183, 0x9ea0e857903e5a08,
  0xe478989fa00bd371, 0x7d08ff3b88be6f81, 0x07d08ff3b88be6f8,
  0x88b81eabe8d57d73, 0xf2606e63d8e0f40a, 0xbd301a4810ffd90e,
  0xc7e86a8020ca5077, 0x4880fbd87094cbfc, 0x32588b1040a14285,
  0xd620138fe0aa91f4, 0xacf86347d09f188d, 0x2390f21f80c18306,
  0x594882d7b0f40a7f, 0x1618f6fc78eb277b, 0x6cc0863448deae02,
  0xe3a8176c18803589, 0x997067a428b5bcf0, 0xfa11fe77117cdf02,
  0x80c98ebf2149567b, 0x0fa11fe77117cdf0, 0x75796f2f41224489,
  0x3a291b04893d698d, 0x40f16bccb908e0f4, 0xcf99fa94e9567b7f,
  0xb5418a5cd963f206, 0x513912c379682177, 0x2be1620b495da80e,
  0xa489f35319033385, 0xde51839b2936bafc, 0x9101f7b0e12997f8,
  0xebd98778d11c1e81, 0x64b116208142850a, 0x1e6966e8b1770c73,
  0x8719014c99c2b083, 0xfdc17184a9f739fa, 0x72a9e0dcf9a9a271,
  0x08719014c99c2b08, 0x4721e43f0183060c, 0x3df994f731b68f75,
  0xb29105af61e814fe, 0xc849756751dd9d87, 0x2c31edf8f1d64ef6,
  0x56e99d30c1e3c78f, 0xd9810c6891bd5c04, 0xa3597ca0a188d57d,
  0xec09088b6997f879, 0x96d1784359a27100, 0x19b9e91b09fcea8b,
  0x636199d339c963f2, 0xdf7adabd7a6e2d6f, 0xa5a2aa754a5ba416,
  0x2aca3b2d1a053f9d, 0x50124be52a30b6e4, 0x1f423fcee22f9be0,
  0x659a4f06d21a1299, 0xeaf2de5e82448912, 0x902aae96b271006b,
  0x74523609127ad31a, 0x0e8a46c1224f5a63, 0x81e2d7997211c1e8,
  0xfb3aa75142244891, 0xb46ad37a8a3b6595, 0xceb2a3b2ba0eecec,
  0x41da32eaea507767, 0x3b024222da65fe1e, 0xa2722586f2d042ee,
  0xd8aa554ec2e5cb97, 0x57c2c41692bb501c, 0x2d1ab4dea28ed965,
  0x624ac0f56a91f461, 0x1892b03d5aa47d18, 0x97fa21650afae693,
  0xed2251ad3acf6fea, 0x095ac9329ac4bc9b, 0x7382b9faaaf135e2,
  0xfcea28a2faafae69, 0x8632586aca9a2710, 0xc9622c4102850a14,
  0xb3ba5c8932b0836d, 0x3cd2cdd162ee18e6, 0x460abd1952db919f,
  0x256b24ca6b12f26d, 0x5fb354025b277b14, 0xd0dbc55a0b79e09f,
  0xaa03b5923b4c69e6, 0xe553c1b9f35344e2, 0x9f8bb171c366cd9b,
  0x10e3202993385610, 0x6a3b50e1a30ddf69, 0x8e43c87e03060c18,
  0xf49bb8b633338561, 0x7bf329ee636d1eea, 0x012b592653589793,
  0x4e7b2d0d9b47ba97, 0x34a35dc5ab7233ee, 0xbbcbcc9dfb2ca865,
  0xc113bc55cb19211c, 0x5863dbf1e3ac9dec, 0x22bbab39d3991495,
  0xadd33a6183c78f1e, 0xd70b4aa9b3f20667, 0x985b3e827bed2b63,
  0xe2834e4a4bd8a21a, 0x6debdf121b863991, 0x1733afda2bb3b0e8,
  0xf34b37458bb86399, 0x8993478dbb8deae0, 0x06fbd6d5ebd3716b,
  0x7c23a61ddbe6f812, 0x3373d23613f9d516, 0x49aba2fe23cc5c6f,
  0xc6c333a67392c7e4, 0xbc1b436e43a74e9d, 0x95ac9329ac4bc9b5,
  0xef74e3e19c7e40cc, 0x601c72b9cc20db47, 0x1ac40271fc15523e,
  0x5594765a340a7f3a, 0x2f4c0692043ff643, 0xa02497ca54616dc8,
  0xdafce7026454e4b1, 0x3e847f9dc45f37c0, 0x445c0f55f46abeb9,
  0xcb349e0da4342532, 0xb1eceec59401ac4b, 0xfebc9aee5c1e814f,
  0x8464ea266c2b0836, 0x0b0c7b7e3c7593bd, 0x71d40bb60c401ac4,
  0xe8a46c1224f5a634, 0x927c1cda14c02f4d, 0x1d148d82449eb4c6,
  0x67ccfd4a74ab3dbf, 0x289c8961bcb410bb, 0x5244f9a98c8199c2,
  0xdd2c68f1dcdf0249, 0xa7f41839ecea8b30, 0x438c80a64ce15841,
  0x3954f06e7cd4d138, 0xb63c61362c8a4ab3, 0xcce411fe1cbfc3ca,
  0x83b465d5d4a0eece, 0xf96c151de49567b7, 0x76048445b4cbfc3c,
  0x0cdcf48d84fe7545, 0x6fbd6d5ebd3716b7, 0x15651d968d029fce,
  0x9a0d8ccedd5c0445, 0xe0d5fc06ed698d3c, 0xaf85882d2576a038,
  0xd55df8e515432941, 0x5a3569bd451db2ca, 0x20ed197575283bb3,
  0xc49581ead523e8c2, 0xbe4df122e51661bb, 0x3125607ab548fa30,
  0x4bfd10b2857d7349, 0x04ad64994d625e4d, 0x7e7514517d57d734,
  0xf11d85092d094cbf, 0x8bc5f5c11d3cc5c6, 0x12b5926535897936,
  0x686de2ad05bcf04f, 0xe70573f555e26bc4, 0x9ddd033d65d7e2bd,
  0xd28d7716adc8cfb9, 0xa85507de9dfd46c0, 0x273d9686cda3dd4b,
  0x5de5e64efd965432, 0xb99d7ed15d9d8743, 0xc3450e196da80e3a,
  0x4c2d9f413df695b1, 0x36f5ef890dc31cc8, 0x79a59ba2c5dc31cc,
  0x037deb6af5e9b8b5, 0x8c157a32a5b7233e, 0xf6cd0afa9582aa47,
  0x4ad64994d625e4da, 0x300e395ce6106da3, 0xbf66a804b64ef628,
  0xc5bed8cc867b7f51, 0x8aeeace74e645255, 0xf036dc2f7e51db2c,
  0x7f5e4d772e0f40a7, 0x05863dbf1e3ac9de, 0xe1fea520be311aaf,
  0x9b26d5e88e0493d6, 0x144e44b0de5a085d, 0x6e963478ee6f8124,
  0x21c640532670ac20, 0x5b1e309b16452559, 0xd476a1c3461bbed2,
  0xaeaed10b762e37ab, 0x37deb6af5e9b8b5b, 0x4d06c6676eae0222,
  0xc26e573f3ef099a9, 0xb8b627f70ec510d0, 0xf7e653dcc6da3dd4,
  0x8d3e2314f6efb4ad, 0x0256b24ca6b12f26, 0x788ec2849684a65f,
  0x9cf65a1b368f752e, 0xe62e2ad306bafc57, 0x6946bb8b56e467dc,
  0x139ecb4366d1eea5, 0x5ccebf68aecec3a1, 0x2616cfa09efb4ad8,
  0xa97e5ef8cea5d153, 0xd3a62e30fe90582a, 0xb0c7b7e3c7593bd8,
  0xca1fc72bf76cb2a1, 0x45775673a732292a, 0x3faf26bb9707a053,
  0x70ff52905f188d57, 0x0a2722586f2d042e, 0x854fb3003f739fa5,
  0xff97c3c80f4616dc, 0x1bef5b57af4dc5ad, 0x61372b9f9f784cd4,
  0xee5fbac7cf26d75f, 0x9487ca0fff135e26, 0xdbd7be24370c7322,
  0xa10fceec0739fa5b, 0x2e675fb4576761d0, 0x54bf2f7c6752e8a9,
  0xcdcf48d84fe75459, 0xb71738107fd2dd20, 0x387fa9482f8c46ab,
  0x42a7d9801fb9cfd2, 0x0df7adabd7a6e2d6, 0x772fdd63e7936baf,
  0xf8474c3bb7cdf024, 0x829f3cf387f8795d, 0x66e7a46c27f3aa2c,
  0x1c3fd4a417c62355, 0x935745fc4798b8de, 0xe98f353477ad31a7,
  0xa6df411fbfb21ca3, 0xdc0731d78f8795da, 0x536fa08fdfd90e51,
  0x29b7d047efec8728
};


hval_t
hset_default_hash
(char * v,
 unsigned int len) {
  hval_t result = 0;
  for(int i = 0; i < len; i++) {
    result = hset_crc64_tab[(uint8_t) (result ^ v[i])] ^ (result >> 8);
  }
  return result;
}


void
hset_realloc_rows
(hset_t hset) {
  char ** new_rows;
  const uint32_t size = hset->base_size * (1 << hset->no_rows);
  const size_t char_size = (size_t) (((size_t) hset->sizeof_slot)
                                     * ((size_t) size));

  assert((new_rows = malloc(sizeof(char *) * (1 + hset->no_rows))));
  for(int i = 0; i < hset->no_rows; i ++) {
    new_rows[i] = hset->rows[i];
  }
  assert((new_rows[hset->no_rows] = malloc(char_size)));
  memset(new_rows[hset->no_rows], 0, char_size);
  free(hset->rows);
  hset->no_rows ++;
  hset->rows = new_rows;
}


hset_t
hset_new
(uint32_t sizeof_item,
 uint32_t base_size,
 char store_hash,
 hset_hash_func_t hash_func,
 hset_cmp_func_t cmp_func) {
  hset_t result;
  uint32_t base = 1;
  
  assert((result = malloc(sizeof(struct_hset_t))));
  while(base < base_size) {
    base <<= 1;
  }
  result->base_size = base;
  result->store_hash = store_hash;
  result->sizeof_item = sizeof_item;
  result->sizeof_slot = sizeof(hset_slot_status_t) + result->sizeof_item;
  if(store_hash) {
    result->sizeof_slot += sizeof(hval_t);
  }
  result->no_rows = 0;
  result->cmp_func = cmp_func;
  result->hash_func = hash_func;
  result->rows = NULL;
  result->hash_size = sizeof_item;
  hset_realloc_rows(result);
  hset_empty(result);
  return result;
}


void
hset_free
(hset_t hset) {
  for(int i = 0; i < hset->no_rows; i ++) {
    free(hset->rows[i]);
  }
  free(hset->rows);
  free(hset);
}


void
hset_empty
(hset_t hset) {
  uint32_t size = hset->base_size * hset->sizeof_slot;
  hset->num_items = 0;
  for(int i = 0; i < hset->no_rows; i ++) {  
    memset(hset->rows[i], 0, size);
    size = size << 1;
  }
}


uint32_t
hset_num_items
(hset_t hset) {
  return hset->num_items;
}


void
hset_set_hash_size
(hset_t hset,
 uint32_t hash_size) {
  hset->hash_size = hash_size;
}


#define HSET_LOOP(_empty_slot_code,                                     \
                  _hash_found_code,                                     \
                  _item_found_code,                                     \
                  _new_row_code,                                        \
                  _post_code) {                                         \
    const hval_t hash = item ? HSET_HASH_ITEM(hset, item) : h;          \
    uint32_t row_size = hset->base_size, row = 0, loops = 0;            \
    uint32_t slot = hash & (row_size - 1);                              \
    uint8_t loop = 1;                                                   \
    char * ptr = HSET_SLOT_ADDR(hset, row, slot);                       \
    while(loop) {                                                       \
      hset_slot_status_t status;                                        \
      HSET_SLOT_GET_STATUS(hset, ptr, &status);                         \
      switch(status) {                                                  \
      case HSET_SLOT_EMPTY:                                             \
        _empty_slot_code;                                               \
        loop = 0;                                                       \
        break;                                                          \
      case HSET_SLOT_OCCUPIED: {                                        \
        char same_hash = 1;                                             \
        if(hset->store_hash) {                                          \
          hval_t other_hash;                                            \
          HSET_SLOT_GET_HASH(hset, ptr, &other_hash);                   \
          same_hash = hash == other_hash;                               \
        }                                                               \
        if(same_hash) {                                                 \
          _hash_found_code;                                             \
          if(HSET_CMP_ITEM(hset, item,                                  \
                           HSET_SLOT_ADDR_ITEM(hset, ptr))) {           \
            _item_found_code;                                           \
          }                                                             \
        }                                                               \
        break;                                                          \
      }                                                                 \
      }                                                                 \
      if(loop) {                                                        \
        if((++ loops) < HSET_MAX_ROW_LOOP) {                            \
          HSET_MOVE_PTR_TO_NEXT_SLOT(hset, row, ptr);                   \
          slot = (slot + 1) & (row_size - 1);                           \
        } else {                                                        \
          loops = 0;                                                    \
          row ++;                                                       \
          row_size <<= 1;                                               \
          _new_row_code;                                                \
          slot = hash & (row_size - 1);                                 \
          ptr = HSET_SLOT_ADDR(hset, row, slot);                        \
        }                                                               \
      }                                                                 \
    }                                                                   \
    _post_code;                                                         \
  }


char
hset_insert
(hset_t hset,
 void * item,
 hset_mdata_t * mdata) {
#define hset_insert_empty_slot_code {                                   \
    hset->num_items ++;                                                 \
    status = HSET_SLOT_OCCUPIED;                                        \
    HSET_SLOT_SET_STATUS(hset, ptr, &status);                           \
    if(hset->store_hash) {                                              \
      HSET_SLOT_SET_HASH(hset, ptr, &hash);                             \
    }                                                                   \
    HSET_SLOT_SET_ITEM(hset, ptr, item);                                \
    result = 1;                                                         \
  }
#define hset_insert_item_found_code {                                   \
    loop = 0;                                                           \
  }
#define hset_insert_new_row_code {                                      \
    if(row == hset->no_rows) {                                          \
                              hset_realloc_rows(hset);                  \
    }                                                                   \
  }
#define hset_insert_post_code {                                         \
    if(mdata) {                                                         \
      mdata->ref = HSET_REF_PACK(row, slot);                            \
      mdata->hash = hash;                                               \
    }                                                                   \
  }
  char result = 0;
  hval_t h = 0;
  HSET_LOOP(hset_insert_empty_slot_code,
            {},
            hset_insert_item_found_code,
            hset_insert_new_row_code,
            hset_insert_post_code);
  return result;
}


void
hset_delete
(hset_t hset,
 hset_ref_t ref) {
  char * ptr;
  hset_slot_status_t status;
  uint32_t row, slot;
  HSET_REF_UNPACK(ref, row, slot);
  ptr = HSET_SLOT_ADDR(hset, row, slot);
  HSET_SLOT_GET_STATUS(hset, ptr, &status);
  if(HSET_SLOT_OCCUPIED == status) {
    memset(ptr, HSET_SLOT_DELETED, sizeof(hset_slot_status_t));
    hset->num_items --;
  }
}


char
hset_contains
(hset_t hset,
 void * item,
 hset_mdata_t * mdata) {
#define hset_contains_item_found_code {                                 \
    result = 1;                                                         \
    loop = 0;                                                           \
  }
#define hset_contains_new_row_code {                                    \
    if(row == hset->no_rows) {                                          \
      loop = 0;                                                         \
    }                                                                   \
  }
#define hset_contains_post_code hset_insert_post_code
  char result = 0;
  hval_t h = 0;
  HSET_LOOP({},
            {},
            hset_contains_item_found_code,
            hset_contains_new_row_code,
            hset_contains_post_code);
  return result;
}


char
hset_get
(hset_t hset,
 hset_ref_t ref,
 void * item) {
  const char * ptr = hset_get_ptr(hset, ref);
  const char result = ptr != NULL;
  if(result) {
    memcpy(item, ptr, hset->sizeof_item);
  }
  return result;
}


void *
hset_get_ptr
(hset_t hset,
 hset_ref_t ref) {
  char * ptr;
  hset_slot_status_t status;
  uint32_t row, slot;
  HSET_REF_UNPACK(ref, row, slot);
  ptr = HSET_SLOT_ADDR(hset, row, slot);
  HSET_SLOT_GET_STATUS(hset, ptr, &status);
  return (status == HSET_SLOT_OCCUPIED) ?
    HSET_SLOT_ADDR_ITEM(hset, ptr) : NULL;
}


void
hset_fold
(hset_t hset,
 void (* fold_func) (void *, void *),
 void * data) {
  uint32_t size = hset->base_size;
  for(int row = 0; row < hset->no_rows; row ++) {
    char * ptr = HSET_SLOT_ADDR(hset, row, 0);
    for(int i = 0; i < size; i ++) {
      hset_slot_status_t status;
      HSET_SLOT_GET_STATUS(hset, ptr, &status);
      if(HSET_SLOT_OCCUPIED == status) {
        fold_func(HSET_SLOT_ADDR_ITEM(hset, ptr), data);
      }
      ptr += hset->sizeof_slot;
    }
    size <<= 1;
  }
}


void
hset_hash_fold
(hset_t hset,
 hval_t h,
 void (* fold_func) (hval_t, void *, void *),
 void * data) {
#define hset_hash_fold_hash_found_code {                                \
    if(hset->store_hash ||                                              \
       h == HSET_HASH_ITEM(hset, HSET_SLOT_ADDR_ITEM(hset, ptr))) {     \
      fold_func(h, HSET_SLOT_ADDR_ITEM(hset, ptr), data);               \
    }                                                                   \
    break; /*  break to avoid item comparison  */                       \
  }
#define hset_hash_fold_new_row_code hset_contains_new_row_code
  void * item = NULL;
  HSET_LOOP({},
            hset_hash_fold_hash_found_code,
            {},
            hset_hash_fold_new_row_code,
            {});
}
